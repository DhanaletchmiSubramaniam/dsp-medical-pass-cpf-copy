package com.etiqa.medicalpass.mapper;

import com.etiqa.medicalpass.pojo.DspTblGender;
import com.etiqa.medicalpass.pojo.DspTblGenderExample;
import java.math.BigDecimal;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface DspTblGenderMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_TBL_GENDER
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    @SelectProvider(type=DspTblGenderSqlProvider.class, method="countByExample")
    long countByExample(DspTblGenderExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_TBL_GENDER
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    @DeleteProvider(type=DspTblGenderSqlProvider.class, method="deleteByExample")
    int deleteByExample(DspTblGenderExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_TBL_GENDER
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    @Delete({
        "delete from DSP_TBL_GENDER",
        "where ID = #{id,jdbcType=DECIMAL}"
    })
    int deleteByPrimaryKey(BigDecimal id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_TBL_GENDER
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    @Insert({
        "insert into DSP_TBL_GENDER (ID, GENDER_CODE, ",
        "GENDER_NAME, DESCRIPTION, ",
        "DESCRIPTION_BM)",
        "values (#{id,jdbcType=DECIMAL}, #{genderCode,jdbcType=VARCHAR}, ",
        "#{genderName,jdbcType=VARCHAR}, #{description,jdbcType=VARCHAR}, ",
        "#{descriptionBm,jdbcType=VARCHAR})"
    })
    int insert(DspTblGender record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_TBL_GENDER
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    @InsertProvider(type=DspTblGenderSqlProvider.class, method="insertSelective")
    int insertSelective(DspTblGender record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_TBL_GENDER
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    @SelectProvider(type=DspTblGenderSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="ID", property="id", jdbcType=JdbcType.DECIMAL, id=true),
        @Result(column="GENDER_CODE", property="genderCode", jdbcType=JdbcType.VARCHAR),
        @Result(column="GENDER_NAME", property="genderName", jdbcType=JdbcType.VARCHAR),
        @Result(column="DESCRIPTION", property="description", jdbcType=JdbcType.VARCHAR),
        @Result(column="DESCRIPTION_BM", property="descriptionBm", jdbcType=JdbcType.VARCHAR)
    })
    List<DspTblGender> selectByExample(DspTblGenderExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_TBL_GENDER
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    @Select({
        "select",
        "ID, GENDER_CODE, GENDER_NAME, DESCRIPTION, DESCRIPTION_BM",
        "from DSP_TBL_GENDER",
        "where ID = #{id,jdbcType=DECIMAL}"
    })
    @Results({
        @Result(column="ID", property="id", jdbcType=JdbcType.DECIMAL, id=true),
        @Result(column="GENDER_CODE", property="genderCode", jdbcType=JdbcType.VARCHAR),
        @Result(column="GENDER_NAME", property="genderName", jdbcType=JdbcType.VARCHAR),
        @Result(column="DESCRIPTION", property="description", jdbcType=JdbcType.VARCHAR),
        @Result(column="DESCRIPTION_BM", property="descriptionBm", jdbcType=JdbcType.VARCHAR)
    })
    DspTblGender selectByPrimaryKey(BigDecimal id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_TBL_GENDER
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    @UpdateProvider(type=DspTblGenderSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") DspTblGender record, @Param("example") DspTblGenderExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_TBL_GENDER
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    @UpdateProvider(type=DspTblGenderSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") DspTblGender record, @Param("example") DspTblGenderExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_TBL_GENDER
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    @UpdateProvider(type=DspTblGenderSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(DspTblGender record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_TBL_GENDER
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    @Update({
        "update DSP_TBL_GENDER",
        "set GENDER_CODE = #{genderCode,jdbcType=VARCHAR},",
          "GENDER_NAME = #{genderName,jdbcType=VARCHAR},",
          "DESCRIPTION = #{description,jdbcType=VARCHAR},",
          "DESCRIPTION_BM = #{descriptionBm,jdbcType=VARCHAR}",
        "where ID = #{id,jdbcType=DECIMAL}"
    })
    int updateByPrimaryKey(DspTblGender record);
}