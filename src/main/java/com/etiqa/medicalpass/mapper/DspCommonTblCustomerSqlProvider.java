package com.etiqa.medicalpass.mapper;

import com.etiqa.medicalpass.pojo.DspCommonTblCustomer;
import com.etiqa.medicalpass.pojo.DspCommonTblCustomerExample.Criteria;
import com.etiqa.medicalpass.pojo.DspCommonTblCustomerExample.Criterion;
import com.etiqa.medicalpass.pojo.DspCommonTblCustomerExample;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.jdbc.SQL;

public class DspCommonTblCustomerSqlProvider {

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_COMMON_TBL_CUSTOMER
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    public String countByExample(DspCommonTblCustomerExample example) {
        SQL sql = new SQL();
        sql.SELECT("count(*)").FROM("DSP_COMMON_TBL_CUSTOMER");
        applyWhere(sql, example, false);
        return sql.toString();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_COMMON_TBL_CUSTOMER
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    public String deleteByExample(DspCommonTblCustomerExample example) {
        SQL sql = new SQL();
        sql.DELETE_FROM("DSP_COMMON_TBL_CUSTOMER");
        applyWhere(sql, example, false);
        return sql.toString();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_COMMON_TBL_CUSTOMER
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    public String insertSelective(DspCommonTblCustomer record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("DSP_COMMON_TBL_CUSTOMER");
        
        if (record.getCustomerId() != null) {
            sql.VALUES("CUSTOMER_ID", "#{customerId,jdbcType=DECIMAL}");
        }
        
        if (record.getCustomerIdType() != null) {
            sql.VALUES("CUSTOMER_ID_TYPE", "#{customerIdType,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerNricId() != null) {
            sql.VALUES("CUSTOMER_NRIC_ID", "#{customerNricId,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerName() != null) {
            sql.VALUES("CUSTOMER_NAME", "#{customerName,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerDob() != null) {
            sql.VALUES("CUSTOMER_DOB", "#{customerDob,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerGender() != null) {
            sql.VALUES("CUSTOMER_GENDER", "#{customerGender,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerRace() != null) {
            sql.VALUES("CUSTOMER_RACE", "#{customerRace,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerReligion() != null) {
            sql.VALUES("CUSTOMER_RELIGION", "#{customerReligion,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerNationality() != null) {
            sql.VALUES("CUSTOMER_NATIONALITY", "#{customerNationality,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerTitle() != null) {
            sql.VALUES("CUSTOMER_TITLE", "#{customerTitle,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerNationalityRace() != null) {
            sql.VALUES("CUSTOMER_NATIONALITY_RACE", "#{customerNationalityRace,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerEduLevel() != null) {
            sql.VALUES("CUSTOMER_EDU_LEVEL", "#{customerEduLevel,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerMaritalstatus() != null) {
            sql.VALUES("CUSTOMER_MARITALSTATUS", "#{customerMaritalstatus,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerSalaryRange() != null) {
            sql.VALUES("CUSTOMER_SALARY_RANGE", "#{customerSalaryRange,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerOccupation() != null) {
            sql.VALUES("CUSTOMER_OCCUPATION", "#{customerOccupation,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerAddress1() != null) {
            sql.VALUES("CUSTOMER_ADDRESS1", "#{customerAddress1,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerAddress2() != null) {
            sql.VALUES("CUSTOMER_ADDRESS2", "#{customerAddress2,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerAddress3() != null) {
            sql.VALUES("CUSTOMER_ADDRESS3", "#{customerAddress3,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerPostcode() != null) {
            sql.VALUES("CUSTOMER_POSTCODE", "#{customerPostcode,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerState() != null) {
            sql.VALUES("CUSTOMER_STATE", "#{customerState,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerCountry() != null) {
            sql.VALUES("CUSTOMER_COUNTRY", "#{customerCountry,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerMailAddress1() != null) {
            sql.VALUES("CUSTOMER_MAIL_ADDRESS1", "#{customerMailAddress1,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerMailAddress2() != null) {
            sql.VALUES("CUSTOMER_MAIL_ADDRESS2", "#{customerMailAddress2,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerMailAddress3() != null) {
            sql.VALUES("CUSTOMER_MAIL_ADDRESS3", "#{customerMailAddress3,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerMailPostcode() != null) {
            sql.VALUES("CUSTOMER_MAIL_POSTCODE", "#{customerMailPostcode,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerMailState() != null) {
            sql.VALUES("CUSTOMER_MAIL_STATE", "#{customerMailState,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerMailCountry() != null) {
            sql.VALUES("CUSTOMER_MAIL_COUNTRY", "#{customerMailCountry,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerNoChildren() != null) {
            sql.VALUES("CUSTOMER_NO_CHILDREN", "#{customerNoChildren,jdbcType=DECIMAL}");
        }
        
        if (record.getCustomerMobileNo() != null) {
            sql.VALUES("CUSTOMER_MOBILE_NO", "#{customerMobileNo,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerEmail() != null) {
            sql.VALUES("CUSTOMER_EMAIL", "#{customerEmail,jdbcType=VARCHAR}");
        }
        
        if (record.getLeadsFlag() != null) {
            sql.VALUES("LEADS_FLAG", "#{leadsFlag,jdbcType=CHAR}");
        }
        
        if (record.getHomeMailCheck() != null) {
            sql.VALUES("HOME_MAIL_CHECK", "#{homeMailCheck,jdbcType=VARCHAR}");
        }
        
        if (record.getEmailSentCounter() != null) {
            sql.VALUES("EMAIL_SENT_COUNTER", "#{emailSentCounter,jdbcType=VARCHAR}");
        }
        
        if (record.getQqId() != null) {
            sql.VALUES("QQ_ID", "#{qqId,jdbcType=DECIMAL}");
        }
        
        if (record.getCreateDate() != null) {
            sql.VALUES("CREATE_DATE", "#{createDate,jdbcType=TIMESTAMP}");
        }
        
        if (record.getUpdatedDate() != null) {
            sql.VALUES("UPDATED_DATE", "#{updatedDate,jdbcType=TIMESTAMP}");
        }
        
        if (record.getCustomerEmployer() != null) {
            sql.VALUES("CUSTOMER_EMPLOYER", "#{customerEmployer,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerClienttype() != null) {
            sql.VALUES("CUSTOMER_CLIENTTYPE", "#{customerClienttype,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerIndustry() != null) {
            sql.VALUES("CUSTOMER_INDUSTRY", "#{customerIndustry,jdbcType=VARCHAR}");
        }
        
        return sql.toString();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_COMMON_TBL_CUSTOMER
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    public String selectByExample(DspCommonTblCustomerExample example) {
        SQL sql = new SQL();
        if (example != null && example.isDistinct()) {
            sql.SELECT_DISTINCT("CUSTOMER_ID");
        } else {
            sql.SELECT("CUSTOMER_ID");
        }
        sql.SELECT("CUSTOMER_ID_TYPE");
        sql.SELECT("CUSTOMER_NRIC_ID");
        sql.SELECT("CUSTOMER_NAME");
        sql.SELECT("CUSTOMER_DOB");
        sql.SELECT("CUSTOMER_GENDER");
        sql.SELECT("CUSTOMER_RACE");
        sql.SELECT("CUSTOMER_RELIGION");
        sql.SELECT("CUSTOMER_NATIONALITY");
        sql.SELECT("CUSTOMER_TITLE");
        sql.SELECT("CUSTOMER_NATIONALITY_RACE");
        sql.SELECT("CUSTOMER_EDU_LEVEL");
        sql.SELECT("CUSTOMER_MARITALSTATUS");
        sql.SELECT("CUSTOMER_SALARY_RANGE");
        sql.SELECT("CUSTOMER_OCCUPATION");
        sql.SELECT("CUSTOMER_ADDRESS1");
        sql.SELECT("CUSTOMER_ADDRESS2");
        sql.SELECT("CUSTOMER_ADDRESS3");
        sql.SELECT("CUSTOMER_POSTCODE");
        sql.SELECT("CUSTOMER_STATE");
        sql.SELECT("CUSTOMER_COUNTRY");
        sql.SELECT("CUSTOMER_MAIL_ADDRESS1");
        sql.SELECT("CUSTOMER_MAIL_ADDRESS2");
        sql.SELECT("CUSTOMER_MAIL_ADDRESS3");
        sql.SELECT("CUSTOMER_MAIL_POSTCODE");
        sql.SELECT("CUSTOMER_MAIL_STATE");
        sql.SELECT("CUSTOMER_MAIL_COUNTRY");
        sql.SELECT("CUSTOMER_NO_CHILDREN");
        sql.SELECT("CUSTOMER_MOBILE_NO");
        sql.SELECT("CUSTOMER_EMAIL");
        sql.SELECT("LEADS_FLAG");
        sql.SELECT("HOME_MAIL_CHECK");
        sql.SELECT("EMAIL_SENT_COUNTER");
        sql.SELECT("QQ_ID");
        sql.SELECT("CREATE_DATE");
        sql.SELECT("UPDATED_DATE");
        sql.SELECT("CUSTOMER_EMPLOYER");
        sql.SELECT("CUSTOMER_CLIENTTYPE");
        sql.SELECT("CUSTOMER_INDUSTRY");
        sql.FROM("DSP_COMMON_TBL_CUSTOMER");
        applyWhere(sql, example, false);
        
        if (example != null && example.getOrderByClause() != null) {
            sql.ORDER_BY(example.getOrderByClause());
        }
        
        return sql.toString();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_COMMON_TBL_CUSTOMER
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    public String updateByExampleSelective(Map<String, Object> parameter) {
        DspCommonTblCustomer record = (DspCommonTblCustomer) parameter.get("record");
        DspCommonTblCustomerExample example = (DspCommonTblCustomerExample) parameter.get("example");
        
        SQL sql = new SQL();
        sql.UPDATE("DSP_COMMON_TBL_CUSTOMER");
        
        if (record.getCustomerId() != null) {
            sql.SET("CUSTOMER_ID = #{record.customerId,jdbcType=DECIMAL}");
        }
        
        if (record.getCustomerIdType() != null) {
            sql.SET("CUSTOMER_ID_TYPE = #{record.customerIdType,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerNricId() != null) {
            sql.SET("CUSTOMER_NRIC_ID = #{record.customerNricId,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerName() != null) {
            sql.SET("CUSTOMER_NAME = #{record.customerName,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerDob() != null) {
            sql.SET("CUSTOMER_DOB = #{record.customerDob,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerGender() != null) {
            sql.SET("CUSTOMER_GENDER = #{record.customerGender,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerRace() != null) {
            sql.SET("CUSTOMER_RACE = #{record.customerRace,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerReligion() != null) {
            sql.SET("CUSTOMER_RELIGION = #{record.customerReligion,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerNationality() != null) {
            sql.SET("CUSTOMER_NATIONALITY = #{record.customerNationality,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerTitle() != null) {
            sql.SET("CUSTOMER_TITLE = #{record.customerTitle,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerNationalityRace() != null) {
            sql.SET("CUSTOMER_NATIONALITY_RACE = #{record.customerNationalityRace,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerEduLevel() != null) {
            sql.SET("CUSTOMER_EDU_LEVEL = #{record.customerEduLevel,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerMaritalstatus() != null) {
            sql.SET("CUSTOMER_MARITALSTATUS = #{record.customerMaritalstatus,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerSalaryRange() != null) {
            sql.SET("CUSTOMER_SALARY_RANGE = #{record.customerSalaryRange,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerOccupation() != null) {
            sql.SET("CUSTOMER_OCCUPATION = #{record.customerOccupation,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerAddress1() != null) {
            sql.SET("CUSTOMER_ADDRESS1 = #{record.customerAddress1,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerAddress2() != null) {
            sql.SET("CUSTOMER_ADDRESS2 = #{record.customerAddress2,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerAddress3() != null) {
            sql.SET("CUSTOMER_ADDRESS3 = #{record.customerAddress3,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerPostcode() != null) {
            sql.SET("CUSTOMER_POSTCODE = #{record.customerPostcode,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerState() != null) {
            sql.SET("CUSTOMER_STATE = #{record.customerState,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerCountry() != null) {
            sql.SET("CUSTOMER_COUNTRY = #{record.customerCountry,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerMailAddress1() != null) {
            sql.SET("CUSTOMER_MAIL_ADDRESS1 = #{record.customerMailAddress1,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerMailAddress2() != null) {
            sql.SET("CUSTOMER_MAIL_ADDRESS2 = #{record.customerMailAddress2,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerMailAddress3() != null) {
            sql.SET("CUSTOMER_MAIL_ADDRESS3 = #{record.customerMailAddress3,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerMailPostcode() != null) {
            sql.SET("CUSTOMER_MAIL_POSTCODE = #{record.customerMailPostcode,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerMailState() != null) {
            sql.SET("CUSTOMER_MAIL_STATE = #{record.customerMailState,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerMailCountry() != null) {
            sql.SET("CUSTOMER_MAIL_COUNTRY = #{record.customerMailCountry,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerNoChildren() != null) {
            sql.SET("CUSTOMER_NO_CHILDREN = #{record.customerNoChildren,jdbcType=DECIMAL}");
        }
        
        if (record.getCustomerMobileNo() != null) {
            sql.SET("CUSTOMER_MOBILE_NO = #{record.customerMobileNo,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerEmail() != null) {
            sql.SET("CUSTOMER_EMAIL = #{record.customerEmail,jdbcType=VARCHAR}");
        }
        
        if (record.getLeadsFlag() != null) {
            sql.SET("LEADS_FLAG = #{record.leadsFlag,jdbcType=CHAR}");
        }
        
        if (record.getHomeMailCheck() != null) {
            sql.SET("HOME_MAIL_CHECK = #{record.homeMailCheck,jdbcType=VARCHAR}");
        }
        
        if (record.getEmailSentCounter() != null) {
            sql.SET("EMAIL_SENT_COUNTER = #{record.emailSentCounter,jdbcType=VARCHAR}");
        }
        
        if (record.getQqId() != null) {
            sql.SET("QQ_ID = #{record.qqId,jdbcType=DECIMAL}");
        }
        
        if (record.getCreateDate() != null) {
            sql.SET("CREATE_DATE = #{record.createDate,jdbcType=TIMESTAMP}");
        }
        
        if (record.getUpdatedDate() != null) {
            sql.SET("UPDATED_DATE = #{record.updatedDate,jdbcType=TIMESTAMP}");
        }
        
        if (record.getCustomerEmployer() != null) {
            sql.SET("CUSTOMER_EMPLOYER = #{record.customerEmployer,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerClienttype() != null) {
            sql.SET("CUSTOMER_CLIENTTYPE = #{record.customerClienttype,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerIndustry() != null) {
            sql.SET("CUSTOMER_INDUSTRY = #{record.customerIndustry,jdbcType=VARCHAR}");
        }
        
        applyWhere(sql, example, true);
        return sql.toString();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_COMMON_TBL_CUSTOMER
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    public String updateByExample(Map<String, Object> parameter) {
        SQL sql = new SQL();
        sql.UPDATE("DSP_COMMON_TBL_CUSTOMER");
        
        sql.SET("CUSTOMER_ID = #{record.customerId,jdbcType=DECIMAL}");
        sql.SET("CUSTOMER_ID_TYPE = #{record.customerIdType,jdbcType=VARCHAR}");
        sql.SET("CUSTOMER_NRIC_ID = #{record.customerNricId,jdbcType=VARCHAR}");
        sql.SET("CUSTOMER_NAME = #{record.customerName,jdbcType=VARCHAR}");
        sql.SET("CUSTOMER_DOB = #{record.customerDob,jdbcType=VARCHAR}");
        sql.SET("CUSTOMER_GENDER = #{record.customerGender,jdbcType=VARCHAR}");
        sql.SET("CUSTOMER_RACE = #{record.customerRace,jdbcType=VARCHAR}");
        sql.SET("CUSTOMER_RELIGION = #{record.customerReligion,jdbcType=VARCHAR}");
        sql.SET("CUSTOMER_NATIONALITY = #{record.customerNationality,jdbcType=VARCHAR}");
        sql.SET("CUSTOMER_TITLE = #{record.customerTitle,jdbcType=VARCHAR}");
        sql.SET("CUSTOMER_NATIONALITY_RACE = #{record.customerNationalityRace,jdbcType=VARCHAR}");
        sql.SET("CUSTOMER_EDU_LEVEL = #{record.customerEduLevel,jdbcType=VARCHAR}");
        sql.SET("CUSTOMER_MARITALSTATUS = #{record.customerMaritalstatus,jdbcType=VARCHAR}");
        sql.SET("CUSTOMER_SALARY_RANGE = #{record.customerSalaryRange,jdbcType=VARCHAR}");
        sql.SET("CUSTOMER_OCCUPATION = #{record.customerOccupation,jdbcType=VARCHAR}");
        sql.SET("CUSTOMER_ADDRESS1 = #{record.customerAddress1,jdbcType=VARCHAR}");
        sql.SET("CUSTOMER_ADDRESS2 = #{record.customerAddress2,jdbcType=VARCHAR}");
        sql.SET("CUSTOMER_ADDRESS3 = #{record.customerAddress3,jdbcType=VARCHAR}");
        sql.SET("CUSTOMER_POSTCODE = #{record.customerPostcode,jdbcType=VARCHAR}");
        sql.SET("CUSTOMER_STATE = #{record.customerState,jdbcType=VARCHAR}");
        sql.SET("CUSTOMER_COUNTRY = #{record.customerCountry,jdbcType=VARCHAR}");
        sql.SET("CUSTOMER_MAIL_ADDRESS1 = #{record.customerMailAddress1,jdbcType=VARCHAR}");
        sql.SET("CUSTOMER_MAIL_ADDRESS2 = #{record.customerMailAddress2,jdbcType=VARCHAR}");
        sql.SET("CUSTOMER_MAIL_ADDRESS3 = #{record.customerMailAddress3,jdbcType=VARCHAR}");
        sql.SET("CUSTOMER_MAIL_POSTCODE = #{record.customerMailPostcode,jdbcType=VARCHAR}");
        sql.SET("CUSTOMER_MAIL_STATE = #{record.customerMailState,jdbcType=VARCHAR}");
        sql.SET("CUSTOMER_MAIL_COUNTRY = #{record.customerMailCountry,jdbcType=VARCHAR}");
        sql.SET("CUSTOMER_NO_CHILDREN = #{record.customerNoChildren,jdbcType=DECIMAL}");
        sql.SET("CUSTOMER_MOBILE_NO = #{record.customerMobileNo,jdbcType=VARCHAR}");
        sql.SET("CUSTOMER_EMAIL = #{record.customerEmail,jdbcType=VARCHAR}");
        sql.SET("LEADS_FLAG = #{record.leadsFlag,jdbcType=CHAR}");
        sql.SET("HOME_MAIL_CHECK = #{record.homeMailCheck,jdbcType=VARCHAR}");
        sql.SET("EMAIL_SENT_COUNTER = #{record.emailSentCounter,jdbcType=VARCHAR}");
        sql.SET("QQ_ID = #{record.qqId,jdbcType=DECIMAL}");
        sql.SET("CREATE_DATE = #{record.createDate,jdbcType=TIMESTAMP}");
        sql.SET("UPDATED_DATE = #{record.updatedDate,jdbcType=TIMESTAMP}");
        sql.SET("CUSTOMER_EMPLOYER = #{record.customerEmployer,jdbcType=VARCHAR}");
        sql.SET("CUSTOMER_CLIENTTYPE = #{record.customerClienttype,jdbcType=VARCHAR}");
        sql.SET("CUSTOMER_INDUSTRY = #{record.customerIndustry,jdbcType=VARCHAR}");
        
        DspCommonTblCustomerExample example = (DspCommonTblCustomerExample) parameter.get("example");
        applyWhere(sql, example, true);
        return sql.toString();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_COMMON_TBL_CUSTOMER
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    public String updateByPrimaryKeySelective(DspCommonTblCustomer record) {
        SQL sql = new SQL();
        sql.UPDATE("DSP_COMMON_TBL_CUSTOMER");
        
        if (record.getCustomerIdType() != null) {
            sql.SET("CUSTOMER_ID_TYPE = #{customerIdType,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerNricId() != null) {
            sql.SET("CUSTOMER_NRIC_ID = #{customerNricId,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerName() != null) {
            sql.SET("CUSTOMER_NAME = #{customerName,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerDob() != null) {
            sql.SET("CUSTOMER_DOB = #{customerDob,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerGender() != null) {
            sql.SET("CUSTOMER_GENDER = #{customerGender,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerRace() != null) {
            sql.SET("CUSTOMER_RACE = #{customerRace,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerReligion() != null) {
            sql.SET("CUSTOMER_RELIGION = #{customerReligion,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerNationality() != null) {
            sql.SET("CUSTOMER_NATIONALITY = #{customerNationality,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerTitle() != null) {
            sql.SET("CUSTOMER_TITLE = #{customerTitle,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerNationalityRace() != null) {
            sql.SET("CUSTOMER_NATIONALITY_RACE = #{customerNationalityRace,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerEduLevel() != null) {
            sql.SET("CUSTOMER_EDU_LEVEL = #{customerEduLevel,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerMaritalstatus() != null) {
            sql.SET("CUSTOMER_MARITALSTATUS = #{customerMaritalstatus,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerSalaryRange() != null) {
            sql.SET("CUSTOMER_SALARY_RANGE = #{customerSalaryRange,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerOccupation() != null) {
            sql.SET("CUSTOMER_OCCUPATION = #{customerOccupation,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerAddress1() != null) {
            sql.SET("CUSTOMER_ADDRESS1 = #{customerAddress1,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerAddress2() != null) {
            sql.SET("CUSTOMER_ADDRESS2 = #{customerAddress2,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerAddress3() != null) {
            sql.SET("CUSTOMER_ADDRESS3 = #{customerAddress3,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerPostcode() != null) {
            sql.SET("CUSTOMER_POSTCODE = #{customerPostcode,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerState() != null) {
            sql.SET("CUSTOMER_STATE = #{customerState,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerCountry() != null) {
            sql.SET("CUSTOMER_COUNTRY = #{customerCountry,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerMailAddress1() != null) {
            sql.SET("CUSTOMER_MAIL_ADDRESS1 = #{customerMailAddress1,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerMailAddress2() != null) {
            sql.SET("CUSTOMER_MAIL_ADDRESS2 = #{customerMailAddress2,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerMailAddress3() != null) {
            sql.SET("CUSTOMER_MAIL_ADDRESS3 = #{customerMailAddress3,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerMailPostcode() != null) {
            sql.SET("CUSTOMER_MAIL_POSTCODE = #{customerMailPostcode,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerMailState() != null) {
            sql.SET("CUSTOMER_MAIL_STATE = #{customerMailState,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerMailCountry() != null) {
            sql.SET("CUSTOMER_MAIL_COUNTRY = #{customerMailCountry,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerNoChildren() != null) {
            sql.SET("CUSTOMER_NO_CHILDREN = #{customerNoChildren,jdbcType=DECIMAL}");
        }
        
        if (record.getCustomerMobileNo() != null) {
            sql.SET("CUSTOMER_MOBILE_NO = #{customerMobileNo,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerEmail() != null) {
            sql.SET("CUSTOMER_EMAIL = #{customerEmail,jdbcType=VARCHAR}");
        }
        
        if (record.getLeadsFlag() != null) {
            sql.SET("LEADS_FLAG = #{leadsFlag,jdbcType=CHAR}");
        }
        
        if (record.getHomeMailCheck() != null) {
            sql.SET("HOME_MAIL_CHECK = #{homeMailCheck,jdbcType=VARCHAR}");
        }
        
        if (record.getEmailSentCounter() != null) {
            sql.SET("EMAIL_SENT_COUNTER = #{emailSentCounter,jdbcType=VARCHAR}");
        }
        
        if (record.getQqId() != null) {
            sql.SET("QQ_ID = #{qqId,jdbcType=DECIMAL}");
        }
        
        if (record.getCreateDate() != null) {
            sql.SET("CREATE_DATE = #{createDate,jdbcType=TIMESTAMP}");
        }
        
        if (record.getUpdatedDate() != null) {
            sql.SET("UPDATED_DATE = #{updatedDate,jdbcType=TIMESTAMP}");
        }
        
        if (record.getCustomerEmployer() != null) {
            sql.SET("CUSTOMER_EMPLOYER = #{customerEmployer,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerClienttype() != null) {
            sql.SET("CUSTOMER_CLIENTTYPE = #{customerClienttype,jdbcType=VARCHAR}");
        }
        
        if (record.getCustomerIndustry() != null) {
            sql.SET("CUSTOMER_INDUSTRY = #{customerIndustry,jdbcType=VARCHAR}");
        }
        
        sql.WHERE("CUSTOMER_ID = #{customerId,jdbcType=DECIMAL}");
        
        return sql.toString();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_COMMON_TBL_CUSTOMER
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    protected void applyWhere(SQL sql, DspCommonTblCustomerExample example, boolean includeExamplePhrase) {
        if (example == null) {
            return;
        }
        
        String parmPhrase1;
        String parmPhrase1_th;
        String parmPhrase2;
        String parmPhrase2_th;
        String parmPhrase3;
        String parmPhrase3_th;
        if (includeExamplePhrase) {
            parmPhrase1 = "%s #{example.oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{example.oredCriteria[%d].allCriteria[%d].value} and #{example.oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{example.oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{example.oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{example.oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        } else {
            parmPhrase1 = "%s #{oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{oredCriteria[%d].allCriteria[%d].value} and #{oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        }
        
        StringBuilder sb = new StringBuilder();
        List<Criteria> oredCriteria = example.getOredCriteria();
        boolean firstCriteria = true;
        for (int i = 0; i < oredCriteria.size(); i++) {
            Criteria criteria = oredCriteria.get(i);
            if (criteria.isValid()) {
                if (firstCriteria) {
                    firstCriteria = false;
                } else {
                    sb.append(" or ");
                }
                
                sb.append('(');
                List<Criterion> criterions = criteria.getAllCriteria();
                boolean firstCriterion = true;
                for (int j = 0; j < criterions.size(); j++) {
                    Criterion criterion = criterions.get(j);
                    if (firstCriterion) {
                        firstCriterion = false;
                    } else {
                        sb.append(" and ");
                    }
                    
                    if (criterion.isNoValue()) {
                        sb.append(criterion.getCondition());
                    } else if (criterion.isSingleValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase1, criterion.getCondition(), i, j));
                        } else {
                            sb.append(String.format(parmPhrase1_th, criterion.getCondition(), i, j,criterion.getTypeHandler()));
                        }
                    } else if (criterion.isBetweenValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase2, criterion.getCondition(), i, j, i, j));
                        } else {
                            sb.append(String.format(parmPhrase2_th, criterion.getCondition(), i, j, criterion.getTypeHandler(), i, j, criterion.getTypeHandler()));
                        }
                    } else if (criterion.isListValue()) {
                        sb.append(criterion.getCondition());
                        sb.append(" (");
                        List<?> listItems = (List<?>) criterion.getValue();
                        boolean comma = false;
                        for (int k = 0; k < listItems.size(); k++) {
                            if (comma) {
                                sb.append(", ");
                            } else {
                                comma = true;
                            }
                            if (criterion.getTypeHandler() == null) {
                                sb.append(String.format(parmPhrase3, i, j, k));
                            } else {
                                sb.append(String.format(parmPhrase3_th, i, j, k, criterion.getTypeHandler()));
                            }
                        }
                        sb.append(')');
                    }
                }
                sb.append(')');
            }
        }
        
        if (sb.length() > 0) {
            sql.WHERE(sb.toString());
        }
    }
}