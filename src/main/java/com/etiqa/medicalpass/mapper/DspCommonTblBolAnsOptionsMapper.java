package com.etiqa.medicalpass.mapper;

import com.etiqa.medicalpass.pojo.DspCommonTblBolAnsOptions;
import com.etiqa.medicalpass.pojo.DspCommonTblBolAnsOptionsExample;
import java.math.BigDecimal;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface DspCommonTblBolAnsOptionsMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_COMMON_TBL_BOL_ANS_OPTIONS
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    @SelectProvider(type=DspCommonTblBolAnsOptionsSqlProvider.class, method="countByExample")
    long countByExample(DspCommonTblBolAnsOptionsExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_COMMON_TBL_BOL_ANS_OPTIONS
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    @DeleteProvider(type=DspCommonTblBolAnsOptionsSqlProvider.class, method="deleteByExample")
    int deleteByExample(DspCommonTblBolAnsOptionsExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_COMMON_TBL_BOL_ANS_OPTIONS
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    @Delete({
        "delete from DSP_COMMON_TBL_BOL_ANS_OPTIONS",
        "where ID = #{id,jdbcType=DECIMAL}"
    })
    int deleteByPrimaryKey(BigDecimal id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_COMMON_TBL_BOL_ANS_OPTIONS
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    @Insert({
        "insert into DSP_COMMON_TBL_BOL_ANS_OPTIONS (ID, ANSWER_OPTION_CODE, ",
        "ANSWER_OPTION_VALUE, DESCRIPTION)",
        "values (#{id,jdbcType=DECIMAL}, #{answerOptionCode,jdbcType=VARCHAR}, ",
        "#{answerOptionValue,jdbcType=VARCHAR}, #{description,jdbcType=VARCHAR})"
    })
    int insert(DspCommonTblBolAnsOptions record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_COMMON_TBL_BOL_ANS_OPTIONS
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    @InsertProvider(type=DspCommonTblBolAnsOptionsSqlProvider.class, method="insertSelective")
    int insertSelective(DspCommonTblBolAnsOptions record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_COMMON_TBL_BOL_ANS_OPTIONS
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    @SelectProvider(type=DspCommonTblBolAnsOptionsSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="ID", property="id", jdbcType=JdbcType.DECIMAL, id=true),
        @Result(column="ANSWER_OPTION_CODE", property="answerOptionCode", jdbcType=JdbcType.VARCHAR),
        @Result(column="ANSWER_OPTION_VALUE", property="answerOptionValue", jdbcType=JdbcType.VARCHAR),
        @Result(column="DESCRIPTION", property="description", jdbcType=JdbcType.VARCHAR)
    })
    List<DspCommonTblBolAnsOptions> selectByExample(DspCommonTblBolAnsOptionsExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_COMMON_TBL_BOL_ANS_OPTIONS
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    @Select({
        "select",
        "ID, ANSWER_OPTION_CODE, ANSWER_OPTION_VALUE, DESCRIPTION",
        "from DSP_COMMON_TBL_BOL_ANS_OPTIONS",
        "where ID = #{id,jdbcType=DECIMAL}"
    })
    @Results({
        @Result(column="ID", property="id", jdbcType=JdbcType.DECIMAL, id=true),
        @Result(column="ANSWER_OPTION_CODE", property="answerOptionCode", jdbcType=JdbcType.VARCHAR),
        @Result(column="ANSWER_OPTION_VALUE", property="answerOptionValue", jdbcType=JdbcType.VARCHAR),
        @Result(column="DESCRIPTION", property="description", jdbcType=JdbcType.VARCHAR)
    })
    DspCommonTblBolAnsOptions selectByPrimaryKey(BigDecimal id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_COMMON_TBL_BOL_ANS_OPTIONS
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    @UpdateProvider(type=DspCommonTblBolAnsOptionsSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") DspCommonTblBolAnsOptions record, @Param("example") DspCommonTblBolAnsOptionsExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_COMMON_TBL_BOL_ANS_OPTIONS
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    @UpdateProvider(type=DspCommonTblBolAnsOptionsSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") DspCommonTblBolAnsOptions record, @Param("example") DspCommonTblBolAnsOptionsExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_COMMON_TBL_BOL_ANS_OPTIONS
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    @UpdateProvider(type=DspCommonTblBolAnsOptionsSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(DspCommonTblBolAnsOptions record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_COMMON_TBL_BOL_ANS_OPTIONS
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    @Update({
        "update DSP_COMMON_TBL_BOL_ANS_OPTIONS",
        "set ANSWER_OPTION_CODE = #{answerOptionCode,jdbcType=VARCHAR},",
          "ANSWER_OPTION_VALUE = #{answerOptionValue,jdbcType=VARCHAR},",
          "DESCRIPTION = #{description,jdbcType=VARCHAR}",
        "where ID = #{id,jdbcType=DECIMAL}"
    })
    int updateByPrimaryKey(DspCommonTblBolAnsOptions record);
}