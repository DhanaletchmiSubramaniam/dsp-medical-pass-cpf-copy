package com.etiqa.medicalpass.mapper;

import com.etiqa.medicalpass.pojo.DspMpTblVpms;
import com.etiqa.medicalpass.pojo.DspMpTblVpmsExample.Criteria;
import com.etiqa.medicalpass.pojo.DspMpTblVpmsExample.Criterion;
import com.etiqa.medicalpass.pojo.DspMpTblVpmsExample;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.jdbc.SQL;

public class DspMpTblVpmsSqlProvider {

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_MP_TBL_VPMS
     *
     * @mbg.generated Thu Feb 14 18:01:39 SGT 2019
     */
    public String countByExample(DspMpTblVpmsExample example) {
        SQL sql = new SQL();
        sql.SELECT("count(*)").FROM("DSP_MP_TBL_VPMS");
        applyWhere(sql, example, false);
        return sql.toString();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_MP_TBL_VPMS
     *
     * @mbg.generated Thu Feb 14 18:01:39 SGT 2019
     */
    public String deleteByExample(DspMpTblVpmsExample example) {
        SQL sql = new SQL();
        sql.DELETE_FROM("DSP_MP_TBL_VPMS");
        applyWhere(sql, example, false);
        return sql.toString();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_MP_TBL_VPMS
     *
     * @mbg.generated Thu Feb 14 18:01:39 SGT 2019
     */
    public String insertSelective(DspMpTblVpms record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("DSP_MP_TBL_VPMS");
        
        if (record.getId() != null) {
            sql.VALUES("ID", "#{id,jdbcType=DECIMAL}");
        }
        
        if (record.getDspQqIdReq() != null) {
            sql.VALUES("DSP_QQ_ID_REQ", "#{dspQqIdReq,jdbcType=DECIMAL}");
        }
        
        if (record.getLanguageReq() != null) {
            sql.VALUES("LANGUAGE_REQ", "#{languageReq,jdbcType=VARCHAR}");
        }
        
        if (record.getPlantypeReq() != null) {
            sql.VALUES("PLANTYPE_REQ", "#{plantypeReq,jdbcType=VARCHAR}");
        }
        
        if (record.getModeofpaymentReq() != null) {
            sql.VALUES("MODEOFPAYMENT_REQ", "#{modeofpaymentReq,jdbcType=VARCHAR}");
        }
        
        if (record.getOccupationReq() != null) {
            sql.VALUES("OCCUPATION_REQ", "#{occupationReq,jdbcType=VARCHAR}");
        }
        
        if (record.getDobReq() != null) {
            sql.VALUES("DOB_REQ", "#{dobReq,jdbcType=VARCHAR}");
        }
        
        if (record.getSmokerReq() != null) {
            sql.VALUES("SMOKER_REQ", "#{smokerReq,jdbcType=VARCHAR}");
        }
        
        if (record.getQuotationdateReq() != null) {
            sql.VALUES("QUOTATIONDATE_REQ", "#{quotationdateReq,jdbcType=VARCHAR}");
        }
        
        if (record.getNameReq() != null) {
            sql.VALUES("NAME_REQ", "#{nameReq,jdbcType=VARCHAR}");
        }
        
        if (record.getNricReq() != null) {
            sql.VALUES("NRIC_REQ", "#{nricReq,jdbcType=VARCHAR}");
        }
        
        if (record.getDeductibleReq() != null) {
            sql.VALUES("DEDUCTIBLE_REQ", "#{deductibleReq,jdbcType=VARCHAR}");
        }
        
        if (record.getStatusResp() != null) {
            sql.VALUES("STATUS_RESP", "#{statusResp,jdbcType=VARCHAR}");
        }
        
        if (record.getMessageResp() != null) {
            sql.VALUES("MESSAGE_RESP", "#{messageResp,jdbcType=VARCHAR}");
        }
        
        if (record.getVersionResp() != null) {
            sql.VALUES("VERSION_RESP", "#{versionResp,jdbcType=VARCHAR}");
        }
        
        if (record.getQuotationdateResp() != null) {
            sql.VALUES("QUOTATIONDATE_RESP", "#{quotationdateResp,jdbcType=VARCHAR}");
        }
        
        if (record.getPremiumpaymenttermResp() != null) {
            sql.VALUES("PREMIUMPAYMENTTERM_RESP", "#{premiumpaymenttermResp,jdbcType=VARCHAR}");
        }
        
        if (record.getMonthlypremiumResp() != null) {
            sql.VALUES("MONTHLYPREMIUM_RESP", "#{monthlypremiumResp,jdbcType=VARCHAR}");
        }
        
        if (record.getAnnualpremiumResp() != null) {
            sql.VALUES("ANNUALPREMIUM_RESP", "#{annualpremiumResp,jdbcType=VARCHAR}");
        }
        
        if (record.getNameResp() != null) {
            sql.VALUES("NAME_RESP", "#{nameResp,jdbcType=VARCHAR}");
        }
        
        if (record.getGenderResp() != null) {
            sql.VALUES("GENDER_RESP", "#{genderResp,jdbcType=VARCHAR}");
        }
        
        if (record.getSmokerResp() != null) {
            sql.VALUES("SMOKER_RESP", "#{smokerResp,jdbcType=VARCHAR}");
        }
        
        if (record.getOccupationclassResp() != null) {
            sql.VALUES("OCCUPATIONCLASS_RESP", "#{occupationclassResp,jdbcType=VARCHAR}");
        }
        
        if (record.getNricResp() != null) {
            sql.VALUES("NRIC_RESP", "#{nricResp,jdbcType=VARCHAR}");
        }
        
        if (record.getDobResp() != null) {
            sql.VALUES("DOB_RESP", "#{dobResp,jdbcType=VARCHAR}");
        }
        
        if (record.getAgenextbirthdayResp() != null) {
            sql.VALUES("AGENEXTBIRTHDAY_RESP", "#{agenextbirthdayResp,jdbcType=VARCHAR}");
        }
        
        if (record.getPremiummodebaseResp() != null) {
            sql.VALUES("PREMIUMMODEBASE_RESP", "#{premiummodebaseResp,jdbcType=VARCHAR}");
        }
        
        if (record.getPlannameResp() != null) {
            sql.VALUES("PLANNAME_RESP", "#{plannameResp,jdbcType=VARCHAR}");
        }
        
        if (record.getPolicytermResp() != null) {
            sql.VALUES("POLICYTERM_RESP", "#{policytermResp,jdbcType=VARCHAR}");
        }
        
        if (record.getPaymenttermResp() != null) {
            sql.VALUES("PAYMENTTERM_RESP", "#{paymenttermResp,jdbcType=VARCHAR}");
        }
        
        if (record.getPlantypeResp() != null) {
            sql.VALUES("PLANTYPE_RESP", "#{plantypeResp,jdbcType=VARCHAR}");
        }
        
        if (record.getDeductibleResp() != null) {
            sql.VALUES("DEDUCTIBLE_RESP", "#{deductibleResp,jdbcType=VARCHAR}");
        }
        
        if (record.getFirstyearpremiumResp() != null) {
            sql.VALUES("FIRSTYEARPREMIUM_RESP", "#{firstyearpremiumResp,jdbcType=VARCHAR}");
        }
        
        if (record.getPremiummodeResp() != null) {
            sql.VALUES("PREMIUMMODE_RESP", "#{premiummodeResp,jdbcType=VARCHAR}");
        }
        
        if (record.getEndcoveredageResp() != null) {
            sql.VALUES("ENDCOVEREDAGE_RESP", "#{endcoveredageResp,jdbcType=VARCHAR}");
        }
        
        if (record.getAnnualpremiumarrayResp() != null) {
            sql.VALUES("ANNUALPREMIUMARRAY_RESP", "#{annualpremiumarrayResp,jdbcType=VARCHAR}");
        }
        
        if (record.getTotalpremiumtodateResp() != null) {
            sql.VALUES("TOTALPREMIUMTODATE_RESP", "#{totalpremiumtodateResp,jdbcType=VARCHAR}");
        }
        
        if (record.getEndcoveredagearrayResp() != null) {
            sql.VALUES("ENDCOVEREDAGEARRAY_RESP", "#{endcoveredagearrayResp,jdbcType=VARCHAR}");
        }
        
        if (record.getPlanbenefitResp() != null) {
            sql.VALUES("PLANBENEFIT_RESP", "#{planbenefitResp,jdbcType=VARCHAR}");
        }
        
        if (record.getAnnuallimitResp() != null) {
            sql.VALUES("ANNUALLIMIT_RESP", "#{annuallimitResp,jdbcType=VARCHAR}");
        }
        
        if (record.getLifetimelimitResp() != null) {
            sql.VALUES("LIFETIMELIMIT_RESP", "#{lifetimelimitResp,jdbcType=VARCHAR}");
        }
        
        if (record.getRoomrateperdayResp() != null) {
            sql.VALUES("ROOMRATEPERDAY_RESP", "#{roomrateperdayResp,jdbcType=VARCHAR}");
        }
        
        if (record.getRoomannuallimitResp() != null) {
            sql.VALUES("ROOMANNUALLIMIT_RESP", "#{roomannuallimitResp,jdbcType=VARCHAR}");
        }
        
        if (record.getDeductibleoptionResp() != null) {
            sql.VALUES("DEDUCTIBLEOPTION_RESP", "#{deductibleoptionResp,jdbcType=VARCHAR}");
        }
        
        return sql.toString();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_MP_TBL_VPMS
     *
     * @mbg.generated Thu Feb 14 18:01:39 SGT 2019
     */
    public String selectByExample(DspMpTblVpmsExample example) {
        SQL sql = new SQL();
        if (example != null && example.isDistinct()) {
            sql.SELECT_DISTINCT("ID");
        } else {
            sql.SELECT("ID");
        }
        sql.SELECT("DSP_QQ_ID_REQ");
        sql.SELECT("LANGUAGE_REQ");
        sql.SELECT("PLANTYPE_REQ");
        sql.SELECT("MODEOFPAYMENT_REQ");
        sql.SELECT("OCCUPATION_REQ");
        sql.SELECT("DOB_REQ");
        sql.SELECT("SMOKER_REQ");
        sql.SELECT("QUOTATIONDATE_REQ");
        sql.SELECT("NAME_REQ");
        sql.SELECT("NRIC_REQ");
        sql.SELECT("DEDUCTIBLE_REQ");
        sql.SELECT("STATUS_RESP");
        sql.SELECT("MESSAGE_RESP");
        sql.SELECT("VERSION_RESP");
        sql.SELECT("QUOTATIONDATE_RESP");
        sql.SELECT("PREMIUMPAYMENTTERM_RESP");
        sql.SELECT("MONTHLYPREMIUM_RESP");
        sql.SELECT("ANNUALPREMIUM_RESP");
        sql.SELECT("NAME_RESP");
        sql.SELECT("GENDER_RESP");
        sql.SELECT("SMOKER_RESP");
        sql.SELECT("OCCUPATIONCLASS_RESP");
        sql.SELECT("NRIC_RESP");
        sql.SELECT("DOB_RESP");
        sql.SELECT("AGENEXTBIRTHDAY_RESP");
        sql.SELECT("PREMIUMMODEBASE_RESP");
        sql.SELECT("PLANNAME_RESP");
        sql.SELECT("POLICYTERM_RESP");
        sql.SELECT("PAYMENTTERM_RESP");
        sql.SELECT("PLANTYPE_RESP");
        sql.SELECT("DEDUCTIBLE_RESP");
        sql.SELECT("FIRSTYEARPREMIUM_RESP");
        sql.SELECT("PREMIUMMODE_RESP");
        sql.SELECT("ENDCOVEREDAGE_RESP");
        sql.SELECT("ANNUALPREMIUMARRAY_RESP");
        sql.SELECT("TOTALPREMIUMTODATE_RESP");
        sql.SELECT("ENDCOVEREDAGEARRAY_RESP");
        sql.SELECT("PLANBENEFIT_RESP");
        sql.SELECT("ANNUALLIMIT_RESP");
        sql.SELECT("LIFETIMELIMIT_RESP");
        sql.SELECT("ROOMRATEPERDAY_RESP");
        sql.SELECT("ROOMANNUALLIMIT_RESP");
        sql.SELECT("DEDUCTIBLEOPTION_RESP");
        sql.FROM("DSP_MP_TBL_VPMS");
        applyWhere(sql, example, false);
        
        if (example != null && example.getOrderByClause() != null) {
            sql.ORDER_BY(example.getOrderByClause());
        }
        
        return sql.toString();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_MP_TBL_VPMS
     *
     * @mbg.generated Thu Feb 14 18:01:39 SGT 2019
     */
    public String updateByExampleSelective(Map<String, Object> parameter) {
        DspMpTblVpms record = (DspMpTblVpms) parameter.get("record");
        DspMpTblVpmsExample example = (DspMpTblVpmsExample) parameter.get("example");
        
        SQL sql = new SQL();
        sql.UPDATE("DSP_MP_TBL_VPMS");
        
        if (record.getId() != null) {
            sql.SET("ID = #{record.id,jdbcType=DECIMAL}");
        }
        
        if (record.getDspQqIdReq() != null) {
            sql.SET("DSP_QQ_ID_REQ = #{record.dspQqIdReq,jdbcType=DECIMAL}");
        }
        
        if (record.getLanguageReq() != null) {
            sql.SET("LANGUAGE_REQ = #{record.languageReq,jdbcType=VARCHAR}");
        }
        
        if (record.getPlantypeReq() != null) {
            sql.SET("PLANTYPE_REQ = #{record.plantypeReq,jdbcType=VARCHAR}");
        }
        
        if (record.getModeofpaymentReq() != null) {
            sql.SET("MODEOFPAYMENT_REQ = #{record.modeofpaymentReq,jdbcType=VARCHAR}");
        }
        
        if (record.getOccupationReq() != null) {
            sql.SET("OCCUPATION_REQ = #{record.occupationReq,jdbcType=VARCHAR}");
        }
        
        if (record.getDobReq() != null) {
            sql.SET("DOB_REQ = #{record.dobReq,jdbcType=VARCHAR}");
        }
        
        if (record.getSmokerReq() != null) {
            sql.SET("SMOKER_REQ = #{record.smokerReq,jdbcType=VARCHAR}");
        }
        
        if (record.getQuotationdateReq() != null) {
            sql.SET("QUOTATIONDATE_REQ = #{record.quotationdateReq,jdbcType=VARCHAR}");
        }
        
        if (record.getNameReq() != null) {
            sql.SET("NAME_REQ = #{record.nameReq,jdbcType=VARCHAR}");
        }
        
        if (record.getNricReq() != null) {
            sql.SET("NRIC_REQ = #{record.nricReq,jdbcType=VARCHAR}");
        }
        
        if (record.getDeductibleReq() != null) {
            sql.SET("DEDUCTIBLE_REQ = #{record.deductibleReq,jdbcType=VARCHAR}");
        }
        
        if (record.getStatusResp() != null) {
            sql.SET("STATUS_RESP = #{record.statusResp,jdbcType=VARCHAR}");
        }
        
        if (record.getMessageResp() != null) {
            sql.SET("MESSAGE_RESP = #{record.messageResp,jdbcType=VARCHAR}");
        }
        
        if (record.getVersionResp() != null) {
            sql.SET("VERSION_RESP = #{record.versionResp,jdbcType=VARCHAR}");
        }
        
        if (record.getQuotationdateResp() != null) {
            sql.SET("QUOTATIONDATE_RESP = #{record.quotationdateResp,jdbcType=VARCHAR}");
        }
        
        if (record.getPremiumpaymenttermResp() != null) {
            sql.SET("PREMIUMPAYMENTTERM_RESP = #{record.premiumpaymenttermResp,jdbcType=VARCHAR}");
        }
        
        if (record.getMonthlypremiumResp() != null) {
            sql.SET("MONTHLYPREMIUM_RESP = #{record.monthlypremiumResp,jdbcType=VARCHAR}");
        }
        
        if (record.getAnnualpremiumResp() != null) {
            sql.SET("ANNUALPREMIUM_RESP = #{record.annualpremiumResp,jdbcType=VARCHAR}");
        }
        
        if (record.getNameResp() != null) {
            sql.SET("NAME_RESP = #{record.nameResp,jdbcType=VARCHAR}");
        }
        
        if (record.getGenderResp() != null) {
            sql.SET("GENDER_RESP = #{record.genderResp,jdbcType=VARCHAR}");
        }
        
        if (record.getSmokerResp() != null) {
            sql.SET("SMOKER_RESP = #{record.smokerResp,jdbcType=VARCHAR}");
        }
        
        if (record.getOccupationclassResp() != null) {
            sql.SET("OCCUPATIONCLASS_RESP = #{record.occupationclassResp,jdbcType=VARCHAR}");
        }
        
        if (record.getNricResp() != null) {
            sql.SET("NRIC_RESP = #{record.nricResp,jdbcType=VARCHAR}");
        }
        
        if (record.getDobResp() != null) {
            sql.SET("DOB_RESP = #{record.dobResp,jdbcType=VARCHAR}");
        }
        
        if (record.getAgenextbirthdayResp() != null) {
            sql.SET("AGENEXTBIRTHDAY_RESP = #{record.agenextbirthdayResp,jdbcType=VARCHAR}");
        }
        
        if (record.getPremiummodebaseResp() != null) {
            sql.SET("PREMIUMMODEBASE_RESP = #{record.premiummodebaseResp,jdbcType=VARCHAR}");
        }
        
        if (record.getPlannameResp() != null) {
            sql.SET("PLANNAME_RESP = #{record.plannameResp,jdbcType=VARCHAR}");
        }
        
        if (record.getPolicytermResp() != null) {
            sql.SET("POLICYTERM_RESP = #{record.policytermResp,jdbcType=VARCHAR}");
        }
        
        if (record.getPaymenttermResp() != null) {
            sql.SET("PAYMENTTERM_RESP = #{record.paymenttermResp,jdbcType=VARCHAR}");
        }
        
        if (record.getPlantypeResp() != null) {
            sql.SET("PLANTYPE_RESP = #{record.plantypeResp,jdbcType=VARCHAR}");
        }
        
        if (record.getDeductibleResp() != null) {
            sql.SET("DEDUCTIBLE_RESP = #{record.deductibleResp,jdbcType=VARCHAR}");
        }
        
        if (record.getFirstyearpremiumResp() != null) {
            sql.SET("FIRSTYEARPREMIUM_RESP = #{record.firstyearpremiumResp,jdbcType=VARCHAR}");
        }
        
        if (record.getPremiummodeResp() != null) {
            sql.SET("PREMIUMMODE_RESP = #{record.premiummodeResp,jdbcType=VARCHAR}");
        }
        
        if (record.getEndcoveredageResp() != null) {
            sql.SET("ENDCOVEREDAGE_RESP = #{record.endcoveredageResp,jdbcType=VARCHAR}");
        }
        
        if (record.getAnnualpremiumarrayResp() != null) {
            sql.SET("ANNUALPREMIUMARRAY_RESP = #{record.annualpremiumarrayResp,jdbcType=VARCHAR}");
        }
        
        if (record.getTotalpremiumtodateResp() != null) {
            sql.SET("TOTALPREMIUMTODATE_RESP = #{record.totalpremiumtodateResp,jdbcType=VARCHAR}");
        }
        
        if (record.getEndcoveredagearrayResp() != null) {
            sql.SET("ENDCOVEREDAGEARRAY_RESP = #{record.endcoveredagearrayResp,jdbcType=VARCHAR}");
        }
        
        if (record.getPlanbenefitResp() != null) {
            sql.SET("PLANBENEFIT_RESP = #{record.planbenefitResp,jdbcType=VARCHAR}");
        }
        
        if (record.getAnnuallimitResp() != null) {
            sql.SET("ANNUALLIMIT_RESP = #{record.annuallimitResp,jdbcType=VARCHAR}");
        }
        
        if (record.getLifetimelimitResp() != null) {
            sql.SET("LIFETIMELIMIT_RESP = #{record.lifetimelimitResp,jdbcType=VARCHAR}");
        }
        
        if (record.getRoomrateperdayResp() != null) {
            sql.SET("ROOMRATEPERDAY_RESP = #{record.roomrateperdayResp,jdbcType=VARCHAR}");
        }
        
        if (record.getRoomannuallimitResp() != null) {
            sql.SET("ROOMANNUALLIMIT_RESP = #{record.roomannuallimitResp,jdbcType=VARCHAR}");
        }
        
        if (record.getDeductibleoptionResp() != null) {
            sql.SET("DEDUCTIBLEOPTION_RESP = #{record.deductibleoptionResp,jdbcType=VARCHAR}");
        }
        
        applyWhere(sql, example, true);
        return sql.toString();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_MP_TBL_VPMS
     *
     * @mbg.generated Thu Feb 14 18:01:39 SGT 2019
     */
    public String updateByExample(Map<String, Object> parameter) {
        SQL sql = new SQL();
        sql.UPDATE("DSP_MP_TBL_VPMS");
        
        sql.SET("ID = #{record.id,jdbcType=DECIMAL}");
        sql.SET("DSP_QQ_ID_REQ = #{record.dspQqIdReq,jdbcType=DECIMAL}");
        sql.SET("LANGUAGE_REQ = #{record.languageReq,jdbcType=VARCHAR}");
        sql.SET("PLANTYPE_REQ = #{record.plantypeReq,jdbcType=VARCHAR}");
        sql.SET("MODEOFPAYMENT_REQ = #{record.modeofpaymentReq,jdbcType=VARCHAR}");
        sql.SET("OCCUPATION_REQ = #{record.occupationReq,jdbcType=VARCHAR}");
        sql.SET("DOB_REQ = #{record.dobReq,jdbcType=VARCHAR}");
        sql.SET("SMOKER_REQ = #{record.smokerReq,jdbcType=VARCHAR}");
        sql.SET("QUOTATIONDATE_REQ = #{record.quotationdateReq,jdbcType=VARCHAR}");
        sql.SET("NAME_REQ = #{record.nameReq,jdbcType=VARCHAR}");
        sql.SET("NRIC_REQ = #{record.nricReq,jdbcType=VARCHAR}");
        sql.SET("DEDUCTIBLE_REQ = #{record.deductibleReq,jdbcType=VARCHAR}");
        sql.SET("STATUS_RESP = #{record.statusResp,jdbcType=VARCHAR}");
        sql.SET("MESSAGE_RESP = #{record.messageResp,jdbcType=VARCHAR}");
        sql.SET("VERSION_RESP = #{record.versionResp,jdbcType=VARCHAR}");
        sql.SET("QUOTATIONDATE_RESP = #{record.quotationdateResp,jdbcType=VARCHAR}");
        sql.SET("PREMIUMPAYMENTTERM_RESP = #{record.premiumpaymenttermResp,jdbcType=VARCHAR}");
        sql.SET("MONTHLYPREMIUM_RESP = #{record.monthlypremiumResp,jdbcType=VARCHAR}");
        sql.SET("ANNUALPREMIUM_RESP = #{record.annualpremiumResp,jdbcType=VARCHAR}");
        sql.SET("NAME_RESP = #{record.nameResp,jdbcType=VARCHAR}");
        sql.SET("GENDER_RESP = #{record.genderResp,jdbcType=VARCHAR}");
        sql.SET("SMOKER_RESP = #{record.smokerResp,jdbcType=VARCHAR}");
        sql.SET("OCCUPATIONCLASS_RESP = #{record.occupationclassResp,jdbcType=VARCHAR}");
        sql.SET("NRIC_RESP = #{record.nricResp,jdbcType=VARCHAR}");
        sql.SET("DOB_RESP = #{record.dobResp,jdbcType=VARCHAR}");
        sql.SET("AGENEXTBIRTHDAY_RESP = #{record.agenextbirthdayResp,jdbcType=VARCHAR}");
        sql.SET("PREMIUMMODEBASE_RESP = #{record.premiummodebaseResp,jdbcType=VARCHAR}");
        sql.SET("PLANNAME_RESP = #{record.plannameResp,jdbcType=VARCHAR}");
        sql.SET("POLICYTERM_RESP = #{record.policytermResp,jdbcType=VARCHAR}");
        sql.SET("PAYMENTTERM_RESP = #{record.paymenttermResp,jdbcType=VARCHAR}");
        sql.SET("PLANTYPE_RESP = #{record.plantypeResp,jdbcType=VARCHAR}");
        sql.SET("DEDUCTIBLE_RESP = #{record.deductibleResp,jdbcType=VARCHAR}");
        sql.SET("FIRSTYEARPREMIUM_RESP = #{record.firstyearpremiumResp,jdbcType=VARCHAR}");
        sql.SET("PREMIUMMODE_RESP = #{record.premiummodeResp,jdbcType=VARCHAR}");
        sql.SET("ENDCOVEREDAGE_RESP = #{record.endcoveredageResp,jdbcType=VARCHAR}");
        sql.SET("ANNUALPREMIUMARRAY_RESP = #{record.annualpremiumarrayResp,jdbcType=VARCHAR}");
        sql.SET("TOTALPREMIUMTODATE_RESP = #{record.totalpremiumtodateResp,jdbcType=VARCHAR}");
        sql.SET("ENDCOVEREDAGEARRAY_RESP = #{record.endcoveredagearrayResp,jdbcType=VARCHAR}");
        sql.SET("PLANBENEFIT_RESP = #{record.planbenefitResp,jdbcType=VARCHAR}");
        sql.SET("ANNUALLIMIT_RESP = #{record.annuallimitResp,jdbcType=VARCHAR}");
        sql.SET("LIFETIMELIMIT_RESP = #{record.lifetimelimitResp,jdbcType=VARCHAR}");
        sql.SET("ROOMRATEPERDAY_RESP = #{record.roomrateperdayResp,jdbcType=VARCHAR}");
        sql.SET("ROOMANNUALLIMIT_RESP = #{record.roomannuallimitResp,jdbcType=VARCHAR}");
        sql.SET("DEDUCTIBLEOPTION_RESP = #{record.deductibleoptionResp,jdbcType=VARCHAR}");
        
        DspMpTblVpmsExample example = (DspMpTblVpmsExample) parameter.get("example");
        applyWhere(sql, example, true);
        return sql.toString();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_MP_TBL_VPMS
     *
     * @mbg.generated Thu Feb 14 18:01:39 SGT 2019
     */
    protected void applyWhere(SQL sql, DspMpTblVpmsExample example, boolean includeExamplePhrase) {
        if (example == null) {
            return;
        }
        
        String parmPhrase1;
        String parmPhrase1_th;
        String parmPhrase2;
        String parmPhrase2_th;
        String parmPhrase3;
        String parmPhrase3_th;
        if (includeExamplePhrase) {
            parmPhrase1 = "%s #{example.oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{example.oredCriteria[%d].allCriteria[%d].value} and #{example.oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{example.oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{example.oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{example.oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        } else {
            parmPhrase1 = "%s #{oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{oredCriteria[%d].allCriteria[%d].value} and #{oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        }
        
        StringBuilder sb = new StringBuilder();
        List<Criteria> oredCriteria = example.getOredCriteria();
        boolean firstCriteria = true;
        for (int i = 0; i < oredCriteria.size(); i++) {
            Criteria criteria = oredCriteria.get(i);
            if (criteria.isValid()) {
                if (firstCriteria) {
                    firstCriteria = false;
                } else {
                    sb.append(" or ");
                }
                
                sb.append('(');
                List<Criterion> criterions = criteria.getAllCriteria();
                boolean firstCriterion = true;
                for (int j = 0; j < criterions.size(); j++) {
                    Criterion criterion = criterions.get(j);
                    if (firstCriterion) {
                        firstCriterion = false;
                    } else {
                        sb.append(" and ");
                    }
                    
                    if (criterion.isNoValue()) {
                        sb.append(criterion.getCondition());
                    } else if (criterion.isSingleValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase1, criterion.getCondition(), i, j));
                        } else {
                            sb.append(String.format(parmPhrase1_th, criterion.getCondition(), i, j,criterion.getTypeHandler()));
                        }
                    } else if (criterion.isBetweenValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase2, criterion.getCondition(), i, j, i, j));
                        } else {
                            sb.append(String.format(parmPhrase2_th, criterion.getCondition(), i, j, criterion.getTypeHandler(), i, j, criterion.getTypeHandler()));
                        }
                    } else if (criterion.isListValue()) {
                        sb.append(criterion.getCondition());
                        sb.append(" (");
                        List<?> listItems = (List<?>) criterion.getValue();
                        boolean comma = false;
                        for (int k = 0; k < listItems.size(); k++) {
                            if (comma) {
                                sb.append(", ");
                            } else {
                                comma = true;
                            }
                            if (criterion.getTypeHandler() == null) {
                                sb.append(String.format(parmPhrase3, i, j, k));
                            } else {
                                sb.append(String.format(parmPhrase3_th, i, j, k, criterion.getTypeHandler()));
                            }
                        }
                        sb.append(')');
                    }
                }
                sb.append(')');
            }
        }
        
        if (sb.length() > 0) {
            sql.WHERE(sb.toString());
        }
    }
}