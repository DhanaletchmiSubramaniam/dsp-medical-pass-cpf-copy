package com.etiqa.medicalpass.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.mapping.StatementType;

import com.etiqa.medicalpass.pojo.PaymentInsert;

public interface PaymentMapper {

	@Insert({
			"{ call DSP_MP_SP_PAYMENT_INSERT (",
			"#{dspQqId, mode=IN, jdbcType=DECIMAL },  ",
			"#{paymentGatewayCode, mode=IN, jdbcType=VARCHAR},  ",
			"#{amount, mode=IN, jdbcType=VARCHAR},  ",
			"#{paymentStatus, mode=IN, jdbcType=VARCHAR},  ",
			"#{productCode, mode=IN, jdbcType=VARCHAR},  ",
			"#{productPlanCode, mode=IN, jdbcType=VARCHAR},  ",
			"#{transactionId, mode=OUT, jdbcType=VARCHAR},  ",
			"#{policyNumber, mode=OUT, jdbcType=VARCHAR},  ",
			"#{receiptNumber, mode=OUT, jdbcType=VARCHAR} ) }" })
	@Options(statementType = StatementType.CALLABLE)
	@ResultType(PaymentInsert.class)
	void insertPayment(PaymentInsert record);
}