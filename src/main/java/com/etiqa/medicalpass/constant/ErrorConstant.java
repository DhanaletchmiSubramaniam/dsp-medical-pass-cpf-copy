package com.etiqa.medicalpass.constant;

public class ErrorConstant {

	private ErrorConstant() {
	}

	public static final String ERROR_PATH = "/error";

}
