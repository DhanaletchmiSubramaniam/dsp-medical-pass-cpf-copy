package com.etiqa.medicalpass.constant;

public class QQConstant {

	private QQConstant() {
	}

	public static final String ORACLE_DATE_FORMAT = "dd/MM/yyyy HH:mm:ss";
	public static final String DOB_FORM_DATE_FORMAT = "dd/MM/yyyy";
	public static final String PDF_DATE_FORMAT = "dd-MM-yyyy  h:mm:ss a";

	public static final String PRODUCT_NAME = "e-Medical Pass";
	public static final String PRODUCT_NAME_INSURANCE = "e-Medical Pass";
	public static final String PRODUCT_NAME_TAKAFUL = "e-Medical Pass Takaful";

	public static final String VPMS_REQUEST_DEFAULT_NRIC = "000000000000";
	public static final String VPMS_REQUEST_DEFAULT_NAME = "AAAAAAAAAAAA";
	public static final String VPMS_REQUEST_DEFAULT_IS_SMOKER = "N";
	public static final String VPMS_REQUEST_DEFAULT_LANG = "E";
	public static final String VPMS_RINGGIT_MALAYSIA = "RM";

	public static final String PAYMENT_TYPE_CODE_MONTHLY = "12";
	public static final String PAYMENT_TYPE_CODE_ANNUALLY = "1";
	public static final String PAYMENT_TYPE_MONTHLY = "monthly";
	public static final String PAYMENT_TYPE_ANNUALLY = "annually";

	public static final String CUSTOMER_ID_TYPE_NRIC = "001";
	public static final String INSURANCE_COMPANY_KEY = "I";
	public static final String TAKAFUL_COMPANY_KEY = "T";

	public static final String INSURANCE_PRODUCT_CODE_SEARCH_KEY = "PCTM0%";
	public static final String TAKAFUL_PRODUCT_CODE_SEARCH_KEY = "PTTM0%";
	
	public static final String INSURANCE_POLICY_PREFIX_SEARCH_KEY = "DMI";
	public static final String TAKAFUL_POLICY_PREFIX_SEARCH_KEY = "DMT";
	
	public static final String NATIONALITY_SEARCH_KEY_MALAYSIAN = "mal";
	public static final String NATIONALITY_SEARCH_KEY_NON_MALAYSIAN = "nonmal";
	public static final String NATIONALITY_VALUE_MALAYSIAN = "Malaysian";
	public static final String NATIONALITY_VALUE_NON_MALAYSIAN = "Non-Malaysian";
	
	public static final String PRODUCT_CODE_SILVER_KEY = "1";
	public static final String PRODUCT_CODE_GOLD_KEY = "2";
	public static final String PRODUCT_CODE_PLATINUM_KEY = "3";
	
	public static final String INSURANCE_PRODUCT_CODE_SEARCH_KEY_STRING = "PCTM0";
	public static final String TAKAFUL_PRODUCT_CODE_SEARCH_KEY_STRING = "PTTM0";

	public static final String INSURANCE_PRODUCT_CODE_SILVER = "PCTM01";
	public static final String INSURANCE_PRODUCT_CODE_GOLD = "PCTM02";
	public static final String INSURANCE_PRODUCT_CODE_PLATINUM = "PCTM03";

	public static final String PRODUCT_CODE_SILVER_SEARCH_KEY = "01";
	public static final String PRODUCT_CODE_GOLD_SEARCH_KEY = "02";
	public static final String PRODUCT_CODE_PLATINUM_SEARCH_KEY = "03";

	public static final String PLAN_TYPE_SILVER = "Silver";
	public static final String PLAN_TYPE_GOLD = "Gold";
	public static final String PLAN_TYPE_PLATINUM = "Platinum";

	public static final String TAKAFUL_PRODUCT_CODE_SILVER = "PTTM01";
	public static final String TAKAFUL_PRODUCT_CODE_GOLD = "PTTM02";
	public static final String TAKAFUL_PRODUCT_CODE_PLATINUM = "PTTM03";

	public static final String PREMIUM_NUMBER_FORMAT = "###,###,##0.00";
	public static final String SUM_COVERAGE_NUMBER_FORMAT = "###,###,##0";
	public static final String BMI_DOUBLE_FORMAT = "#0.00";

	public static final String BMI_KEY = "BMI";

	public static final String ENGLISH_KEY = "E";
	public static final String BAHASA_KEY = "B";

	public static final String SPLIT_FIRST_LEVEL = "\\|";
	public static final String SPLIT_SECOND_LEVEL = ",";

	public static final String OCCUPATION_SESSION_KEY = "occupationSession";
	public static final String COMPANY_SESSION_KEY = "companySession";
	public static final String VPMS_REQUEST_SESSION_KEY = "vpmsRequest";
	public static final String VPMS_RESPONSE_SESSION_KEY = "vpmsResponse";
	public static final String MP_QQ_DATA_SESSION_KEY = "mpQQData";
	public static final String DSP_QQ_ID_DATA_SESSION_KEY = "dspIdData";
	public static final String MDM_DATA_SESSION_KEY = "mdmData";
	public static final String COMMON_CUSTOMER_DATA_SESSION_KEY = "commonCustData";
	public static final String IS_REJECTED_SESSION_KEY = "isRejected";
	public static final String PAYMENT_TRANSACTION_ID_SESSION_KEY = "transactionId";
	public static final String RESPONSE_HASH_SESSION_KEY = "responseHash";
	public static final String PAYMENT_RESPONSE_SESSION_KEY = "paymentResponse";
	public static final String BANK_LIST_SESSION_KEY = "bankList";

	public static final String[] CACHE_KEYS = { MP_QQ_DATA_SESSION_KEY, DSP_QQ_ID_DATA_SESSION_KEY,
			MDM_DATA_SESSION_KEY, IS_REJECTED_SESSION_KEY, VPMS_REQUEST_SESSION_KEY, VPMS_RESPONSE_SESSION_KEY,
			COMMON_CUSTOMER_DATA_SESSION_KEY, PAYMENT_TRANSACTION_ID_SESSION_KEY, RESPONSE_HASH_SESSION_KEY,
			PAYMENT_RESPONSE_SESSION_KEY, COMPANY_SESSION_KEY };
	
	public static final String[] CACHE_KEYS_EXCEPT_REJECTED = { MP_QQ_DATA_SESSION_KEY, DSP_QQ_ID_DATA_SESSION_KEY,
			MDM_DATA_SESSION_KEY, VPMS_REQUEST_SESSION_KEY, VPMS_RESPONSE_SESSION_KEY,
			COMMON_CUSTOMER_DATA_SESSION_KEY, PAYMENT_TRANSACTION_ID_SESSION_KEY, RESPONSE_HASH_SESSION_KEY,
			PAYMENT_RESPONSE_SESSION_KEY, COMPANY_SESSION_KEY };

	public static final String GENDER_MALE = "M";
	public static final String MALE_CODE = "001";
	public static final String GENDER_FEMALE = "F";
	public static final String FEMALE_CODE = "002";
	public static final String MALE_TITLE = "Mr.";
	public static final String FEMALE_TITLE = "Ms.";
	public static final String FEMALE = "Female";
	public static final String MALE = "Male";

	public static final String PAYMENT_GATEWAY_HASH_KEY_CONTINUE = "Continue";
	public static final String PAYMENT_RESPOND_CODE_SUCCESS = "0";
	public static final String ETIQA_MID_CODE_INSURANCE = "0000001076";
	public static final String ETIQA_MID_CODE_TAKAFUL = "0000000210";
	public static final String PAYMENT_GATEWAY_TRANSACTION_CODE = "MPAY";
	public static final String PAYMENT_GATEWAY_CODE_INSURANCE = "CREDIT CARD";
	public static final String PAYMENT_GATEWAY_CODE_TAKAFUL = "CREDIT CARD IDS";
	public static final String PAYMENT_GATEWAY_SUCCESS_CODE = "0";
	public static final String PAYMENT_PRODUCT_NAME_INSURANCE = "MP";
	public static final String PAYMENT_PRODUCT_NAME_TAKAFUL = "MPT";
	public static final String MPAY_HASH_KEY_INSURANCE = "ETHHJ8U5NSTY0ADJ3V9XWE17F5BL7OF5";
	public static final String MPAY_HASH_KEY_TAKAFUL = "UN9E01V773M488158CZEDBT7N42K3L61";
	public static final String VERIFY_PAYMENT_ENDPOINT = "verify";
	public static final String PAYMENT_STATUS_ATTEMPT_PAYMENT = "AP";
	public static final String PAYMENT_STATUS_SUCCESS = "S";
	public static final String PAYMENT_STATUS_FAILED = "F";

	public static final String AGENT_CODE_INSURANCE = "8OI00003";
	public static final String AGENT_CODE_TAKAFUL = "8OT00003";
	public static final String FORM_NO_CODE_INSURANCE = "eMEDP";
	public static final String FORM_NO_CODE_TAKAFUL = "eMEDPTKF";
	public static final String FORM_NO_CODE_SILVER = "01";
	public static final String FORM_NO_CODE_GOLD = "02";
	public static final String FORM_NO_CODE_PLATINUM = "03";


	public static final String PROPERTIES_FILE_NAME = "application.properties";

	public static final String VPMS_TAKAFUL_URL_KEY = "vpms.medicalpass.takaful.url";
	public static final String VPMS_INSURANCE_URL_KEY = "vpms.medicalpass.insurance.url";
	public static final String VPMS_PASS_KEY = "vpms.medicalpass.password";

	public static final String CPF_MDM_URL_KEY = "cpf.mdm.url";

	public static final String MPAY_PAYMENT_URL_KEY = "mpay.payment.url";

	public static final String PAGE_DIRECTORY_START_PAYMENT = "payment";

	public static final String PAGE_DIRECTORY_ERROR_INSURANCE_EN = "salesfunnel/en/error-insurance";
	public static final String PAGE_DIRECTORY_INSURANCE_EN_URL_MASK = "salesfunnel/en/insurance-en";
	public static final String PAGE_DIRECTORY_TAKAFUL_EN_URL_MASK = "salesfunnel/en/takaful-en";
	
	public static final String PAGE_DIRECTORY_INSURACE_QQ1_EN = "salesfunnel/en/mpi-qq1";
	public static final String PAGE_DIRECTORY_INSURACE_QQ2A_EN = "salesfunnel/en/mpi-qq2a";
	public static final String PAGE_DIRECTORY_INSURACE_QQ2B_EN = "salesfunnel/en/mpi-qq2b";
	public static final String PAGE_DIRECTORY_INSURACE_QQ3_EN = "salesfunnel/en/mpi-qq3";
	public static final String PAGE_DIRECTORY_INSURACE_QQ4_SUCCESS_EN = "salesfunnel/en/mpi-qq4-success";
	public static final String PAGE_DIRECTORY_INSURACE_QQ4_FAILED_EN = "salesfunnel/en/mpi-qq4-failed";
	
	public static final String PAGE_DIRECTORY_TAKAFUL_QQ1_EN = "salesfunnel/en/mpt-qq1";
	public static final String PAGE_DIRECTORY_TAKAFUL_QQ2A_EN = "salesfunnel/en/mpt-qq2a";
	public static final String PAGE_DIRECTORY_TAKAFUL_QQ2B_EN = "salesfunnel/en/mpt-qq2b";
	public static final String PAGE_DIRECTORY_TAKAFUL_QQ3_EN = "salesfunnel/en/mpt-qq3";
	public static final String PAGE_DIRECTORY_TAKAFUL_QQ4_SUCCESS_EN = "salesfunnel/en/mpt-qq4-success";
	public static final String PAGE_DIRECTORY_TAKAFUL_QQ4_FAILED_EN = "salesfunnel/en/mpt-qq4-failed";
	
	
	public static final String REQUEST_MAPPING_INDEX = "/index";
	public static final String REQUEST_MAPPING_ENGLISH = "/en";
	public static final String REQUEST_MAPPING_BAHASA = "/my";
	public static final String REQUEST_MAPPING_QQ_PREFIX = "/medical-pass";
	public static final String REQUEST_MAPPING_QQ_ERROR = "/error";
	public static final String REQUEST_MAPPING_QQ_INSURANCE_PREFIX = "/insurance";
	public static final String REQUEST_MAPPING_QQ_TAKAFUL_PREFIX = "/takaful";
	public static final String REQUEST_MAPPING_QQ_ONE = "/qq1";
	public static final String REQUEST_MAPPING_QQ_TWO_A = "/qq2a";
	public static final String REQUEST_MAPPING_QQ_TWO_B = "/qq2b";
	public static final String REQUEST_MAPPING_QQ_THREE = "/qq3";
	public static final String REQUEST_MAPPING_QQ_FOUR_SUCCESS = "/qq4-success";
	public static final String REQUEST_MAPPING_QQ_FOUR_FAILED = "/qq4-failed";
	public static final String REQUEST_MAPPING_QQ_FOUR_SUCCESS_PARAM = "?myParam=1";
	public static final String REQUEST_MAPPING_QQ_FOUR_FAILED_PARAM = "?myParam=2";
	public static final String REQUEST_MAPPING_QQ_ENDPOINT_PREFIX = "/qq";
	public static final String REQUEST_MAPPING_QQ_ENDPOINT_REJECTION = "/rejection";
	public static final String REQUEST_MAPPING_QQ_ENDPOINT_REJECTION_UNDERWRITING = "/rejection-uw";
	public static final String REQUEST_MAPPING_QQ_ENDPOINT_OCCUPATION = "/occupation";
	public static final String REQUEST_MAPPING_QQ_ENDPOINT_BANK_ACCOUNT = "/bank-account";
	public static final String REQUEST_MAPPING_QQ_ENDPOINT_ONE = "/1";
	public static final String REQUEST_MAPPING_QQ_ENDPOINT_TWO_A = "/2a";
	public static final String REQUEST_MAPPING_QQ_ENDPOINT_TWO_B = "/2b";
	public static final String REQUEST_MAPPING_QQ_ENDPOINT_FOUR = "/4";
	public static final String REQUEST_MAPPING_PAYMENT_PREFIX = "/payment";
	public static final String REQUEST_MAPPING_PAYMENT_START = "/start";
	public static final String REQUEST_MAPPING_PAYMENT_VERIFY = "/verify";

	public static final String PAGE_URL_VERIFY_PAYMENT = REQUEST_MAPPING_PAYMENT_PREFIX + REQUEST_MAPPING_PAYMENT_VERIFY;
	public static final String PAGE_URL_ERROR_EN = REQUEST_MAPPING_QQ_PREFIX + REQUEST_MAPPING_ENGLISH + REQUEST_MAPPING_QQ_ERROR;
	
	public static final String PAGE_URL_INSURACE_QQ1_EN = REQUEST_MAPPING_QQ_INSURANCE_PREFIX + REQUEST_MAPPING_ENGLISH + REQUEST_MAPPING_QQ_ONE;
	public static final String PAGE_URL_INSURACE_QQ2A_EN = REQUEST_MAPPING_QQ_INSURANCE_PREFIX + REQUEST_MAPPING_ENGLISH + REQUEST_MAPPING_QQ_TWO_A;
	public static final String PAGE_URL_INSURACE_QQ2B_EN = REQUEST_MAPPING_QQ_INSURANCE_PREFIX + REQUEST_MAPPING_ENGLISH + REQUEST_MAPPING_QQ_TWO_B;
	public static final String PAGE_URL_INSURACE_QQ3_EN = REQUEST_MAPPING_QQ_INSURANCE_PREFIX + REQUEST_MAPPING_ENGLISH + REQUEST_MAPPING_QQ_THREE;
	public static final String PAGE_URL_INSURACE_QQ4_SUCCESS_EN = REQUEST_MAPPING_QQ_INSURANCE_PREFIX + REQUEST_MAPPING_ENGLISH + REQUEST_MAPPING_QQ_FOUR_SUCCESS;
	public static final String PAGE_URL_INSURACE_QQ4_SUCCESS_EN_MASKED = REQUEST_MAPPING_QQ_PREFIX + REQUEST_MAPPING_ENGLISH + REQUEST_MAPPING_QQ_INSURANCE_PREFIX + REQUEST_MAPPING_QQ_FOUR_SUCCESS_PARAM;
	public static final String PAGE_URL_INSURACE_QQ4_FAILED_EN = REQUEST_MAPPING_QQ_INSURANCE_PREFIX + REQUEST_MAPPING_ENGLISH + REQUEST_MAPPING_QQ_FOUR_FAILED;
	public static final String PAGE_URL_INSURACE_QQ4_FAILED_EN_MASKED = REQUEST_MAPPING_QQ_PREFIX + REQUEST_MAPPING_ENGLISH + REQUEST_MAPPING_QQ_INSURANCE_PREFIX + REQUEST_MAPPING_QQ_FOUR_FAILED_PARAM;

	public static final String PAGE_URL_TAKAFUL_QQ1_EN = REQUEST_MAPPING_QQ_TAKAFUL_PREFIX + REQUEST_MAPPING_ENGLISH + REQUEST_MAPPING_QQ_ONE;
	public static final String PAGE_URL_TAKAFUL_QQ2A_EN = REQUEST_MAPPING_QQ_TAKAFUL_PREFIX + REQUEST_MAPPING_ENGLISH + REQUEST_MAPPING_QQ_TWO_A;
	public static final String PAGE_URL_TAKAFUL_QQ2B_EN = REQUEST_MAPPING_QQ_TAKAFUL_PREFIX + REQUEST_MAPPING_ENGLISH + REQUEST_MAPPING_QQ_TWO_B;
	public static final String PAGE_URL_TAKAFUL_QQ3_EN = REQUEST_MAPPING_QQ_TAKAFUL_PREFIX + REQUEST_MAPPING_ENGLISH + REQUEST_MAPPING_QQ_THREE;
	public static final String PAGE_URL_TAKAFUL_QQ4_SUCCESS_EN = REQUEST_MAPPING_QQ_TAKAFUL_PREFIX + REQUEST_MAPPING_ENGLISH + REQUEST_MAPPING_QQ_FOUR_SUCCESS;
	public static final String PAGE_URL_TAKAFUL_QQ4_SUCCESS_EN_MASKED = REQUEST_MAPPING_QQ_PREFIX + REQUEST_MAPPING_ENGLISH + REQUEST_MAPPING_QQ_TAKAFUL_PREFIX + REQUEST_MAPPING_QQ_FOUR_SUCCESS_PARAM;
	public static final String PAGE_URL_TAKAFUL_QQ4_FAILED_EN = REQUEST_MAPPING_QQ_TAKAFUL_PREFIX + REQUEST_MAPPING_ENGLISH + REQUEST_MAPPING_QQ_FOUR_FAILED;
	public static final String PAGE_URL_TAKAFUL_QQ4_FAILED_EN_MASKED = REQUEST_MAPPING_QQ_PREFIX + REQUEST_MAPPING_ENGLISH + REQUEST_MAPPING_QQ_TAKAFUL_PREFIX + REQUEST_MAPPING_QQ_FOUR_FAILED_PARAM;

	
	public static final String REJECT_CODE_BMI = "BMI";
	public static final String REJECT_CODE_OCCUPATION = "Occupation";
	public static final String REJECT_CODE_LIAM = "LIAM List";
	public static final String REJECT_CODE_TERRORIST = "Terrorist List";
	public static final String REJECT_CODE_REJECTED_BEFORE = "Rejected Before";
	public static final String REJECT_CODE_PURCHASE_BEFORE = "Purchased Before";
	public static final String REJECT_CODE_HEALTH_QUESTION_1 = "HQ1";
	public static final String REJECT_CODE_HEALTH_QUESTION_2 = "HQ2";
	public static final String REJECT_CODE_HEALTH_QUESTION_3 = "HQ3";
	public static final String REJECT_CODE_CLAIM_HISTORY = "Claim History";
	public static final String REJECT_CODE_NATIONALITY = "Nationality";

	public static final String PAGE_NUMBER_1 = "1";
	public static final String PAGE_NUMBER_2 = "2";
	public static final String PAGE_NUMBER_3 = "3";
	public static final String PAGE_NUMBER_4 = "4";

	public static final String QUOTATION_STATUS_OPEN = "O";
	public static final String QUOTATION_STATUS_COMPLETE = "C";
	public static final String QUOTATION_STATUS_REJECTED = "R";

	public static final String UNDERWRITING_ANSWER_YES = "y";
	public static final String UNDERWRITING_ANSWER_NO = "n";

	public static final String MODAL_KEY_IS_REJECTED = "isRejected";
	public static final String MODAL_KEY_PREMIUM_FREQUENCY_TYPE = "premiumPaymentFrequencyType";
	public static final String MODAL_KEY_PREMIUM_FREQUENCY = "premiumPaymentFrequency";
	public static final String MODAL_KEY_PLAN_TYPE = "planType";
	public static final String MODAL_KEY_ANNUAL_LIMIT = "annualLimit";
	public static final String MODAL_KEY_DEDUCTIBLE = "deductible";
	public static final String MODAL_KEY_ROOM_RATE = "roomRate";

	public static final String FORM_NATIONALITY_MALAYSIAN = "mal";
	public static final String FORM_NATIONALITY_NON_MALAYSIAN = "nonmal";
	public static final String FORM_DEFAULT_NATIONALITY = "mal";
	public static final String FORM_DEFAULT_COUNTRY = "Malaysia";
	public static final String FORM_DEFAULT_COUNTRY_KEY = "mal";
	public static final String FORM_DEFAULT_IS_MAIL_ON_YES = "yes";
	public static final String FORM_DEFAULT_IS_MAIL_ON_NO = "no";
	public static final String ERROR_MESSAGE_UPDATE_FAILED = "UPDATE TO DATABASE FAILED";
	public static final String ERROR_MESSAGE_SESSION_EXPIRED = "SESSION EXPIRED";
	public static final String ERROR_MESSAGE_VPMS_RESPONSE_NULL = "VPMS RESPONSE NULL";
	public static final String ERROR_MESSAGE_COMPANY_NAME_NULL = "COMPANY NAME NULL";

	public static final String REQUEST_PAYMENT_INSERT_QUERY = "{CALL DSP_SP_PAYMENT_REQUEST_INSERT(?,?,?,?,?)}";
	public static final String RESPONSE_PAYMENT_INSERT_QUERY = "{CALL DSP_SP_PAYMENT_RESPONSE_INSERT(?,?,?,?,?)}";

	public static final String TRANSACTION_TYPE_REQUEST = "REQ";
	public static final String TRANSACTION_TYPE_RESPSONSE = "RESP";

	public static final String REQUEST_PAYMENT_KEY_SECURE_HASH = "SECUREHASH";
	public static final String REQUEST_PAYMENT_KEY_MID = "MID";
	public static final String REQUEST_PAYMENT_KEY_INVOICE_NUMBER = "INVNO";
	public static final String REQUEST_PAYMENT_KEY_AMOUNT = "AMT";
	public static final String REQUEST_PAYMENT_KEY_RECFLAG = "RECFLAG";
	public static final String REQUEST_PAYMENT_KEY_CUSTOMER_NAME = "CUSTNAME";
	public static final String REQUEST_PAYMENT_KEY_CUSTOMER_EMAIL = "CUSTEMAIL";
	public static final String REQUEST_PAYMENT_KEY_POST_URL = "POSTURL";
	public static final String REQUEST_PAYMENT_KEY_CUSTOMER_PHONE_NUMBER = "CUSTPHONE";
	public static final String REQUEST_PAYMENT_KEY_CUSTOMER_ADDRESS_1 = "CUSTADDRESS1";
	public static final String REQUEST_PAYMENT_KEY_CUSTOMER_ADDRESS_2 = "CUSTADDRESS2";
	public static final String REQUEST_PAYMENT_KEY_CUSTOMER_POSTAL = "CUSTPOSTAL";
	public static final String REQUEST_PAYMENT_KEY_CUSTOMER_CITY = "CUSTCITY";
	public static final String REQUEST_PAYMENT_KEY_CUSTOMER_PROVINCE = "CUSTPROVINCE";
	public static final String REQUEST_PAYMENT_KEY_CUSTOMER_STATE = "CUSTSTATE";
	public static final String REQUEST_PAYMENT_KEY_CUSTOMER_COUNTRY = "CUSTCOUNTRY";
	public static final String REQUEST_PAYMENT_KEY_DESCRIPTION = "DESC";

	public static final String RESPONSE_PAYMENT_KEY_RESPONSE_DATA = "RESPONSE";
	public static final String RESPONSE_PAYMENT_KEY_AUTH_CODE = "AUTHCODE";
	public static final String RESPONSE_PAYMENT_KEY_INVOICE_NO = "INVNO";
	public static final String RESPONSE_PAYMENT_KEY_CARD_4_DIGIT = "CARD4DIGIT";
	public static final String RESPONSE_PAYMENT_KEY_AMOUNT = "AMT";
	public static final String RESPONSE_PAYMENT_KEY_CARD_TYPE = "CARDTYPE";
	public static final String RESPONSE_PAYMENT_KEY_TRANSACTION_ID = "TXNID";
	
	public static final String PAYMENT_MODAL_KEY_AMOUNT = "amount";
	public static final String PAYMENT_MODAL_KEY_TRANSACTION_STATUS = "status";
	public static final String PAYMENT_MODAL_KEY_TRANSACTION_NUMBER = "transactionNumber";
	public static final String PAYMENT_MODAL_KEY_TRANSACTION_DATE_TIME = "datetime";
	public static final String PAYMENT_MODAL_KEY_RECEIPT_NUMBER = "receiptNumber";

	public static final String REQUEST_PAYMENT_VALUE_RECFLAG = "1";
	public static final String AMOUNT_LEFT_PADDING_CHARACTER = "0";
	
	public static final String RESTFUL_STATUS_ISINSERTDSPCOMMON_KEY = "isInsertDSPCommon";
	public static final String RESTFUL_STATUS_ISINSERTREJECTION_KEY = "isInsertRejection";
	public static final String RESTFUL_STATUS_ISINSERTMPQQ_KEY = "isInsertMpQq";
	public static final String RESTFUL_STATUS_ISINSERTCUSTCOMMON_KEY = "isInsertCustCommon";
	
	public static final String RESTFUL_STATUS_ISUPDATEMPQQ_KEY = "isUpdateMpQq";
	public static final String RESTFUL_STATUS_ISUPDATEREJECTION_KEY = "isUpdateRejection";
	
	public static final String RESTFUL_STATUS_CODE_KEY = "code";
	public static final String RESTFUL_STATUS_CODE_SUCCESS = "1";
	public static final String RESTFUL_STATUS_CODE_FAILED = "0";
	
	public static final String RESTFUL_STATUS_MESSAGE_KEY = "message";
	public static final String RESTFUL_STATUS_MESSAGE_SUCCESS = "Success";
	public static final String RESTFUL_STATUS_MESSAGE_SUCCESS_SAVED = "Data successfully saved";
	public static final String RESTFUL_STATUS_MESSAGE_SESSION_NULL = "Session not exist";
	public static final String RESTFUL_STATUS_MESSAGE_INCORRECT_ACCOUNT_NUMBER_LENGTH = "Length of account number is not correct";
	public static final String RESTFUL_STATUS_MESSAGE_BANK_DETAILS_NULL = "Bank details not exist";

	public static final String MAP_KEY_IS_PASSED = "isPassed";


}
