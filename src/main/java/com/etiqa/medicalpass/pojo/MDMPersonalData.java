package com.etiqa.medicalpass.pojo;

import java.io.Serializable;

public class MDMPersonalData implements Serializable {

	private static final long serialVersionUID = 1L;

	private String country;

	private String gender;

	private String city;

	private String mobileNumber;

	private String identificationType;

	private String idNumber;

	private String trxid;

	private String educationLvl;

	private String noofChildrens;

	private String insuredsinedt;

	private String prefLang;

	private String address5;

	private String address4;

	private String state;

	private String email;

	private String monthlyIncome;

	private String housePhno;

	private String staffFlag;

	private String address3;

	private String race;

	private String address2;

	private String address1;

	private String postcode;

	private String rescode;

	private String religion;

	private String nationality;

	private String dob;

	private String name;

	private String maritalStatus;

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country == null ? null : country.trim();
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender == null ? null : gender.trim();
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city == null ? null : city.trim();
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber == null ? null : mobileNumber.trim();
	}

	public String getIdentificationType() {
		return identificationType;
	}

	public void setIdentificationType(String identificationType) {
		this.identificationType = identificationType == null ? null : identificationType.trim();
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber == null ? null : idNumber.trim();
	}

	public String getTrxid() {
		return trxid;
	}

	public void setTrxid(String trxid) {
		this.trxid = trxid == null ? null : trxid.trim();
	}

	public String getEducationLvl() {
		return educationLvl;
	}

	public void setEducationLvl(String educationLvl) {
		this.educationLvl = educationLvl == null ? null : educationLvl.trim();
	}

	public String getNoofChildrens() {
		return noofChildrens;
	}

	public void setNoofChildrens(String noofChildrens) {
		this.noofChildrens = noofChildrens == null ? null : noofChildrens.trim();
	}

	public String getInsuredsinedt() {
		return insuredsinedt;
	}

	public void setInsuredsinedt(String insuredsinedt) {
		this.insuredsinedt = insuredsinedt == null ? null : insuredsinedt.trim();
	}

	public String getPrefLang() {
		return prefLang;
	}

	public void setPrefLang(String prefLang) {
		this.prefLang = prefLang == null ? null : prefLang.trim();
	}

	public String getAddress5() {
		return address5;
	}

	public void setAddress5(String address5) {
		this.address5 = address5 == null ? null : address5.trim();
	}

	public String getAddress4() {
		return address4;
	}

	public void setAddress4(String address4) {
		this.address4 = address4 == null ? null : address4.trim();
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state == null ? null : state.trim();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email == null ? null : email.trim();
	}

	public String getMonthlyIncome() {
		return monthlyIncome;
	}

	public void setMonthlyIncome(String monthlyIncome) {
		this.monthlyIncome = monthlyIncome == null ? null : monthlyIncome.trim();
	}

	public String getHousePhno() {
		return housePhno;
	}

	public void setHousePhno(String housePhno) {
		this.housePhno = housePhno == null ? null : housePhno.trim();
	}

	public String getStaffFlag() {
		return staffFlag;
	}

	public void setStaffFlag(String staffFlag) {
		this.staffFlag = staffFlag == null ? null : staffFlag.trim();
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3 == null ? null : address3.trim();
	}

	public String getRace() {
		return race;
	}

	public void setRace(String race) {
		this.race = race == null ? null : race.trim();
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2 == null ? null : address2.trim();
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1 == null ? null : address1.trim();
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode == null ? null : postcode.trim();
	}

	public String getRescode() {
		return rescode;
	}

	public void setRescode(String rescode) {
		this.rescode = rescode == null ? null : rescode.trim();
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion == null ? null : religion.trim();
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality == null ? null : nationality.trim();
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob == null ? null : dob.trim();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name == null ? null : name.trim();
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus == null ? null : maritalStatus.trim();
	}
	
	

}
