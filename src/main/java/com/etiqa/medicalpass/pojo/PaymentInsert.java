package com.etiqa.medicalpass.pojo;

import java.io.Serializable;
import java.math.BigDecimal;

public class PaymentInsert implements Serializable {

	private static final long serialVersionUID = 1L;
	
    private BigDecimal dspQqId;
    
    private String paymentGatewayCode;

    private String amount;

    private String paymentStatus;

    private String productCode;

    private String productPlanCode;

    private String transactionId;

    private String policyNumber;

    private String receiptNumber;

	public BigDecimal getDspQqId() {
		return dspQqId;
	}

	public void setDspQqId(BigDecimal dspQqId) {
		this.dspQqId = dspQqId;
	}

	public String getPaymentGatewayCode() {
		return paymentGatewayCode;
	}

	public void setPaymentGatewayCode(String paymentGatewayCode) {
		this.paymentGatewayCode = paymentGatewayCode == null ? null : paymentGatewayCode.trim();
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount == null ? null : amount.trim();
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus == null ? null : paymentStatus.trim();
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode == null ? null : productCode.trim();
	}

	public String getProductPlanCode() {
		return productPlanCode;
	}

	public void setProductPlanCode(String productPlanCode) {
		this.productPlanCode = productPlanCode == null ? null : productPlanCode.trim();
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId == null ? null : transactionId.trim();
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber == null ? null : policyNumber.trim();
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber == null ? null : receiptNumber.trim();
	}


}
