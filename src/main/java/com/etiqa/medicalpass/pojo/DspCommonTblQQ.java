package com.etiqa.medicalpass.pojo;

import java.math.BigDecimal;
import java.util.Date;

public class DspCommonTblQQ {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column DSP_COMMON_TBL_QQ.DSP_QQ_ID
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    private BigDecimal dspQqId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column DSP_COMMON_TBL_QQ.PRODUCT_CODE
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    private String productCode;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column DSP_COMMON_TBL_QQ.CUSTOMER_ID
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    private BigDecimal customerId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column DSP_COMMON_TBL_QQ.CREATED_DATE
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    private Date createdDate;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column DSP_COMMON_TBL_QQ.DSP_QQ_ID
     *
     * @return the value of DSP_COMMON_TBL_QQ.DSP_QQ_ID
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    public BigDecimal getDspQqId() {
        return dspQqId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column DSP_COMMON_TBL_QQ.DSP_QQ_ID
     *
     * @param dspQqId the value for DSP_COMMON_TBL_QQ.DSP_QQ_ID
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    public void setDspQqId(BigDecimal dspQqId) {
        this.dspQqId = dspQqId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column DSP_COMMON_TBL_QQ.PRODUCT_CODE
     *
     * @return the value of DSP_COMMON_TBL_QQ.PRODUCT_CODE
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column DSP_COMMON_TBL_QQ.PRODUCT_CODE
     *
     * @param productCode the value for DSP_COMMON_TBL_QQ.PRODUCT_CODE
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode == null ? null : productCode.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column DSP_COMMON_TBL_QQ.CUSTOMER_ID
     *
     * @return the value of DSP_COMMON_TBL_QQ.CUSTOMER_ID
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    public BigDecimal getCustomerId() {
        return customerId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column DSP_COMMON_TBL_QQ.CUSTOMER_ID
     *
     * @param customerId the value for DSP_COMMON_TBL_QQ.CUSTOMER_ID
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    public void setCustomerId(BigDecimal customerId) {
        this.customerId = customerId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column DSP_COMMON_TBL_QQ.CREATED_DATE
     *
     * @return the value of DSP_COMMON_TBL_QQ.CREATED_DATE
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    public Date getCreatedDate() {
        return createdDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column DSP_COMMON_TBL_QQ.CREATED_DATE
     *
     * @param createdDate the value for DSP_COMMON_TBL_QQ.CREATED_DATE
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}