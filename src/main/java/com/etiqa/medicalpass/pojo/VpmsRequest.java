package com.etiqa.medicalpass.pojo;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class VpmsRequest implements Serializable {

	private static final long serialVersionUID = -6116990477593039561L;

	@SerializedName("A_Language")
	private String lang;

	@SerializedName("A_PlanType")
	private String planType;

	@SerializedName("A_PaymentMode")
	private String paymentMode;

	@SerializedName("A_Occ")
	private String occupation;

	@SerializedName("A_Gender")
	private String gender;

	@SerializedName("A_DOB")
	private String dateOfBirth;

	@SerializedName("A_Quotation_Date")
	private String quotationDate;

	@SerializedName("A_Smoker")
	private String isSmoker;

	@SerializedName("A_Name")
	private String name;

	@SerializedName("A_IC")
	private String nric;

	@SerializedName("A_Deductible")
	private String deductible;

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang == null ? "" : lang.trim();
	}

	public String getPlanType() {
		return planType;
	}

	public void setPlanType(String planType) {
		this.planType = planType == null ? "" : planType.trim();
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode == null ? "" : paymentMode.trim();
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation == null ? "" : occupation.trim();
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender == null ? "" : gender.trim();
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth == null ? "" : dateOfBirth.trim();
	}

	public String getQuotationDate() {
		return quotationDate;
	}

	public void setQuotationDate(String quotationDate) {
		this.quotationDate = quotationDate == null ? "" : quotationDate.trim();
	}

	public String getIsSmoker() {
		return isSmoker;
	}

	public void setIsSmoker(String isSmoker) {
		this.isSmoker = isSmoker == null ? "" : isSmoker.trim();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name == null ? "" : name.trim();
	}

	public String getNric() {
		return nric;
	}

	public void setNric(String nric) {
		this.nric = nric == null ? "" : nric.trim();
	}

	public String getDeductible() {
		return deductible;
	}

	public void setDeductible(String deductible) {
		this.deductible = deductible == null ? "" : deductible.trim();
	}
}
