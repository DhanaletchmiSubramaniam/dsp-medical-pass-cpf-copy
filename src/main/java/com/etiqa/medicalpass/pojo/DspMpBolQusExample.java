package com.etiqa.medicalpass.pojo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class DspMpBolQusExample {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table DSP_MP_BOL_QUS
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    protected String orderByClause;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table DSP_MP_BOL_QUS
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    protected boolean distinct;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table DSP_MP_BOL_QUS
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    protected List<Criteria> oredCriteria;

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_MP_BOL_QUS
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    public DspMpBolQusExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_MP_BOL_QUS
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_MP_BOL_QUS
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    public String getOrderByClause() {
        return orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_MP_BOL_QUS
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_MP_BOL_QUS
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    public boolean isDistinct() {
        return distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_MP_BOL_QUS
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_MP_BOL_QUS
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_MP_BOL_QUS
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_MP_BOL_QUS
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_MP_BOL_QUS
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_MP_BOL_QUS
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table DSP_MP_BOL_QUS
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(BigDecimal value) {
            addCriterion("ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(BigDecimal value) {
            addCriterion("ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(BigDecimal value) {
            addCriterion("ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(BigDecimal value) {
            addCriterion("ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(BigDecimal value) {
            addCriterion("ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<BigDecimal> values) {
            addCriterion("ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<BigDecimal> values) {
            addCriterion("ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andQusCodeIsNull() {
            addCriterion("QUS_CODE is null");
            return (Criteria) this;
        }

        public Criteria andQusCodeIsNotNull() {
            addCriterion("QUS_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andQusCodeEqualTo(String value) {
            addCriterion("QUS_CODE =", value, "qusCode");
            return (Criteria) this;
        }

        public Criteria andQusCodeNotEqualTo(String value) {
            addCriterion("QUS_CODE <>", value, "qusCode");
            return (Criteria) this;
        }

        public Criteria andQusCodeGreaterThan(String value) {
            addCriterion("QUS_CODE >", value, "qusCode");
            return (Criteria) this;
        }

        public Criteria andQusCodeGreaterThanOrEqualTo(String value) {
            addCriterion("QUS_CODE >=", value, "qusCode");
            return (Criteria) this;
        }

        public Criteria andQusCodeLessThan(String value) {
            addCriterion("QUS_CODE <", value, "qusCode");
            return (Criteria) this;
        }

        public Criteria andQusCodeLessThanOrEqualTo(String value) {
            addCriterion("QUS_CODE <=", value, "qusCode");
            return (Criteria) this;
        }

        public Criteria andQusCodeLike(String value) {
            addCriterion("QUS_CODE like", value, "qusCode");
            return (Criteria) this;
        }

        public Criteria andQusCodeNotLike(String value) {
            addCriterion("QUS_CODE not like", value, "qusCode");
            return (Criteria) this;
        }

        public Criteria andQusCodeIn(List<String> values) {
            addCriterion("QUS_CODE in", values, "qusCode");
            return (Criteria) this;
        }

        public Criteria andQusCodeNotIn(List<String> values) {
            addCriterion("QUS_CODE not in", values, "qusCode");
            return (Criteria) this;
        }

        public Criteria andQusCodeBetween(String value1, String value2) {
            addCriterion("QUS_CODE between", value1, value2, "qusCode");
            return (Criteria) this;
        }

        public Criteria andQusCodeNotBetween(String value1, String value2) {
            addCriterion("QUS_CODE not between", value1, value2, "qusCode");
            return (Criteria) this;
        }

        public Criteria andQusDescIsNull() {
            addCriterion("QUS_DESC is null");
            return (Criteria) this;
        }

        public Criteria andQusDescIsNotNull() {
            addCriterion("QUS_DESC is not null");
            return (Criteria) this;
        }

        public Criteria andQusDescEqualTo(String value) {
            addCriterion("QUS_DESC =", value, "qusDesc");
            return (Criteria) this;
        }

        public Criteria andQusDescNotEqualTo(String value) {
            addCriterion("QUS_DESC <>", value, "qusDesc");
            return (Criteria) this;
        }

        public Criteria andQusDescGreaterThan(String value) {
            addCriterion("QUS_DESC >", value, "qusDesc");
            return (Criteria) this;
        }

        public Criteria andQusDescGreaterThanOrEqualTo(String value) {
            addCriterion("QUS_DESC >=", value, "qusDesc");
            return (Criteria) this;
        }

        public Criteria andQusDescLessThan(String value) {
            addCriterion("QUS_DESC <", value, "qusDesc");
            return (Criteria) this;
        }

        public Criteria andQusDescLessThanOrEqualTo(String value) {
            addCriterion("QUS_DESC <=", value, "qusDesc");
            return (Criteria) this;
        }

        public Criteria andQusDescLike(String value) {
            addCriterion("QUS_DESC like", value, "qusDesc");
            return (Criteria) this;
        }

        public Criteria andQusDescNotLike(String value) {
            addCriterion("QUS_DESC not like", value, "qusDesc");
            return (Criteria) this;
        }

        public Criteria andQusDescIn(List<String> values) {
            addCriterion("QUS_DESC in", values, "qusDesc");
            return (Criteria) this;
        }

        public Criteria andQusDescNotIn(List<String> values) {
            addCriterion("QUS_DESC not in", values, "qusDesc");
            return (Criteria) this;
        }

        public Criteria andQusDescBetween(String value1, String value2) {
            addCriterion("QUS_DESC between", value1, value2, "qusDesc");
            return (Criteria) this;
        }

        public Criteria andQusDescNotBetween(String value1, String value2) {
            addCriterion("QUS_DESC not between", value1, value2, "qusDesc");
            return (Criteria) this;
        }

        public Criteria andCorrectAnsCodeIsNull() {
            addCriterion("CORRECT_ANS_CODE is null");
            return (Criteria) this;
        }

        public Criteria andCorrectAnsCodeIsNotNull() {
            addCriterion("CORRECT_ANS_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andCorrectAnsCodeEqualTo(String value) {
            addCriterion("CORRECT_ANS_CODE =", value, "correctAnsCode");
            return (Criteria) this;
        }

        public Criteria andCorrectAnsCodeNotEqualTo(String value) {
            addCriterion("CORRECT_ANS_CODE <>", value, "correctAnsCode");
            return (Criteria) this;
        }

        public Criteria andCorrectAnsCodeGreaterThan(String value) {
            addCriterion("CORRECT_ANS_CODE >", value, "correctAnsCode");
            return (Criteria) this;
        }

        public Criteria andCorrectAnsCodeGreaterThanOrEqualTo(String value) {
            addCriterion("CORRECT_ANS_CODE >=", value, "correctAnsCode");
            return (Criteria) this;
        }

        public Criteria andCorrectAnsCodeLessThan(String value) {
            addCriterion("CORRECT_ANS_CODE <", value, "correctAnsCode");
            return (Criteria) this;
        }

        public Criteria andCorrectAnsCodeLessThanOrEqualTo(String value) {
            addCriterion("CORRECT_ANS_CODE <=", value, "correctAnsCode");
            return (Criteria) this;
        }

        public Criteria andCorrectAnsCodeLike(String value) {
            addCriterion("CORRECT_ANS_CODE like", value, "correctAnsCode");
            return (Criteria) this;
        }

        public Criteria andCorrectAnsCodeNotLike(String value) {
            addCriterion("CORRECT_ANS_CODE not like", value, "correctAnsCode");
            return (Criteria) this;
        }

        public Criteria andCorrectAnsCodeIn(List<String> values) {
            addCriterion("CORRECT_ANS_CODE in", values, "correctAnsCode");
            return (Criteria) this;
        }

        public Criteria andCorrectAnsCodeNotIn(List<String> values) {
            addCriterion("CORRECT_ANS_CODE not in", values, "correctAnsCode");
            return (Criteria) this;
        }

        public Criteria andCorrectAnsCodeBetween(String value1, String value2) {
            addCriterion("CORRECT_ANS_CODE between", value1, value2, "correctAnsCode");
            return (Criteria) this;
        }

        public Criteria andCorrectAnsCodeNotBetween(String value1, String value2) {
            addCriterion("CORRECT_ANS_CODE not between", value1, value2, "correctAnsCode");
            return (Criteria) this;
        }

        public Criteria andEnforceFlagIsNull() {
            addCriterion("ENFORCE_FLAG is null");
            return (Criteria) this;
        }

        public Criteria andEnforceFlagIsNotNull() {
            addCriterion("ENFORCE_FLAG is not null");
            return (Criteria) this;
        }

        public Criteria andEnforceFlagEqualTo(String value) {
            addCriterion("ENFORCE_FLAG =", value, "enforceFlag");
            return (Criteria) this;
        }

        public Criteria andEnforceFlagNotEqualTo(String value) {
            addCriterion("ENFORCE_FLAG <>", value, "enforceFlag");
            return (Criteria) this;
        }

        public Criteria andEnforceFlagGreaterThan(String value) {
            addCriterion("ENFORCE_FLAG >", value, "enforceFlag");
            return (Criteria) this;
        }

        public Criteria andEnforceFlagGreaterThanOrEqualTo(String value) {
            addCriterion("ENFORCE_FLAG >=", value, "enforceFlag");
            return (Criteria) this;
        }

        public Criteria andEnforceFlagLessThan(String value) {
            addCriterion("ENFORCE_FLAG <", value, "enforceFlag");
            return (Criteria) this;
        }

        public Criteria andEnforceFlagLessThanOrEqualTo(String value) {
            addCriterion("ENFORCE_FLAG <=", value, "enforceFlag");
            return (Criteria) this;
        }

        public Criteria andEnforceFlagLike(String value) {
            addCriterion("ENFORCE_FLAG like", value, "enforceFlag");
            return (Criteria) this;
        }

        public Criteria andEnforceFlagNotLike(String value) {
            addCriterion("ENFORCE_FLAG not like", value, "enforceFlag");
            return (Criteria) this;
        }

        public Criteria andEnforceFlagIn(List<String> values) {
            addCriterion("ENFORCE_FLAG in", values, "enforceFlag");
            return (Criteria) this;
        }

        public Criteria andEnforceFlagNotIn(List<String> values) {
            addCriterion("ENFORCE_FLAG not in", values, "enforceFlag");
            return (Criteria) this;
        }

        public Criteria andEnforceFlagBetween(String value1, String value2) {
            addCriterion("ENFORCE_FLAG between", value1, value2, "enforceFlag");
            return (Criteria) this;
        }

        public Criteria andEnforceFlagNotBetween(String value1, String value2) {
            addCriterion("ENFORCE_FLAG not between", value1, value2, "enforceFlag");
            return (Criteria) this;
        }

        public Criteria andAns1CodeIsNull() {
            addCriterion("ANS_1_CODE is null");
            return (Criteria) this;
        }

        public Criteria andAns1CodeIsNotNull() {
            addCriterion("ANS_1_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andAns1CodeEqualTo(String value) {
            addCriterion("ANS_1_CODE =", value, "ans1Code");
            return (Criteria) this;
        }

        public Criteria andAns1CodeNotEqualTo(String value) {
            addCriterion("ANS_1_CODE <>", value, "ans1Code");
            return (Criteria) this;
        }

        public Criteria andAns1CodeGreaterThan(String value) {
            addCriterion("ANS_1_CODE >", value, "ans1Code");
            return (Criteria) this;
        }

        public Criteria andAns1CodeGreaterThanOrEqualTo(String value) {
            addCriterion("ANS_1_CODE >=", value, "ans1Code");
            return (Criteria) this;
        }

        public Criteria andAns1CodeLessThan(String value) {
            addCriterion("ANS_1_CODE <", value, "ans1Code");
            return (Criteria) this;
        }

        public Criteria andAns1CodeLessThanOrEqualTo(String value) {
            addCriterion("ANS_1_CODE <=", value, "ans1Code");
            return (Criteria) this;
        }

        public Criteria andAns1CodeLike(String value) {
            addCriterion("ANS_1_CODE like", value, "ans1Code");
            return (Criteria) this;
        }

        public Criteria andAns1CodeNotLike(String value) {
            addCriterion("ANS_1_CODE not like", value, "ans1Code");
            return (Criteria) this;
        }

        public Criteria andAns1CodeIn(List<String> values) {
            addCriterion("ANS_1_CODE in", values, "ans1Code");
            return (Criteria) this;
        }

        public Criteria andAns1CodeNotIn(List<String> values) {
            addCriterion("ANS_1_CODE not in", values, "ans1Code");
            return (Criteria) this;
        }

        public Criteria andAns1CodeBetween(String value1, String value2) {
            addCriterion("ANS_1_CODE between", value1, value2, "ans1Code");
            return (Criteria) this;
        }

        public Criteria andAns1CodeNotBetween(String value1, String value2) {
            addCriterion("ANS_1_CODE not between", value1, value2, "ans1Code");
            return (Criteria) this;
        }

        public Criteria andAns2CodeIsNull() {
            addCriterion("ANS_2_CODE is null");
            return (Criteria) this;
        }

        public Criteria andAns2CodeIsNotNull() {
            addCriterion("ANS_2_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andAns2CodeEqualTo(String value) {
            addCriterion("ANS_2_CODE =", value, "ans2Code");
            return (Criteria) this;
        }

        public Criteria andAns2CodeNotEqualTo(String value) {
            addCriterion("ANS_2_CODE <>", value, "ans2Code");
            return (Criteria) this;
        }

        public Criteria andAns2CodeGreaterThan(String value) {
            addCriterion("ANS_2_CODE >", value, "ans2Code");
            return (Criteria) this;
        }

        public Criteria andAns2CodeGreaterThanOrEqualTo(String value) {
            addCriterion("ANS_2_CODE >=", value, "ans2Code");
            return (Criteria) this;
        }

        public Criteria andAns2CodeLessThan(String value) {
            addCriterion("ANS_2_CODE <", value, "ans2Code");
            return (Criteria) this;
        }

        public Criteria andAns2CodeLessThanOrEqualTo(String value) {
            addCriterion("ANS_2_CODE <=", value, "ans2Code");
            return (Criteria) this;
        }

        public Criteria andAns2CodeLike(String value) {
            addCriterion("ANS_2_CODE like", value, "ans2Code");
            return (Criteria) this;
        }

        public Criteria andAns2CodeNotLike(String value) {
            addCriterion("ANS_2_CODE not like", value, "ans2Code");
            return (Criteria) this;
        }

        public Criteria andAns2CodeIn(List<String> values) {
            addCriterion("ANS_2_CODE in", values, "ans2Code");
            return (Criteria) this;
        }

        public Criteria andAns2CodeNotIn(List<String> values) {
            addCriterion("ANS_2_CODE not in", values, "ans2Code");
            return (Criteria) this;
        }

        public Criteria andAns2CodeBetween(String value1, String value2) {
            addCriterion("ANS_2_CODE between", value1, value2, "ans2Code");
            return (Criteria) this;
        }

        public Criteria andAns2CodeNotBetween(String value1, String value2) {
            addCriterion("ANS_2_CODE not between", value1, value2, "ans2Code");
            return (Criteria) this;
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table DSP_MP_BOL_QUS
     *
     * @mbg.generated do_not_delete_during_merge Wed Jan 30 15:41:57 SGT 2019
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table DSP_MP_BOL_QUS
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}