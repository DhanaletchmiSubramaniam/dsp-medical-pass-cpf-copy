package com.etiqa.medicalpass.pojo;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class VpmsResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private String message;

	private String status;

	@SerializedName("data")
	private VpmsResponseData vpmsResponseData;

	private String code;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public VpmsResponseData getData() {
		return vpmsResponseData;
	}

	public void setData(VpmsResponseData vpmsResponseData) {
		this.vpmsResponseData = vpmsResponseData;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "VpmsResponse [message = " + message + ", status = " + status + ", data = " + vpmsResponseData.toString()
				+ ", code = " + code + "]";
	}

}
