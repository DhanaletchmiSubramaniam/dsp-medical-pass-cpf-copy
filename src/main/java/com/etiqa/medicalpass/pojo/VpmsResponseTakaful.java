package com.etiqa.medicalpass.pojo;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class VpmsResponseTakaful implements Serializable {

	private static final long serialVersionUID = 1L;

	private String message;

	private String status;

	@SerializedName("data")
	private VpmsResponseTakafulData vpmsResponseTakafulData;

	private String code;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public VpmsResponseTakafulData getData() {
		return vpmsResponseTakafulData;
	}

	public void setData(VpmsResponseTakafulData vpmsResponseTakafulData) {
		this.vpmsResponseTakafulData = vpmsResponseTakafulData;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "VpmsResponse [message = " + message + ", status = " + status + ", data = " + vpmsResponseTakafulData.toString()
				+ ", code = " + code + "]";
	}

}
