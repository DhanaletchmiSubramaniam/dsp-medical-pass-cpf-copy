package com.etiqa.medicalpass.pojo;

public class DocCustomPojo {

	private String language;

	private String sumCovered;

	private String paymentFrequency;

	private String policyNo;

	private String name;

	private String quotationDate;

	private String isSmoker;

	private String gender;

	private String occupationClass;

	private String dob;

	private String ageOfNextBday;

	private String dspQQId;

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getSumCovered() {
		return sumCovered;
	}

	public void setSumCovered(String sumCovered) {
		this.sumCovered = sumCovered;
	}

	public String getPaymentFrequency() {
		return paymentFrequency;
	}

	public void setPaymentFrequency(String paymentFrequency) {
		this.paymentFrequency = paymentFrequency;
	}

	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public String getPayableAmt() {
		return payableAmt;
	}

	public void setPayableAmt(String payableAmt) {
		this.payableAmt = payableAmt;
	}

	private String payableAmt;

	public String getDurationOfBenefit() {
		return durationOfBenefit;
	}

	public void setDurationOfBenefit(String durationOfBenefit) {
		this.durationOfBenefit = durationOfBenefit;
	}

	private String durationOfBenefit;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getQuotationDate() {
		return quotationDate;
	}

	public void setQuotationDate(String quotationDate) {
		this.quotationDate = quotationDate;
	}

	public String getIsSmoker() {
		return isSmoker;
	}

	public void setIsSmoker(String isSmoker) {
		this.isSmoker = isSmoker;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getOccupationClass() {
		return occupationClass;
	}

	public void setOccupationClass(String occupationClass) {
		this.occupationClass = occupationClass;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getAgeOfNextBday() {
		return ageOfNextBday;
	}

	public void setAgeOfNextBday(String ageOfNextBday) {
		this.ageOfNextBday = ageOfNextBday;
	}

	public String getDspQQId() {
		return dspQQId;
	}

	public void setDspQQId(String dspQQId) {
		this.dspQQId = dspQQId;
	}

	public String getIc() {
		return ic;
	}

	public void setIc(String ic) {
		this.ic = ic;
	}

	private String ic;

}
