package com.etiqa.medicalpass.pojo;

import java.io.Serializable;
import java.math.BigDecimal;

public class PaymentResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
    private BigDecimal dspQqId;
    
    private String paymentStatus;

    private String amount;

    private String transactionDateTime;

    private String policyNumber;

    private String receiptNumber;

	public BigDecimal getDspQqId() {
		return dspQqId;
	}

	public void setDspQqId(BigDecimal dspQqId) {
		this.dspQqId = dspQqId;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus == null ? null : paymentStatus.trim();
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount == null ? null : amount.trim();
	}

	public String getTransactionDateTime() {
		return transactionDateTime;
	}

	public void setTransactionDateTime(String transactionDateTime) {
		this.transactionDateTime = transactionDateTime == null ? null : transactionDateTime.trim();
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber == null ? null : policyNumber.trim();
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber == null ? null : receiptNumber.trim();
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	
}
