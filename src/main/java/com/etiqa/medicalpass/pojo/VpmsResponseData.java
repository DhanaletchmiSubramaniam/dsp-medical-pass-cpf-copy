package com.etiqa.medicalpass.pojo;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class VpmsResponseData implements Serializable {

	private static final long serialVersionUID = 1L;

	@SerializedName("P_Version")
	private String version;

	@SerializedName("P_SI_Quotation_Date")
	private String quotationDate;

	@SerializedName("P_Premium_Payment_Term")
	private String premiumPaymentTerm;

	@SerializedName("P_MonthlyPrem")
	private String monthlyPremium;

	@SerializedName("P_YearlyPrem")
	private String annualPremium;

	@SerializedName("P_SI_Name")
	private String name;

	@SerializedName("P_SI_Gender")
	private String gender;

	@SerializedName("P_SI_Smoker")
	private String smoker;

	@SerializedName("P_SI_Occ_Class")
	private String occupationClass;

	@SerializedName("P_SI_IC")
	private String nric;

	@SerializedName("P_SI_DOB")
	private String dob;

	@SerializedName("P_SI_ANB")
	private String ageNextBirthday;

	@SerializedName("P_SI_PlanName")
	private String planName;

	@SerializedName("P_SI_PolicyTerm")
	private String policyTerm;

	@SerializedName("P_SI_PremPaymentTerm")
	private String paymentTerm;

	@SerializedName("P_SI_PlanType")
	private String planType;

	@SerializedName("P_SI_Deductible")
	private String deductible;

	@SerializedName("P_SI_FirstYearPrem")
	private String firstYearPremium;

	@SerializedName("P_SI_PremiumMode")
	private String premiumMode;

	@SerializedName("P_O_SI_EOP_Yr")
	private String endCoveredAgeAndWakalah;

	@SerializedName("P_O_SI_Premium_Yearly")
	private String annualPremiumArray;

	@SerializedName("P_O_SI_Total_Prem_To_Date")
	private String totalPremiumToDate;

	@SerializedName("P_O_SI_End_Insured_Age")
	private String endCoveredAgeArray;

	@SerializedName("P_SI_Benefit")
	private String planBenefit;

	@SerializedName("P_SI_OverAnnLimit")
	private String annualLimit;

	@SerializedName("P_SI_OverLifeLimit")
	private String lifetimeLimit;

	@SerializedName("P_SI_HospRoomDay")
	private String roomRatePerDay;

	@SerializedName("P_SI_HospRoomAnnum")
	private String roomAnnualLimit;

	@SerializedName("P_SI_DeductibleOpt")
	private String deductibleOption;

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version == null ? "" : version.trim();
	}

	public String getQuotationDate() {
		return quotationDate;
	}

	public void setQuotationDate(String quotationDate) {
		this.quotationDate = quotationDate == null ? "" : quotationDate.trim();
	}

	public String getPremiumPaymentTerm() {
		return premiumPaymentTerm;
	}

	public void setPremiumPaymentTerm(String premiumPaymentTerm) {
		this.premiumPaymentTerm = premiumPaymentTerm == null ? "" : premiumPaymentTerm.trim();
	}

	public String getMonthlyPremium() {
		return monthlyPremium;
	}

	public void setMonthlyPremium(String monthlyPremium) {
		this.monthlyPremium = monthlyPremium == null ? "" : monthlyPremium.trim();
	}

	public String getAnnualPremium() {
		return annualPremium;
	}

	public void setAnnualPremium(String annualPremium) {
		this.annualPremium = annualPremium == null ? "" : annualPremium.trim();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name == null ? "" : name.trim();
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender == null ? "" : gender.trim();
	}

	public String getSmoker() {
		return smoker;
	}

	public void setSmoker(String smoker) {
		this.smoker = smoker == null ? "" : smoker.trim();
	}

	public String getOccupationClass() {
		return occupationClass;
	}

	public void setOccupationClass(String occupationClass) {
		this.occupationClass = occupationClass == null ? "" : occupationClass.trim();
	}

	public String getNric() {
		return nric;
	}

	public void setNric(String nric) {
		this.nric = nric == null ? "" : nric.trim();
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob == null ? "" : dob.trim();
	}

	public String getAgeNextBirthday() {
		return ageNextBirthday;
	}

	public void setAgeNextBirthday(String ageNextBirthday) {
		this.ageNextBirthday = ageNextBirthday == null ? "" : ageNextBirthday.trim();
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName == null ? "" : planName.trim();
	}

	public String getPolicyTerm() {
		return policyTerm;
	}

	public void setPolicyTerm(String policyTerm) {
		this.policyTerm = policyTerm == null ? "" : policyTerm.trim();
	}

	public String getPaymentTerm() {
		return paymentTerm;
	}

	public void setPaymentTerm(String paymentTerm) {
		this.paymentTerm = paymentTerm == null ? "" : paymentTerm.trim();
	}

	public String getPlanType() {
		return planType;
	}

	public void setPlanType(String planType) {
		this.planType = planType == null ? "" : planType.trim();
	}

	public String getDeductible() {
		return deductible;
	}

	public void setDeductible(String deductible) {
		this.deductible = deductible == null ? "" : deductible.trim();
	}

	public String getFirstYearPremium() {
		return firstYearPremium;
	}

	public void setFirstYearPremium(String firstYearPremium) {
		this.firstYearPremium = firstYearPremium == null ? "" : firstYearPremium.trim();
	}

	public String getPremiumMode() {
		return premiumMode;
	}

	public void setPremiumMode(String premiumMode) {
		this.premiumMode = premiumMode == null ? "" : premiumMode.trim();
	}

	public String[] getAnnualPremiumArray() {
		return annualPremiumArray.split("\\|", -1);
	}

	public String getAnnualPremiumString() {
		return annualPremiumArray;
	}

	public void setAnnualPremiumArray(String annualPremiumArray) {
		this.annualPremiumArray = annualPremiumArray == null ? "" : annualPremiumArray.trim();
	}

	public String[] getTotalPremiumToDate() {
		return totalPremiumToDate.split("\\|", -1);
	}

	public String getTotalPremiumToDateString() {
		return totalPremiumToDate;
	}

	public void setTotalPremiumToDate(String totalPremiumToDate) {
		this.totalPremiumToDate = totalPremiumToDate == null ? "" : totalPremiumToDate.trim();
	}

	public String[] getEndCoveredAgeArray() {
		return endCoveredAgeArray.split("\\|", -1);
	}

	public String getEndCoveredAgeArrayInString() {
		return endCoveredAgeArray;
	}

	public void setEndCoveredAgeArray(String endCoveredAgeArray) {
		this.endCoveredAgeArray = endCoveredAgeArray == null ? "" : endCoveredAgeArray.trim();
	}

	public String getPlanBenefit() {
		return planBenefit;
	}

	public void setPlanBenefit(String planBenefit) {
		this.planBenefit = planBenefit == null ? "" : planBenefit.trim();
	}

	public String getAnnualLimit() {
		return annualLimit;
	}

	public void setAnnualLimit(String annualLimit) {
		this.annualLimit = annualLimit == null ? "" : annualLimit.trim();
	}

	public String getLifetimeLimit() {
		return lifetimeLimit;
	}

	public void setLifetimeLimit(String lifetimeLimit) {
		this.lifetimeLimit = lifetimeLimit == null ? "" : lifetimeLimit.trim();
	}

	public String getRoomRatePerDay() {
		return roomRatePerDay;
	}

	public void setRoomRatePerDay(String roomRatePerDay) {
		this.roomRatePerDay = roomRatePerDay == null ? "" : roomRatePerDay.trim();
	}

	public String getRoomAnnualLimit() {
		return roomAnnualLimit;
	}

	public void setRoomAnnualLimit(String roomAnnualLimit) {
		this.roomAnnualLimit = roomAnnualLimit == null ? "" : roomAnnualLimit.trim();
	}

	public String getDeductibleOption() {
		return deductibleOption;
	}

	public void setDeductibleOption(String deductibleOption) {
		this.deductibleOption = deductibleOption == null ? "" : deductibleOption.trim();
	}

	public String getEndCoveredAgeAndWakalah() {
		return endCoveredAgeAndWakalah;
	}
	
	public String[] getEndCoveredAgeAndWakalahArray() {
		return endCoveredAgeAndWakalah.split("\\|", -1);
	}

	public void setEndCoveredAgeAndWakalah(String endCoveredAgeAndWakalah) {
		this.endCoveredAgeAndWakalah = endCoveredAgeAndWakalah == null ? null : endCoveredAgeAndWakalah.trim();
	}
}
