package com.etiqa.medicalpass.service;

import java.io.IOException;
import java.util.List;

import com.etiqa.medicalpass.pojo.DspCommonTblBankList;

public interface DspCommonTblBankListService {

	public List<DspCommonTblBankList> getBanks() throws IOException;

}
