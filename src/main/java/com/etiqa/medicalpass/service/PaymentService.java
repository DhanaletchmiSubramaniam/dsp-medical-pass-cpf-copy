package com.etiqa.medicalpass.service;

import java.io.IOException;

import com.etiqa.medicalpass.pojo.DspCommonTblPayment;
import com.etiqa.medicalpass.pojo.DspCommonTblPaymentExample;
import com.etiqa.medicalpass.pojo.PaymentInsert;

public interface PaymentService {

	public void insertPayment(PaymentInsert record) throws IOException;

	public Integer updatePayment(DspCommonTblPayment record, DspCommonTblPaymentExample example) throws IOException;

}
