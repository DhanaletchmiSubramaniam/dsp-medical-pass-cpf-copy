package com.etiqa.medicalpass.service;

import com.etiqa.medicalpass.email.DspEmailTemplateBean;
import com.etiqa.medicalpass.pojo.DspCommonTblCustomer;
import com.etiqa.medicalpass.pojo.DspMpTblQQ;
import com.etiqa.medicalpass.pojo.PaymentResponse;

public interface ProductMailTemplateService {

	public DspEmailTemplateBean loadMailTemplate(DspMpTblQQ mpQqData, PaymentResponse paymentResponse,
			DspCommonTblCustomer dspCommonTblCustomer, String company);

}
