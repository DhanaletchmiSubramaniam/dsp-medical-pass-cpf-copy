package com.etiqa.medicalpass.service;

import java.io.IOException;
import java.util.concurrent.Future;

public interface DspMpTblTerroristService {

	public Future<Long> getTerroristCount(String nric) throws IOException;

}
