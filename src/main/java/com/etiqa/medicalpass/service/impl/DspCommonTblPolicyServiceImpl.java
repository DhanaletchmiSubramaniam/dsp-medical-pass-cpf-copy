package com.etiqa.medicalpass.service.impl;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etiqa.medicalpass.mapper.DspCommonTblPolicyMapper;
import com.etiqa.medicalpass.pojo.DspCommonTblPolicy;
import com.etiqa.medicalpass.pojo.DspCommonTblPolicyExample;
import com.etiqa.medicalpass.service.DspCommonTblPolicyService;

@Service
public class DspCommonTblPolicyServiceImpl implements DspCommonTblPolicyService {

	private static final Logger logger = LoggerFactory.getLogger(DspCommonTblPolicyServiceImpl.class);

	@Autowired
	private DspCommonTblPolicyMapper mapper;

	@Override
	public List<DspCommonTblPolicy> getPolicyDetails(DspCommonTblPolicy record, DspCommonTblPolicyExample example)
			throws IOException {
		List<DspCommonTblPolicy> policies = null;
		try {
			policies = mapper.selectByExample(example);
			logger.info("Result: {}", policies.size());
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}

		return policies;
	}
}
