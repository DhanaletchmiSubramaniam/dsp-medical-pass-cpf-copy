package com.etiqa.medicalpass.service.impl;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etiqa.medicalpass.mapper.DspTlTblOccMapper;
import com.etiqa.medicalpass.pojo.DspTlTblOcc;
import com.etiqa.medicalpass.pojo.DspTlTblOccExample;
import com.etiqa.medicalpass.service.DspTlTblOccService;

@Service
public class DspTlTblOccServiceImpl implements DspTlTblOccService {

	private static final Logger logger = LoggerFactory.getLogger(DspTlTblOccServiceImpl.class);

	@Autowired
	private DspTlTblOccMapper mapper;

	@Override
	public List<DspTlTblOcc> getOccupation(HttpServletRequest request, String occupation) throws IOException {

		List<DspTlTblOcc> occupations = null;
		try {
			DspTlTblOccExample example = new DspTlTblOccExample();
			occupation = "%" + StringUtils.upperCase(occupation) + "%";
			example.createCriteria().andOccNameLike(occupation);
			occupations = mapper.selectByExample(example);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}

		return occupations;
	}

}
