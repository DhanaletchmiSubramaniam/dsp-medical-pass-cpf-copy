package com.etiqa.medicalpass.service.impl;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.etiqa.medicalpass.mapper.DspMpTblCreditCardOwnerMapper;
import com.etiqa.medicalpass.pojo.DspMpTblCreditCardOwner;
import com.etiqa.medicalpass.service.DspMpTblCreditCardOwnerService;

@Service
public class DspMpTblCreditCardOwnerServiceImpl implements DspMpTblCreditCardOwnerService {

	private static final Logger logger = LoggerFactory.getLogger(DspMpTblCreditCardOwnerServiceImpl.class);

	@Autowired
	private DspMpTblCreditCardOwnerMapper mapper;

	@Override
	@Async("threadPoolTaskExecutor")
	public void insertDspMpTblCreditCardOwnerVoidAsync(DspMpTblCreditCardOwner record) throws IOException {
		Integer count = 0;
		try {
			count = mapper.insertSelective(record);
			logger.info("Result: {}", count);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			Thread.currentThread().interrupt();
		}
	}

}
