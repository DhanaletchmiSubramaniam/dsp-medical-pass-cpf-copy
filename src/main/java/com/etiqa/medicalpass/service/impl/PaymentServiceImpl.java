package com.etiqa.medicalpass.service.impl;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etiqa.medicalpass.mapper.DspCommonTblPaymentMapper;
import com.etiqa.medicalpass.mapper.PaymentMapper;
import com.etiqa.medicalpass.pojo.DspCommonTblPayment;
import com.etiqa.medicalpass.pojo.DspCommonTblPaymentExample;
import com.etiqa.medicalpass.pojo.PaymentInsert;
import com.etiqa.medicalpass.service.PaymentService;

@Service
public class PaymentServiceImpl implements PaymentService {

	private static final Logger logger = LoggerFactory.getLogger(PaymentServiceImpl.class);

	@Autowired
	private PaymentMapper mapper;
	
	@Autowired
	private DspCommonTblPaymentMapper mybatisPaymentmapper;

	@Override
	public void insertPayment(PaymentInsert record) throws IOException {
		try {
			mapper.insertPayment(record);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
	}

	@Override
	public Integer updatePayment(DspCommonTblPayment record, DspCommonTblPaymentExample example) throws IOException {
		Integer update = 0;
		try {
			update = mybatisPaymentmapper.updateByExampleSelective(record, example);
			logger.error("update: {}", update);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return update;
	}
}
