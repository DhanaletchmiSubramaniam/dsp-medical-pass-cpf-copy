package com.etiqa.medicalpass.service.impl;

import java.io.IOException;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import com.etiqa.medicalpass.mapper.DspMpTblTerroristMapper;
import com.etiqa.medicalpass.pojo.DspMpTblTerroristExample;
import com.etiqa.medicalpass.service.DspMpTblTerroristService;

@Service
public class DspMpTblTerroristServiceImpl implements DspMpTblTerroristService {

	private static final Logger logger = LoggerFactory.getLogger(DspMpTblTerroristServiceImpl.class);

	@Autowired
	private DspMpTblTerroristMapper mapper;

	@Override
	@Async("threadPoolTaskExecutor")
	public Future<Long> getTerroristCount(String nric) throws IOException {
		Long count = 0L;
		try {
			DspMpTblTerroristExample example = new DspMpTblTerroristExample();
			example.createCriteria().andNricEqualTo(nric);
			count = mapper.countByExample(example);
			logger.info("Result: {}", count);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			Thread.currentThread().interrupt();
		}

		return new AsyncResult<Long>(count);
	}
}
