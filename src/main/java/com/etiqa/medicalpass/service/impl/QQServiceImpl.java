package com.etiqa.medicalpass.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etiqa.medicalpass.constant.QQConstant;
import com.etiqa.medicalpass.mapper.DspMpTblParamMapper;
import com.etiqa.medicalpass.pojo.BmiLimit;
import com.etiqa.medicalpass.pojo.DspMpTblParam;
import com.etiqa.medicalpass.pojo.DspMpTblParamExample;
import com.etiqa.medicalpass.pojo.DspMpTblQQ;
import com.etiqa.medicalpass.pojo.DspMpTblQQExample;
import com.etiqa.medicalpass.pojo.DspMpTblUwRejection;
import com.etiqa.medicalpass.service.DspMPTblQQService;
import com.etiqa.medicalpass.service.DspMpTblUwRejectionService;
import com.etiqa.medicalpass.service.QQService;
import com.etiqa.medicalpass.util.CommonUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Service
public class QQServiceImpl implements QQService {

	private static final Logger logger = LoggerFactory.getLogger(QQServiceImpl.class);

	@Autowired
	private DspMpTblParamMapper mapper;

	@Autowired
	private DspMpTblUwRejectionService rejectionService;

	@Autowired
	private DspMPTblQQService dspMPTblQQService;

	@Override
	public Map<String, Object> calculateBMI(HttpServletRequest request, String weight, String height) {

		double weightDbl = 0.0;
		double heightDbl = 0.0;
		double bmi = 0.0;

		int isPassed = 0;
		int age = 0;

		DspMpTblQQ mpQQData = null;
		Map<String, Object> map = new HashMap<>();
		try {
			HttpSession session = request.getSession();
			mpQQData = (DspMpTblQQ) session.getAttribute(QQConstant.MP_QQ_DATA_SESSION_KEY);
			BigDecimal qqIdKey = (BigDecimal) session.getAttribute(QQConstant.DSP_QQ_ID_DATA_SESSION_KEY);
			if (mpQQData != null && qqIdKey != null) {
				weightDbl = NumberUtils.toDouble(weight);
				heightDbl = NumberUtils.toDouble(height) / 100;
				bmi = weightDbl / (heightDbl * heightDbl);
				DspMpTblParamExample example = new DspMpTblParamExample();
				example.createCriteria().andParamCodeEqualTo(QQConstant.BMI_KEY);
				List<DspMpTblParam> mpTblParams = mapper.selectByExample(example);
				BmiLimit bmiLimit = null;
				age = CommonUtils.getAge(mpQQData.getDob(), QQConstant.DOB_FORM_DATE_FORMAT);
				if (age != 0) { 
					age += 1;
					for (DspMpTblParam param : mpTblParams) {
						String paramCode = StringUtils.trimToNull(param.getParamCode());
						if (paramCode != null && paramCode.equals(QQConstant.BMI_KEY)) {
							Gson gson = new Gson();
							ArrayList<BmiLimit> bmiLimits = gson.fromJson(param.getParamDesc(),
									new TypeToken<ArrayList<BmiLimit>>() {
									}.getType());
							for (int i = 0; i < bmiLimits.size(); i++) {
								bmiLimit = bmiLimits.get(i);
								if ((age >= bmiLimit.getAgeLower() && age <= bmiLimit.getAgeUpper())
										&& (bmi >= bmiLimit.getBmiLower() && bmi <= bmiLimit.getBmiUpper())) {
									isPassed = 1;
									break;
								}
							}
						}
					}
					DspMpTblQQ dspMpTblQq = new DspMpTblQQ();
					DspMpTblQQExample dspMpTblQQExample = new DspMpTblQQExample();
					dspMpTblQq.setHeight(height);
					dspMpTblQq.setWeight(weight);
					dspMpTblQq.setBmi(CommonUtils.formatBMI(String.valueOf(bmi)));
					dspMpTblQQExample.createCriteria().andDspQqIdEqualTo(qqIdKey)
							.andMpQqIdEqualTo(mpQQData.getMpQqId());
					if (isPassed != 1) {
						DspMpTblUwRejection rejection = new DspMpTblUwRejection();
						rejection.setUwQsCode(QQConstant.REJECT_CODE_BMI);
						dspMpTblQq.setRejectedReason(QQConstant.REJECT_CODE_BMI);
						rejection.setDspQqId(String.valueOf(qqIdKey));
						rejectionService.insertRejectionVoid(rejection);
					}
					dspMPTblQQService.updateDspMpTblQQVoid(dspMpTblQq, dspMpTblQQExample);
				}
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		} finally {
			map.put(QQConstant.MAP_KEY_IS_PASSED, isPassed);
			map.put("status", isPassed);
		}

		return map;
	}

}
