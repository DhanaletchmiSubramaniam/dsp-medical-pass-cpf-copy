package com.etiqa.medicalpass.service.impl;

import java.io.IOException;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import com.etiqa.medicalpass.mapper.DspMpTblUwRejectionMapper;
import com.etiqa.medicalpass.pojo.DspMpTblUwRejection;
import com.etiqa.medicalpass.pojo.DspMpTblUwRejectionExample;
import com.etiqa.medicalpass.service.DspMpTblUwRejectionService;

@Service
public class DspMpTblUwRejectionServiceImpl implements DspMpTblUwRejectionService {

	private static final Logger logger = LoggerFactory.getLogger(DspMpTblUwRejectionServiceImpl.class);

	@Autowired
	private DspMpTblUwRejectionMapper mapper;

	@Override
	@Async("threadPoolTaskExecutor")
	public Future<Long> getRejectionCount(String nric, String rejectedReason) throws IOException {
		Long count = 0L;
		try {
			DspMpTblUwRejectionExample example = new DspMpTblUwRejectionExample();
			example.createCriteria().andCustomerNricEqualTo(nric).andUwQsCodeNotEqualTo(rejectedReason);
			count = mapper.countByExample(example);
			logger.info("Result: {}", count);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			Thread.currentThread().interrupt();
		}

		return new AsyncResult<Long>(count);
	}

	@Override
	@Async("threadPoolTaskExecutor")
	public Future<Integer> insertRejection(DspMpTblUwRejection record) throws IOException {
		Integer count = 0;
		try {
			count = mapper.insertSelective(record);
			logger.info("Result: {}", count);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			Thread.currentThread().interrupt();
		}

		return new AsyncResult<Integer>(count);
	}

	@Override
	@Async("threadPoolTaskExecutor")
	public void insertRejectionVoid(DspMpTblUwRejection record) throws IOException {
		Integer count = 0;
		try {
			count = mapper.insertSelective(record);
			logger.info("Result: {}", count);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			Thread.currentThread().interrupt();
		}
	}
	
	@Override
	@Async("threadPoolTaskExecutor")
	public Future<Integer> updateSelectiveAsync(DspMpTblUwRejection record, DspMpTblUwRejectionExample example) throws IOException {
		Integer count = 0;
		try {
			count = mapper.updateByExampleSelective(record, example);
			logger.info("Result: {}", count);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			Thread.currentThread().interrupt();
		}
		return new AsyncResult<Integer>(count);
	}

	@Override
	public Integer updateSelective(DspMpTblUwRejection record, DspMpTblUwRejectionExample example) throws IOException {
		Integer count = 0;
		try {
			count = mapper.updateByExampleSelective(record, example);
			logger.info("Result: {}", count);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			Thread.currentThread().interrupt();
		}
		return count;
	}
}
