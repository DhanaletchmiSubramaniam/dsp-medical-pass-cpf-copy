package com.etiqa.medicalpass.service.impl;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import com.etiqa.medicalpass.mapper.DspMpTblQQMapper;
import com.etiqa.medicalpass.pojo.DspMpTblQQ;
import com.etiqa.medicalpass.pojo.DspMpTblQQExample;
import com.etiqa.medicalpass.service.DspMPTblQQService;

@Service
public class DspMPTblQQServiceImpl implements DspMPTblQQService {

	private static final Logger logger = LoggerFactory.getLogger(DspMPTblQQServiceImpl.class);

	@Autowired
	private DspMpTblQQMapper mapper;

	@Override
	@Async("threadPoolTaskExecutor")
	public Future<Integer> insertDspMpTblQQ(DspMpTblQQ record) throws IOException {
		Integer count = 0;
		try {
			count = mapper.insertSelective(record);
			logger.info("Result: {}", count);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			Thread.currentThread().interrupt();
		}

		return new AsyncResult<Integer>(count);
	}

	@Override
	@Async("threadPoolTaskExecutor")
	public void updateDspMpTblQQVoid(DspMpTblQQ record, DspMpTblQQExample example) throws IOException {
		Integer count = 0;
		try {
			count = mapper.updateByExampleSelective(record, example);
			logger.info("Result: {}", count);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			Thread.currentThread().interrupt();
		}
	}

	@Override
	@Async("threadPoolTaskExecutor")
	public Future<Integer> updateDspMpTblQQAsync(DspMpTblQQ record, DspMpTblQQExample example) throws IOException {
		Integer count = 0;
		try {
			count = mapper.updateByExampleSelective(record, example);
			logger.info("Result: {}", count);
		} catch (Exception ex) {
			Thread.currentThread().interrupt();
			logger.error(ex.getMessage(), ex);
		}

		return new AsyncResult<Integer>(count);
	}

	@Override
	@Async("threadPoolTaskExecutor")
	public Future<Long> getPolicyPurchasedCount(DspMpTblQQExample example) throws IOException {
		Long count = 0L;
		try {
			count = mapper.countByExample(example);
			logger.info("Result: {}", count);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			Thread.currentThread().interrupt();
		}

		return new AsyncResult<Long>(count);
	}

	@Override
	@Async("threadPoolTaskExecutor")
	public Future<List<DspMpTblQQ>> getPolicyPurchased(DspMpTblQQExample example) throws IOException {
		List<DspMpTblQQ> lists = null;
		try {
			lists = mapper.selectByExample(example);
			logger.info("Result: {}", lists.size());
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			Thread.currentThread().interrupt();
		}

		return new AsyncResult<List<DspMpTblQQ>>(lists);
	}
}
