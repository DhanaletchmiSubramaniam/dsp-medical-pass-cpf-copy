package com.etiqa.medicalpass.service.impl;

import com.etiqa.medicalpass.constant.QQConstant;
import com.etiqa.medicalpass.email.DspEmailTemplateBean;
import com.etiqa.medicalpass.pojo.DspCommonTblCustomer;
import com.etiqa.medicalpass.pojo.DspMpTblQQ;
import com.etiqa.medicalpass.pojo.PaymentResponse;
import com.etiqa.medicalpass.service.ProductMailTemplateService;

public class ProductMailTemplateServiceImpl implements ProductMailTemplateService {

	@Override
	public DspEmailTemplateBean loadMailTemplate(DspMpTblQQ mpQqData, PaymentResponse paymentResponse,
			DspCommonTblCustomer dspCommonTblCustomer, String company) {

		DspEmailTemplateBean templateBean = new DspEmailTemplateBean();
		
		String productName = null;
		String policyTerm = null;
		String paymentTerm = null;
		String entityTerm = null;

		
		if (company.equals(QQConstant.INSURANCE_COMPANY_KEY)) {
			productName = QQConstant.PRODUCT_NAME_INSURANCE;
			policyTerm = "Policy";
			paymentTerm = "Premium";
			entityTerm = "Etiqa Life Insurance Berhad";
		}else{
			productName = QQConstant.PRODUCT_NAME_TAKAFUL;
			policyTerm = "Certificate";
			paymentTerm = "Contribution";
			entityTerm = "Etiqa Family Takaful Berhad";
		}

		String subject = productName + " Detail (" + paymentResponse.getPolicyNumber() + ")";

		String premium = mpQqData.getPremiumPaymentFrequency();
		String frequencyType = "";
		if (mpQqData.getPremiumPaymentFrequencyType().equals("annualy")) {
			frequencyType = "Annually";
		} else {
			frequencyType = "Monthly";
		}
		StringBuilder sb = new StringBuilder();

		sb.append("<b>Dear   ");
		sb.append(dspCommonTblCustomer.getCustomerName());
		sb.append(",</b><br><br><br>");
		sb.append("Congratulations! Your transaction is successful. We are pleased to extend our warmest welcome to you for being a part of our family.<br><br>");
		sb.append("Please find the copy of your e-policy and receipt as attached for your reference.<br><br>");
		sb.append("<b>");
		sb.append(productName);
		sb.append("</b><br>");
		sb.append("<hr><table> <col width=");
		sb.append(250);
		sb.append(">");
		sb.append("<tr><td><b>Transaction Date</b></td><td><b>:</b>");
		sb.append(paymentResponse.getTransactionDateTime());
		sb.append("</td></tr><br>");
		sb.append("<tr><td><b>Transaction No.</b></td><td><b>:</b>");
		sb.append(paymentResponse.getReceiptNumber());
		sb.append("</td></tr><br>");
		sb.append("<tr><td><b>");
		sb.append(policyTerm);
		sb.append(" Holder Name</b></td><td><b>:</b>");
		sb.append(dspCommonTblCustomer.getCustomerName());
		sb.append("</td></tr><br>");
		sb.append("<tr><td><b>NRIC No.</b></td><td><b>:</b>");
		sb.append(dspCommonTblCustomer.getCustomerNricId());
		sb.append("</td></tr><br>");
		sb.append("<tr><td><b>Plan Name</b></td><td><b>:</b>");
		sb.append(productName);
		sb.append("</td></tr><br>");
		sb.append("<tr><td><b>");
		sb.append(paymentTerm);
		sb.append(" Frequency</b></td><td><b>:</b>");
		sb.append(frequencyType);
		sb.append("</td></tr><br>");
		sb.append("<tr><td><b>E-");
		sb.append(policyTerm);
		sb.append(" No.</b></td><td><b>:</b>");
		sb.append(paymentResponse.getPolicyNumber());
		sb.append("</td></tr><br>");
		sb.append("<tr><td><b>Receipt No.</b></td><td><b>:</b>");
		sb.append(paymentResponse.getPolicyNumber());
		sb.append("</td></tr><br>");
		sb.append("<tr><td><b>Payment Mode</b></td><td><b>:</b>Credit Card (VISA/MASTER) </td></tr><br>");
		sb.append("<tr><td><b>");
		sb.append(paymentTerm);
		sb.append(" Amount (RM)</b></td><td><b>:</b>");
		sb.append(premium);
		sb.append("</td></tr><br>");
		sb.append("</table> <br><br><br>"); 
		sb.append("Please visit www.etiqa.com.my if you wish to perform nomination in the future.");
		sb.append("<br><br>"); 
		sb.append("Note: This is an auto email notification. Please do not reply this email. You may call our Etiqa"); 
		sb.append("Oneline call centre at 1300 13 8888 or email us at info@etiqa.com.my.<br><br>"); 
		sb.append("Thank you.<br><br>"); 
		sb.append("Yours Sincerely,<br>"); 
		sb.append(entityTerm); 
		sb.append("<br><br>"); 

		String templateBody = sb.toString();
		templateBean.setTemplateSubject(subject);
		templateBean.setTemplateBody(templateBody);
		templateBean.setEmailTo(dspCommonTblCustomer.getCustomerEmail());
		return templateBean;
	}
}
