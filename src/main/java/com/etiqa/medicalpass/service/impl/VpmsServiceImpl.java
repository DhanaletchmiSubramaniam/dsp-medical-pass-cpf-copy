package com.etiqa.medicalpass.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Properties;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.etiqa.medicalpass.constant.QQConstant;
import com.etiqa.medicalpass.mapper.DspMpTblVpmsMapper;
import com.etiqa.medicalpass.pojo.DspMpTblVpms;
import com.etiqa.medicalpass.pojo.VpmsRequest;
import com.etiqa.medicalpass.pojo.VpmsResponse;
import com.etiqa.medicalpass.pojo.VpmsResponseData;
import com.etiqa.medicalpass.pojo.VpmsResponseTakaful;
import com.etiqa.medicalpass.service.VpmsService;
import com.etiqa.medicalpass.util.InputOutputStreamUtil;
import com.google.gson.Gson;

@Service
public class VpmsServiceImpl implements VpmsService {

	private static final Logger logger = LoggerFactory.getLogger(VpmsServiceImpl.class);

	@Autowired
	private DspMpTblVpmsMapper mapper;

	private Properties prop = new Properties();

	@Override
	public VpmsResponseTakaful getVpmsTakaful(VpmsRequest vpmsRequest) {

		VpmsResponseTakaful vpmsResponseTakaful = new VpmsResponseTakaful();
		InputStream is = this.getClass().getClassLoader().getResourceAsStream(QQConstant.PROPERTIES_FILE_NAME);

		try {
			prop.load(is);
			String url = prop.getProperty(QQConstant.VPMS_TAKAFUL_URL_KEY);
			RestTemplate restTemplate = new RestTemplate();
			Gson gson = new Gson();
			JSONObject requestJson = new JSONObject(gson.toJson(vpmsRequest));

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.add("REMOTE_TOKEN", prop.getProperty(QQConstant.VPMS_PASS_KEY));
			headers.add("Content-Type", "application/json");

			logger.info("{}", requestJson);

			HttpEntity<String> entity = new HttpEntity<>(requestJson.toString(), headers);
			ResponseEntity<String> loginResponse = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
			String body = loginResponse.getBody();
			logger.info(body);

			vpmsResponseTakaful = gson.fromJson(body, VpmsResponseTakaful.class);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		} finally {
			InputOutputStreamUtil.close(is);
		}

		return vpmsResponseTakaful;
	}

	@Override
	public VpmsResponse getVpmsInsurance(VpmsRequest vpmsRequest) {

		VpmsResponse vpmsResponse = new VpmsResponse();
		InputStream is = this.getClass().getClassLoader().getResourceAsStream(QQConstant.PROPERTIES_FILE_NAME);
		try {
			prop.load(is);
			String url = prop.getProperty(QQConstant.VPMS_INSURANCE_URL_KEY);
			RestTemplate restTemplate = new RestTemplate();
			Gson gson = new Gson();
			JSONObject requestJson = new JSONObject(gson.toJson(vpmsRequest));

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.add("REMOTE_TOKEN", prop.getProperty(QQConstant.VPMS_PASS_KEY));
			headers.add("Content-Type", "application/json");

			logger.info("{}", requestJson);

			HttpEntity<String> entity = new HttpEntity<>(requestJson.toString(), headers);
			ResponseEntity<String> loginResponse = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
			String body = loginResponse.getBody();
			logger.info(body);

			vpmsResponse = gson.fromJson(body, VpmsResponse.class);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		} finally {
			InputOutputStreamUtil.close(is);
		}

		return vpmsResponse;
	}

	@Override
	@Async("threadPoolTaskExecutor")
	public void insertDspMpTblVpmsAsyncVoid(DspMpTblVpms record) throws IOException {
		Integer count = 0;
		try {
			count = mapper.insertSelective(record);
			logger.info("Result: {}", count);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			Thread.currentThread().interrupt();
		}
	}

	@Override
	public DspMpTblVpms compileIntoDspMpTblVpms(BigDecimal dspQqId, VpmsRequest vpmsRequest, VpmsResponse vpmsResponse,
			VpmsResponseData vpmsResponseData) {
		DspMpTblVpms record = new DspMpTblVpms();
		try {
			record.setDspQqIdReq(dspQqId);
			record.setLanguageReq(vpmsRequest.getLang());
			record.setPlantypeReq(vpmsRequest.getPlanType());
			record.setModeofpaymentReq(vpmsRequest.getPaymentMode());
			record.setOccupationReq(vpmsRequest.getOccupation());
			record.setDobReq(vpmsRequest.getDateOfBirth());
			record.setSmokerReq(vpmsRequest.getIsSmoker());
			record.setQuotationdateReq(vpmsRequest.getQuotationDate());
			record.setNameReq(vpmsRequest.getName());
			record.setNricReq(vpmsRequest.getNric());
			record.setDeductibleReq(vpmsRequest.getDeductible());
			record.setStatusResp(vpmsResponse.getStatus());
			record.setMessageResp(vpmsResponse.getMessage());
			record.setVersionResp(vpmsResponseData.getVersion());
			record.setQuotationdateResp(vpmsResponseData.getQuotationDate());
			record.setPremiumpaymenttermResp(vpmsResponseData.getPremiumPaymentTerm());
			record.setMonthlypremiumResp(vpmsResponseData.getMonthlyPremium());
			record.setAnnualpremiumResp(vpmsResponseData.getAnnualPremium());
			record.setNameResp(vpmsResponseData.getName());
			record.setGenderResp(vpmsResponseData.getGender());
			record.setSmokerResp(vpmsResponseData.getSmoker());
			record.setOccupationclassResp(vpmsResponseData.getOccupationClass());
			record.setNricResp(vpmsResponseData.getNric());
			record.setDobResp(vpmsResponseData.getDob());
			record.setAgenextbirthdayResp(vpmsResponseData.getAgeNextBirthday());
			record.setPremiummodebaseResp(vpmsResponseData.getPremiumMode());
			record.setPlannameResp(vpmsResponseData.getPlanName());
			record.setPolicytermResp(vpmsResponseData.getPolicyTerm());
			record.setPaymenttermResp(vpmsResponseData.getPaymentTerm());
			record.setPlantypeResp(vpmsResponseData.getPlanType());
			record.setDeductibleResp(vpmsResponseData.getDeductible());
			record.setFirstyearpremiumResp(vpmsResponseData.getFirstYearPremium());
			record.setPremiummodeResp(vpmsResponseData.getPremiumMode());
			record.setEndcoveredageResp(vpmsResponseData.getEndCoveredAgeAndWakalah());
			record.setAnnualpremiumarrayResp(vpmsResponseData.getAnnualPremiumString());
			record.setTotalpremiumtodateResp(vpmsResponseData.getTotalPremiumToDateString());
			record.setEndcoveredagearrayResp(vpmsResponseData.getEndCoveredAgeArrayInString());
			record.setPlanbenefitResp(vpmsResponseData.getPlanBenefit());
			record.setAnnuallimitResp(vpmsResponseData.getAnnualLimit());
			record.setLifetimelimitResp(vpmsResponseData.getLifetimeLimit());
			record.setRoomrateperdayResp(vpmsResponseData.getRoomRatePerDay());
			record.setRoomannuallimitResp(vpmsResponseData.getRoomAnnualLimit());
			record.setDeductibleoptionResp(vpmsResponseData.getDeductibleOption());
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return record;
	}
}
