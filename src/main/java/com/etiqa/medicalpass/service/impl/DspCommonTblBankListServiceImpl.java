package com.etiqa.medicalpass.service.impl;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etiqa.medicalpass.mapper.DspCommonTblBankListMapper;
import com.etiqa.medicalpass.pojo.DspCommonTblBankList;
import com.etiqa.medicalpass.pojo.DspCommonTblBankListExample;
import com.etiqa.medicalpass.service.DspCommonTblBankListService;

@Service
public class DspCommonTblBankListServiceImpl implements DspCommonTblBankListService {

	private static final Logger logger = LoggerFactory.getLogger(DspCommonTblBankListServiceImpl.class);

	@Autowired
	private DspCommonTblBankListMapper mapper;

	@Override
	public List<DspCommonTblBankList> getBanks() throws IOException {
		List<DspCommonTblBankList> bankList = null;
		try {
			DspCommonTblBankListExample example = new DspCommonTblBankListExample();
			bankList = mapper.selectByExample(example);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}

		return bankList;
	}

}
