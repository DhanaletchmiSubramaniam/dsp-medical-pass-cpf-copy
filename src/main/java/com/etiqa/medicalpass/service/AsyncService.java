package com.etiqa.medicalpass.service;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.Future;

public interface AsyncService {

	public Future<Map<String, Object>> getMDMCustomerService(String nric, String url) throws IOException;

	public Future<Integer> getMDMClaimHistoryService(String nric, String url) throws IOException;

}
