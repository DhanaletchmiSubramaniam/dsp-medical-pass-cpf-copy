package com.etiqa.medicalpass.service;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.etiqa.medicalpass.pojo.DspTlTblOcc;

public interface DspTlTblOccService {

	public List<DspTlTblOcc> getOccupation(HttpServletRequest request, String occupation) throws IOException;

}
