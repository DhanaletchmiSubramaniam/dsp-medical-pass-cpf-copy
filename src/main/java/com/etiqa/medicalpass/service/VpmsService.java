package com.etiqa.medicalpass.service;

import java.io.IOException;
import java.math.BigDecimal;

import com.etiqa.medicalpass.pojo.DspMpTblVpms;
import com.etiqa.medicalpass.pojo.VpmsRequest;
import com.etiqa.medicalpass.pojo.VpmsResponse;
import com.etiqa.medicalpass.pojo.VpmsResponseData;
import com.etiqa.medicalpass.pojo.VpmsResponseTakaful;

public interface VpmsService {

	public VpmsResponseTakaful getVpmsTakaful(VpmsRequest vpmsRequest) throws Exception;

	public VpmsResponse getVpmsInsurance(VpmsRequest vpmsRequest) throws Exception;

	public void insertDspMpTblVpmsAsyncVoid(DspMpTblVpms record) throws IOException;

	public DspMpTblVpms compileIntoDspMpTblVpms(BigDecimal dspQqId, VpmsRequest vpmsRequest, VpmsResponse vpmsResponse,
			VpmsResponseData vpmsResponseData);

}
