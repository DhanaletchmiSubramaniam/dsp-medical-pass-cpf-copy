package com.etiqa.medicalpass.service;

import java.io.IOException;
import java.util.concurrent.Future;

import com.etiqa.medicalpass.pojo.DspMpTblUwRejection;
import com.etiqa.medicalpass.pojo.DspMpTblUwRejectionExample;

public interface DspMpTblUwRejectionService {

	public Future<Long> getRejectionCount(String nric, String rejectedReason) throws IOException;

	public Future<Integer> insertRejection(DspMpTblUwRejection record) throws IOException;
	
	public Future<Integer> updateSelectiveAsync(DspMpTblUwRejection record, DspMpTblUwRejectionExample example)
			throws IOException;

	public void insertRejectionVoid(DspMpTblUwRejection record) throws IOException;

	public Integer updateSelective(DspMpTblUwRejection record, DspMpTblUwRejectionExample example) throws IOException;

}
