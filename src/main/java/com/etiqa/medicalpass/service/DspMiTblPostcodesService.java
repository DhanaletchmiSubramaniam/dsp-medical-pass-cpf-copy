package com.etiqa.medicalpass.service;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.etiqa.medicalpass.pojo.DspMiTblPostcodes;

public interface DspMiTblPostcodesService {

	public List<DspMiTblPostcodes> getState(HttpServletRequest request, String postcode) throws IOException;

}
