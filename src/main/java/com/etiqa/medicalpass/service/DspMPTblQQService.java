package com.etiqa.medicalpass.service;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Future;

import com.etiqa.medicalpass.pojo.DspMpTblQQ;
import com.etiqa.medicalpass.pojo.DspMpTblQQExample;

public interface DspMPTblQQService {

	public Future<Integer> insertDspMpTblQQ(DspMpTblQQ record) throws IOException;

	public void updateDspMpTblQQVoid(DspMpTblQQ record, DspMpTblQQExample example) throws IOException;

	public Future<Integer> updateDspMpTblQQAsync(DspMpTblQQ record, DspMpTblQQExample example) throws IOException;

	public Future<Long> getPolicyPurchasedCount(DspMpTblQQExample example) throws IOException;

	public Future<List<DspMpTblQQ>> getPolicyPurchased(DspMpTblQQExample example) throws IOException;
}
