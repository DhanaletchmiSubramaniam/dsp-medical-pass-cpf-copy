package com.etiqa.medicalpass.db;

import java.sql.Connection;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConnectionFactory {

	private static final Logger logger = LoggerFactory.getLogger(ConnectionFactory.class);

	public static Connection getConnection() {
		Context ctx = null;
		Connection conn = null;
		try {

			ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup("jdbc/dspuser");
			conn = ds.getConnection();

		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return conn;

	}

}
