package com.etiqa.medicalpass.controller.exception;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.etiqa.medicalpass.constant.QQConstant;
import com.etiqa.medicalpass.pojo.ApiErrorResponse;
import com.etiqa.medicalpass.util.ExceptionUtils;

@RestController
public class GlobalRestExceptionHandler {

	protected Logger logger;

	public GlobalRestExceptionHandler() {
		logger = LoggerFactory.getLogger(getClass());
	}

	@ResponseStatus(HttpStatus.CONFLICT)
	@ExceptionHandler(value = { IllegalArgumentException.class, IllegalStateException.class,
			DataIntegrityViolationException.class })
	protected ResponseEntity<ApiErrorResponse> handleConflict(Exception ex) {
		ApiErrorResponse apiErrorResponse = new ApiErrorResponse();
		apiErrorResponse.setStatus(HttpStatus.CONFLICT);
		apiErrorResponse.setErrorCode("TC003");
		apiErrorResponse.setMessage("Page Conflict");
		apiErrorResponse.setDatetime(new DateTime());
		apiErrorResponse.setDetails(ExceptionUtils.toArrayString(ex));
		logger.error(apiErrorResponse.toString(), ex);

		return new ResponseEntity<ApiErrorResponse>(apiErrorResponse, HttpStatus.CONFLICT);
	}

	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(value = NoHandlerFoundException.class)
	protected ResponseEntity<ApiErrorResponse> handleNotFound(Exception ex) {
		ApiErrorResponse apiErrorResponse = new ApiErrorResponse();
		apiErrorResponse.setStatus(HttpStatus.NOT_FOUND);
		apiErrorResponse.setErrorCode("TC001");
		apiErrorResponse.setMessage("Page Not Found");
		apiErrorResponse.setDatetime(new DateTime());
		apiErrorResponse.setDetails(ExceptionUtils.toArrayString(ex));
		logger.error(apiErrorResponse.toString(), ex);

		return new ResponseEntity<ApiErrorResponse>(apiErrorResponse, HttpStatus.NOT_FOUND);
	}

	@ResponseStatus(HttpStatus.NOT_FOUND)
	@RequestMapping("**")
	@ExceptionHandler(value = Exception.class)
	protected void handleGeneralError(Exception ex, HttpServletResponse response) {
		try {
			ApiErrorResponse apiErrorResponse = new ApiErrorResponse();
			apiErrorResponse.setStatus(HttpStatus.NOT_FOUND);
			apiErrorResponse.setErrorCode("TC000");
			apiErrorResponse.setMessage(ex.getClass().getSimpleName());
			apiErrorResponse.setDatetime(new DateTime());
			apiErrorResponse.setDetails(ExceptionUtils.toArrayString(ex));
			logger.error(apiErrorResponse.toString(), ex);
			response.sendRedirect(QQConstant.PAGE_URL_ERROR_EN);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler({ SQLException.class, DataAccessException.class })
	public ResponseEntity<ApiErrorResponse> handleDatabaseError(Exception ex) {
		ApiErrorResponse apiErrorResponse = new ApiErrorResponse();
		apiErrorResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
		apiErrorResponse.setErrorCode("TC002");
		apiErrorResponse.setMessage(ex.getClass().getSimpleName());
		apiErrorResponse.setDatetime(new DateTime());
		apiErrorResponse.setDetails(ExceptionUtils.toArrayString(ex));
		logger.error(apiErrorResponse.toString(), ex);

		return new ResponseEntity<ApiErrorResponse>(apiErrorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@RequestMapping("/error")
	protected ResponseEntity<ApiErrorResponse> error(Exception ex) {
		ApiErrorResponse apiErrorResponse = new ApiErrorResponse();
		apiErrorResponse.setStatus(HttpStatus.NOT_FOUND);
		apiErrorResponse.setErrorCode("TC000");
		apiErrorResponse.setMessage(ex.getMessage());
		apiErrorResponse.setDatetime(new DateTime());
		apiErrorResponse.setDetails(ExceptionUtils.toArrayString(ex));
		logger.error(apiErrorResponse.toString(), ex);

		return new ResponseEntity<ApiErrorResponse>(apiErrorResponse, HttpStatus.NOT_FOUND);
	}
}