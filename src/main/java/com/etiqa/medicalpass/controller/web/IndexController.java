package com.etiqa.medicalpass.controller.web;

import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.etiqa.medicalpass.constant.QQConstant;

@Controller
public class IndexController {

	@Value("#{servletContext.contextPath}")
	private String servletContextPath;

	@RequestMapping(value = QQConstant.REQUEST_MAPPING_INDEX, method = RequestMethod.GET)
	public String index(Locale locale, Model model, HttpSession session) {
		return "index";
	}

	@RequestMapping(value = QQConstant.REQUEST_MAPPING_ENGLISH
			+ QQConstant.REQUEST_MAPPING_QQ_INSURANCE_PREFIX, method = RequestMethod.GET)
	public String insuranceMaskUrl(Locale locale, Model model) {
		return QQConstant.PAGE_DIRECTORY_INSURANCE_EN_URL_MASK;
	}

	@RequestMapping(value = QQConstant.REQUEST_MAPPING_ENGLISH
			+ QQConstant.REQUEST_MAPPING_QQ_TAKAFUL_PREFIX, method = RequestMethod.GET)
	public String takafulMaskUrl(Locale locale, Model model) {
		return QQConstant.PAGE_DIRECTORY_TAKAFUL_EN_URL_MASK;
	}

	@RequestMapping(value = QQConstant.REQUEST_MAPPING_ENGLISH
			+ QQConstant.REQUEST_MAPPING_QQ_ERROR, method = RequestMethod.GET)
	public String error(Locale locale, Model model) {
		return QQConstant.PAGE_DIRECTORY_ERROR_INSURANCE_EN;
	}
}
