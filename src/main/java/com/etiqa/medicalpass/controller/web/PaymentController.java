package com.etiqa.medicalpass.controller.web;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.etiqa.medicalpass.constant.QQConstant;
import com.etiqa.medicalpass.controller.rest.DocGenController;
import com.etiqa.medicalpass.db.ConnectionFactory;
import com.etiqa.medicalpass.email.CiZipFileGeneration;
import com.etiqa.medicalpass.email.DspEmailDispatchProcessor;
import com.etiqa.medicalpass.pojo.DocCustomPojo;
import com.etiqa.medicalpass.pojo.DspCommonTblCustomer;
import com.etiqa.medicalpass.pojo.DspCommonTblPayment;
import com.etiqa.medicalpass.pojo.DspCommonTblPaymentExample;
import com.etiqa.medicalpass.pojo.DspCommonTblPolicy;
import com.etiqa.medicalpass.pojo.DspCommonTblPolicyExample;
import com.etiqa.medicalpass.pojo.DspMpTblCreditCardOwner;
import com.etiqa.medicalpass.pojo.DspMpTblQQ;
import com.etiqa.medicalpass.pojo.DspMpTblQQExample;
import com.etiqa.medicalpass.pojo.DspTlTblOcc;
import com.etiqa.medicalpass.pojo.GeneratingReportsResponseVo;
import com.etiqa.medicalpass.pojo.PaymentInsert;
import com.etiqa.medicalpass.pojo.PaymentResponse;
import com.etiqa.medicalpass.pojo.VpmsRequest;
import com.etiqa.medicalpass.pojo.VpmsResponseData;
import com.etiqa.medicalpass.service.DspCommonTblBankListService;
import com.etiqa.medicalpass.service.DspCommonTblPolicyService;
import com.etiqa.medicalpass.service.DspMPTblQQService;
import com.etiqa.medicalpass.service.DspMpTblCreditCardOwnerService;
import com.etiqa.medicalpass.service.PaymentService;
import com.etiqa.medicalpass.service.impl.ProductMailTemplateServiceImpl;
import com.etiqa.medicalpass.util.CommonUtils;
import com.etiqa.medicalpass.util.InputOutputStreamUtil;
import com.etiqa.medicalpass.util.MpayUtils;

@Controller
@RequestMapping(value = QQConstant.REQUEST_MAPPING_PAYMENT_PREFIX)
public class PaymentController {

	private static final Logger logger = LoggerFactory.getLogger(PaymentController.class);

	@Autowired
	private PaymentService paymentService;

	@Autowired
	private DspCommonTblPolicyService policyService;

	@Autowired
	private DspMPTblQQService mpQqService;

	@Autowired
	private DspMpTblCreditCardOwnerService mpCreditCardService;

	@Autowired
	private DspCommonTblBankListService dspCommonTblBankListService;

	@Value("#{servletContext.contextPath}")
	private String servletContextPath;

	private Properties prop = new Properties();

	@RequestMapping(value = QQConstant.REQUEST_MAPPING_PAYMENT_START, method = RequestMethod.POST)
	public String startPayment(@RequestParam String company, @RequestParam String nameCcOwner,
			@RequestParam String nricCcOwner, @RequestParam String relationshipCcOwner, Model model,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		DspMpTblQQ mpQQData = null;
		String page = QQConstant.PAGE_DIRECTORY_START_PAYMENT;
		InputStream is = null;
		try {
			is = this.getClass().getClassLoader().getResourceAsStream(QQConstant.PROPERTIES_FILE_NAME);
			prop.load(is);
			HttpSession session = request.getSession();
			CommonUtils.clearSession(QQConstant.PAYMENT_RESPONSE_SESSION_KEY, session);
			mpQQData = (DspMpTblQQ) session.getAttribute(QQConstant.MP_QQ_DATA_SESSION_KEY);
			BigDecimal qqIdKey = (BigDecimal) session.getAttribute(QQConstant.DSP_QQ_ID_DATA_SESSION_KEY);
			DspCommonTblCustomer dspCommonTblCustomer = (DspCommonTblCustomer) session
					.getAttribute(QQConstant.COMMON_CUSTOMER_DATA_SESSION_KEY);
			if (mpQQData != null && qqIdKey != null && dspCommonTblCustomer != null) {
				PaymentInsert paymentInsert = new PaymentInsert();
				StringBuffer url = request.getRequestURL();
				String baseUrl = url.substring(0, StringUtils.ordinalIndexOf(url, "/", 4));
				String postUrl = baseUrl + QQConstant.PAGE_URL_VERIFY_PAYMENT;
				String mpayUrl = prop.getProperty(QQConstant.MPAY_PAYMENT_URL_KEY);

				String mid = null;
				String amount = mpQQData.getPremiumPaymentFrequency();
				String productCode = null;
				String productName = null;

				if (company.equals(QQConstant.INSURANCE_COMPANY_KEY)) {
					productName = QQConstant.PRODUCT_NAME_INSURANCE;
					productCode = QQConstant.PAYMENT_PRODUCT_NAME_INSURANCE;
					mid = QQConstant.ETIQA_MID_CODE_INSURANCE;
					paymentInsert.setPaymentGatewayCode(QQConstant.PAYMENT_GATEWAY_CODE_INSURANCE);
				} else if (company.equals(QQConstant.TAKAFUL_COMPANY_KEY)) {
					productName = QQConstant.PRODUCT_NAME_TAKAFUL;
					productCode = QQConstant.PAYMENT_PRODUCT_NAME_TAKAFUL;
					mid = QQConstant.ETIQA_MID_CODE_TAKAFUL;
					paymentInsert.setPaymentGatewayCode(QQConstant.PAYMENT_GATEWAY_CODE_TAKAFUL);
				} else {
					throw new NullPointerException(QQConstant.ERROR_MESSAGE_COMPANY_NAME_NULL);
				}

				nameCcOwner = StringUtils.trimToNull(nameCcOwner);
				nricCcOwner = StringUtils.trimToNull(nricCcOwner);
				relationshipCcOwner = StringUtils.trimToNull(relationshipCcOwner);

				if (nameCcOwner != null && nricCcOwner != null && relationshipCcOwner != null) {
					DspMpTblCreditCardOwner dspMpTblCreditCardOwner = new DspMpTblCreditCardOwner();
					dspMpTblCreditCardOwner.setDspQqId(qqIdKey);
					dspMpTblCreditCardOwner.setCustomerName(nameCcOwner);
					dspMpTblCreditCardOwner.setCustomerNricId(nricCcOwner);
					dspMpTblCreditCardOwner.setCustomerRelationship(relationshipCcOwner);
					mpCreditCardService.insertDspMpTblCreditCardOwnerVoidAsync(dspMpTblCreditCardOwner);
				}

				paymentInsert.setDspQqId(qqIdKey);
				paymentInsert.setAmount(amount);
				paymentInsert.setProductCode(productCode);
				paymentInsert.setProductPlanCode(mpQQData.getProductCode());
				paymentInsert.setPaymentStatus(QQConstant.PAYMENT_STATUS_ATTEMPT_PAYMENT);
				paymentService.insertPayment(paymentInsert);

				String receiptNo = paymentInsert.getReceiptNumber();

				String name = dspCommonTblCustomer.getCustomerName();
				String email = dspCommonTblCustomer.getCustomerEmail();
				String mobile = dspCommonTblCustomer.getCustomerMobileNo();
				String address1 = dspCommonTblCustomer.getCustomerAddress1();
				String address2 = dspCommonTblCustomer.getCustomerAddress2();
				String postcode = dspCommonTblCustomer.getCustomerPostcode();
				String state = dspCommonTblCustomer.getCustomerState();
				String country = dspCommonTblCustomer.getCustomerCountry();
				String description = productName;

				String tmpAmt = StringUtils.leftPad(StringUtils.replace(amount, ".", ""), 12, '0');
				String secureHashKey = generateRequestSecureHashKey(mid, receiptNo, tmpAmt, company);
				model.addAttribute("mid", mid);
				model.addAttribute("policyNo", receiptNo);
				model.addAttribute("amt", tmpAmt);
				model.addAttribute("secureHashKey", StringUtils.upperCase(secureHashKey));
				model.addAttribute("name", dspCommonTblCustomer.getCustomerName());
				model.addAttribute("email", dspCommonTblCustomer.getCustomerEmail());
				model.addAttribute("mobile", dspCommonTblCustomer.getCustomerMobileNo());
				model.addAttribute("address1", dspCommonTblCustomer.getCustomerAddress1());
				model.addAttribute("address2", dspCommonTblCustomer.getCustomerAddress2());
				model.addAttribute("postcode", postcode);
				model.addAttribute("state", state);
				model.addAttribute("country", country);
				model.addAttribute("description", productName);
				model.addAttribute("postUrl", postUrl);
				model.addAttribute("mpayUrl", mpayUrl);
				model.addAttribute("recflag", QQConstant.REQUEST_PAYMENT_VALUE_RECFLAG);

				Map<String, String> params = new HashMap<>();
				params.put(QQConstant.REQUEST_PAYMENT_KEY_SECURE_HASH, StringUtils.upperCase(secureHashKey));
				params.put(QQConstant.REQUEST_PAYMENT_KEY_MID, mid);
				params.put(QQConstant.REQUEST_PAYMENT_KEY_INVOICE_NUMBER, receiptNo);
				params.put(QQConstant.REQUEST_PAYMENT_KEY_AMOUNT, amount);
				params.put(QQConstant.REQUEST_PAYMENT_KEY_RECFLAG, QQConstant.REQUEST_PAYMENT_VALUE_RECFLAG);
				params.put(QQConstant.REQUEST_PAYMENT_KEY_CUSTOMER_NAME, name);
				params.put(QQConstant.REQUEST_PAYMENT_KEY_CUSTOMER_EMAIL, email);
				params.put(QQConstant.REQUEST_PAYMENT_KEY_CUSTOMER_PHONE_NUMBER, mobile);
				params.put(QQConstant.REQUEST_PAYMENT_KEY_CUSTOMER_ADDRESS_1, address1);
				params.put(QQConstant.REQUEST_PAYMENT_KEY_CUSTOMER_ADDRESS_2, address2);
				params.put(QQConstant.REQUEST_PAYMENT_KEY_CUSTOMER_POSTAL, postcode);
				params.put(QQConstant.REQUEST_PAYMENT_KEY_CUSTOMER_CITY, "");
				params.put(QQConstant.REQUEST_PAYMENT_KEY_CUSTOMER_PROVINCE, "");
				params.put(QQConstant.REQUEST_PAYMENT_KEY_CUSTOMER_STATE, state);
				params.put(QQConstant.REQUEST_PAYMENT_KEY_CUSTOMER_COUNTRY, country);
				params.put(QQConstant.REQUEST_PAYMENT_KEY_DESCRIPTION, description);
				params.put(QQConstant.REQUEST_PAYMENT_KEY_POST_URL, postUrl);
				batchInsertPaymentRecord(receiptNo, params, QQConstant.REQUEST_PAYMENT_INSERT_QUERY,
						QQConstant.TRANSACTION_TYPE_REQUEST);
			} else {
				throw new NullPointerException(QQConstant.ERROR_MESSAGE_SESSION_EXPIRED);
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			page = QQConstant.PAGE_DIRECTORY_ERROR_INSURANCE_EN;
		} finally {
			InputOutputStreamUtil.close(is);
		}

		return page;
	}

	@RequestMapping(value = QQConstant.REQUEST_MAPPING_PAYMENT_VERIFY, method = RequestMethod.GET)
	public void verifyPayment(@RequestParam String result, @RequestParam String secureHash, Model model,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		String pageFailed = QQConstant.PAGE_URL_INSURACE_QQ4_FAILED_EN_MASKED;
		String pageSuccess = QQConstant.PAGE_URL_INSURACE_QQ4_SUCCESS_EN_MASKED;
		String page = pageFailed;

		HttpSession session = request.getSession();
		String responseCode = "";

		try {
			secureHash = StringUtils.trimToNull(secureHash);
			result = StringUtils.trimToNull(result);
			PaymentResponse paymentResponse = (PaymentResponse) session
					.getAttribute(QQConstant.PAYMENT_RESPONSE_SESSION_KEY);

			if (secureHash != null && result != null) {
				if (paymentResponse == null) {
					request.setAttribute(QQConstant.RESPONSE_HASH_SESSION_KEY, secureHash);
					String authCode = null;
					String invoiceNo = null;
					String amount = null;
					String transactionId = null;
					String card4Digit = null;
					String cardType = null;
					String paymentStatus = null;
					String quotationStatus = null;
					Map<String, String> params = new HashMap<>();
					String strDateTime = CommonUtils.receiptDate();
					result = StringUtils.trimToNull(result);
					if (result != null && result.length() == 55) {
						responseCode = result.substring(0, 1);
						authCode = result.substring(1, 7);
						invoiceNo = result.substring(7, 27);
						card4Digit = result.substring(27, 31);
						amount = result.substring(31, 43);
						cardType = result.substring(43, 45);
						transactionId = result.substring(45, 55);
					}

					invoiceNo = StringUtils.stripStart(invoiceNo, QQConstant.AMOUNT_LEFT_PADDING_CHARACTER);

					String company = null;
					company = CommonUtils.getCompanyFromPolicyNo(invoiceNo);
					CommonUtils.clearSession(QQConstant.COMPANY_SESSION_KEY, session);
					if (company.equals(QQConstant.TAKAFUL_COMPANY_KEY)) {
						pageSuccess = QQConstant.PAGE_URL_TAKAFUL_QQ4_SUCCESS_EN_MASKED;
						pageFailed = QQConstant.PAGE_URL_TAKAFUL_QQ4_FAILED_EN_MASKED;
						session.setAttribute(QQConstant.COMPANY_SESSION_KEY, QQConstant.TAKAFUL_COMPANY_KEY);
					}else{
						session.setAttribute(QQConstant.COMPANY_SESSION_KEY, QQConstant.INSURANCE_COMPANY_KEY);
					}

					String secureHashKeyGenerated = StringUtils
							.upperCase(generateResponseSecureHashKey(result, company));
					secureHash = StringUtils.upperCase(secureHash);

					amount = CommonUtils.formatPremium(NumberUtils
							.toDouble(StringUtils.stripStart(amount, QQConstant.AMOUNT_LEFT_PADDING_CHARACTER)) / 100);

					if (responseCode.equals(QQConstant.PAYMENT_RESPOND_CODE_SUCCESS)
							&& secureHashKeyGenerated.equals(secureHash)) {
						paymentStatus = QQConstant.PAYMENT_STATUS_SUCCESS;
						quotationStatus = QQConstant.QUOTATION_STATUS_COMPLETE;
						page = pageSuccess;
					} else {
						paymentStatus = QQConstant.PAYMENT_STATUS_FAILED;
						page = pageFailed;
					}
					params.put("RESPONSE", result);
					params.put("SECUREHASH", secureHash);
					params.put("AUTHCODE", authCode);
					params.put("INVNO", invoiceNo);
					params.put("CARD4DIGIT", card4Digit);
					params.put("AMT", amount);
					params.put("CARDTYPE", cardType);
					params.put("TXNID", transactionId);
					paymentResponse = new PaymentResponse();
					paymentResponse.setAmount(amount);
					paymentResponse.setPaymentStatus(responseCode);
					paymentResponse.setPolicyNumber(invoiceNo);
					paymentResponse.setReceiptNumber(transactionId);
					paymentResponse.setTransactionDateTime(strDateTime);
					session.setAttribute(QQConstant.PAYMENT_RESPONSE_SESSION_KEY, paymentResponse);
					batchInsertPaymentRecord(invoiceNo, params, QQConstant.RESPONSE_PAYMENT_INSERT_QUERY,
							QQConstant.TRANSACTION_TYPE_REQUEST);

					Integer updateRespond = updatePaymentAndQuotation(paymentStatus, quotationStatus, invoiceNo, paymentResponse);
					if (updateRespond == 1) {
						session.setAttribute(QQConstant.BANK_LIST_SESSION_KEY, dspCommonTblBankListService.getBanks());
						if (responseCode.equals(QQConstant.PAYMENT_RESPOND_CODE_SUCCESS)
								&& secureHashKeyGenerated.equals(secureHash)) {
							generateDocumentAndEmail(request, response, paymentResponse);
						} 
					}else{
						page = pageFailed;
					}
				} else {
					responseCode = paymentResponse.getPaymentStatus();
					if (responseCode.equals("0")) {
						page = pageSuccess;
					} else {
						page = pageFailed;
					}
				}
			}

		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}

		response.sendRedirect(page);
	}

	private String generateRequestSecureHashKey(String mid, String policyno, String amt, String company) {
		return MpayUtils.requestSecureHashValue(mid, policyno, amt, company);
	}

	private String generateResponseSecureHashKey(String result, String company) {
		return MpayUtils.responseSecureHashValue(result, company);
	}

	private Integer updatePaymentAndQuotation(String paymentStatus, String quotationStatus, String policyNo,
			PaymentResponse paymentResponse) {
		BigDecimal dspQqId = null;
		Integer updateRespond = 0;
		try {
			paymentStatus = StringUtils.trimToNull(paymentStatus);
			quotationStatus = StringUtils.trimToNull(quotationStatus);
			policyNo = StringUtils.trimToNull(policyNo);

			if (policyNo != null && paymentStatus != null) {
				policyNo = StringUtils.chop(policyNo);
				DspCommonTblPolicy record = new DspCommonTblPolicy();
				DspCommonTblPolicyExample example = new DspCommonTblPolicyExample();
				example.createCriteria().andPolicyNumberEqualTo(policyNo);
				List<DspCommonTblPolicy> policies = policyService.getPolicyDetails(record, example);

				if (policies.isEmpty() == false) {
					dspQqId = policies.get(0).getDspQqId();

					DspCommonTblPayment paymentRecord = new DspCommonTblPayment();
					DspCommonTblPaymentExample paymentExample = new DspCommonTblPaymentExample();

					paymentRecord.setPmntStatus(paymentStatus);
					paymentExample.createCriteria().andDspQqIdEqualTo(dspQqId);

					Integer updatePaymentTable = paymentService.updatePayment(paymentRecord, paymentExample);
					Integer updateMpQqTable = 0;
					if (quotationStatus != null) {
						DspMpTblQQ mpQqRecord = new DspMpTblQQ();
						DspMpTblQQExample mpQqExample = new DspMpTblQQExample();
						mpQqRecord.setMpQqStatus(QQConstant.QUOTATION_STATUS_COMPLETE);
						mpQqExample.createCriteria().andDspQqIdEqualTo(dspQqId);
						paymentResponse.setDspQqId(dspQqId);
						Future<Integer> future = mpQqService.updateDspMpTblQQAsync(mpQqRecord, mpQqExample);
						updateMpQqTable = future.get();
					}
					
					updateRespond = (updatePaymentTable + updateMpQqTable) / 2;
				}
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return updateRespond;
	}

	private void batchInsertPaymentRecord(String transactionId, Map<String, String> params, String query,
			String transactionType) throws SQLException {

		Connection connection = null;

		try {
			connection = ConnectionFactory.getConnection();

			CallableStatement pmntGtwayRequestCs = connection.prepareCall(query);

			for (Map.Entry<String, String> item : params.entrySet()) {
				pmntGtwayRequestCs.setString(1, transactionId);
				pmntGtwayRequestCs.setString(2, transactionType);
				pmntGtwayRequestCs.setString(3, item.getKey());
				pmntGtwayRequestCs.setString(4, item.getValue());
				pmntGtwayRequestCs.setString(5, QQConstant.PAYMENT_GATEWAY_TRANSACTION_CODE);
				pmntGtwayRequestCs.addBatch();
			}

			pmntGtwayRequestCs.executeBatch();

			if (!connection.isClosed()) {
				connection.close();
			}

		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		} finally {
			if (null != connection && !connection.isClosed()) {
				connection.close();
			}
		}
	}

	public static void setDetails(DocCustomPojo doc, DspMpTblQQ mpQQData, DspCommonTblCustomer dspCommonTblCustomer,
			String policyNumber) {
		try {

			if (mpQQData != null && dspCommonTblCustomer != null) {
				doc.setLanguage("E");
				doc.setSumCovered(mpQQData.getMpSumInsured());
				doc.setPayableAmt(mpQQData.getPremiumPaymentFrequency());
				String gender = "";
				if ("F".equalsIgnoreCase(mpQQData.getGender())) {
					gender = "Male";
				} else if ("M".equalsIgnoreCase(mpQQData.getGender())) {
					gender = "Female";
				}
				doc.setGender(gender);
				doc.setIc(mpQQData.getNewIcCopy());
				doc.setPolicyNo(policyNumber);
				doc.setPaymentFrequency(mpQQData.getPremiumPaymentFrequencyType());
				doc.setDurationOfBenefit(mpQQData.getDurationOfBenifit());
				doc.setDspQQId(String.valueOf(mpQQData.getDspQqId()));
				doc.setIsSmoker(mpQQData.getIsSmoker());
				doc.setDob(mpQQData.getDob());
				doc.setName(dspCommonTblCustomer.getCustomerName());
				doc.setQuotationDate(new SimpleDateFormat("dd-MMM-yyyy").format(new Date()));
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
	}

	private void generateDocumentAndEmail(HttpServletRequest request, HttpServletResponse response,
			PaymentResponse paymentResponse) {
		DspMpTblQQ mpQQData = null;
		DspCommonTblCustomer dspCommonTblCustomer = null;
		BigDecimal qqId = null;
		String filename = null;
		String path = null;
		String qqIdKey = null;
		VpmsRequest vpmsRequest = null;
		VpmsResponseData vpmsResponseData = null;
		List<GeneratingReportsResponseVo> reportVos = new ArrayList<GeneratingReportsResponseVo>();
		List<DspTlTblOcc> occupations = null;
		try {
			logger.error("Start generateDocumentAndEmail");
			HttpSession session = request.getSession();
			mpQQData = (DspMpTblQQ) session.getAttribute(QQConstant.MP_QQ_DATA_SESSION_KEY);
			qqId = (BigDecimal) session.getAttribute(QQConstant.DSP_QQ_ID_DATA_SESSION_KEY);
			dspCommonTblCustomer = (DspCommonTblCustomer) session
					.getAttribute(QQConstant.COMMON_CUSTOMER_DATA_SESSION_KEY);
			String company = (String) session.getAttribute(QQConstant.COMPANY_SESSION_KEY);
			vpmsRequest = (VpmsRequest) session.getAttribute(QQConstant.VPMS_REQUEST_SESSION_KEY);
			vpmsResponseData = (VpmsResponseData) session.getAttribute(QQConstant.VPMS_RESPONSE_SESSION_KEY);
			occupations = CommonUtils.castList(session.getAttribute(QQConstant.OCCUPATION_SESSION_KEY),
					DspTlTblOcc.class);

			GeneratingReportsResponseVo reportVo = null;
			if (paymentResponse != null && mpQQData != null && dspCommonTblCustomer != null
					&& QQConstant.PAYMENT_GATEWAY_SUCCESS_CODE.equals(paymentResponse.getPaymentStatus())
					&& company != null) {
				qqIdKey = String.valueOf(qqId);
				String policyNumber = paymentResponse.getPolicyNumber();
				DocGenController docGenController = new DocGenController();
				DocCustomPojo doc = new DocCustomPojo();
				setDetails(doc, mpQQData, dspCommonTblCustomer, policyNumber);

				logger.error("policyNumber: {}", policyNumber);
				logger.error("qqIdKey: {}", qqIdKey);

				if (company.equals(QQConstant.INSURANCE_COMPANY_KEY)) {
					filename = policyNumber + "-e-SalesIllustration.pdf";
				}else{
					filename = policyNumber + "-e-MarketingIllustration.pdf";
				}
				path = docGenController.salesIllustrationFormMethod(doc, vpmsRequest, vpmsResponseData, company, dspCommonTblCustomer, occupations, filename);
				logger.error("path SalesIllustration: {}", path);
				if (path != null && path.isEmpty() == false) {
					reportVo = new GeneratingReportsResponseVo();
					reportVo.setFileName(filename);
					reportVo.setDocCode("SIFORM");
					reportVo.setPolicyNo(policyNumber);
					reportVos.add(reportVo);
				}

				filename = policyNumber + "-e-ProductDisclosureSheet.pdf";
				path = docGenController.ePDSAfterDoc(doc, company);
				logger.error("path ePDSAfterDoc: {}", path);
				if (path != null && path.isEmpty() == false) {
					reportVo = new GeneratingReportsResponseVo();
					reportVo.setFileName(filename);
					reportVo.setDocCode("PDSFORM");
					reportVo.setPolicyNo(policyNumber);
					reportVos.add(reportVo);
				}

				filename = qqIdKey + "-EAppForm.pdf";
				path = docGenController.eApplicationFormMethod(filename, company, mpQQData, dspCommonTblCustomer,
						vpmsRequest, occupations, paymentResponse);
				logger.error("filename: {}", filename);
				logger.error("path: {}", path);
				if (path != null && path.isEmpty() == false) {
					reportVo = new GeneratingReportsResponseVo();
					reportVo.setDocCode("APPFORM");
					reportVo.setFileName(filename);
					reportVo.setPolicyNo(policyNumber);
					reportVos.add(reportVo);
				}

				if (company.equals(QQConstant.INSURANCE_COMPANY_KEY)) {
					filename = policyNumber + "-e-PolicyInformationPage.pdf";
				}else{
					filename = policyNumber + "-e-TakafulSchedule.pdf";
				}
				path = docGenController.ePolicyInfoPageMethod(mpQQData, dspCommonTblCustomer, policyNumber, company,
						paymentResponse, filename, vpmsResponseData);
				logger.error("filename: {}", filename);
				logger.error("path: {}", path);
				if (path != null && path.isEmpty() == false) {
					reportVo = new GeneratingReportsResponseVo();
					reportVo.setDocCode("INFOFORM");
					reportVo.setFileName(filename);
					reportVo.setPolicyNo(policyNumber);
					reportVos.add(reportVo);
				}

				filename = policyNumber + "-e-Receipt.pdf";
				path = docGenController.eReceiptMethod(mpQQData, dspCommonTblCustomer, policyNumber, company);
				logger.error("filename: {}", filename);
				logger.error("path: {}", path);
				if (path != null && path.isEmpty() == false) {
					reportVo = new GeneratingReportsResponseVo();
					reportVo.setDocCode("RCPTFORM");
					reportVo.setFileName(filename);
					reportVo.setPolicyNo(policyNumber);
					reportVos.add(reportVo);
				}

				if (company.equals(QQConstant.INSURANCE_COMPANY_KEY)) {
					filename = policyNumber + "-Contract.pdf";
				}else{
					filename = policyNumber + "-Certificate.pdf";
				}
				path = docGenController.getPaymentContractPdfFile(request, response, company, filename);
				logger.error("filename: {}", filename);
				logger.error("path: {}", path);
				if (path != null && path.isEmpty() == false) {
					reportVo = new GeneratingReportsResponseVo();
					reportVo.setDocCode("CONTRACTFORM");
					reportVo.setFileName(filename);
					reportVo.setPolicyNo(policyNumber);
					reportVos.add(reportVo);
				}

				filename = policyNumber + "-Nomination.pdf";
				path = docGenController.getPaymentNominationPdfFile(request, response, company);
				logger.error("filename: {}", filename);
				logger.error("path: {}", path);
				if (path != null && path.isEmpty() == false) {
					reportVo = new GeneratingReportsResponseVo();
					reportVo.setDocCode("NOMINATIONFORM");
					reportVo.setFileName(filename);
					reportVo.setPolicyNo(policyNumber);
					reportVos.add(reportVo);
				}

				filename = policyNumber + "-Claims-Guide.pdf";
				path = docGenController.getPaymentClaimGuide(request, response);
				logger.error("filename: {}", filename);
				logger.error("path: {}", path);
				if (path != null && path.isEmpty() == false) {
					reportVo = new GeneratingReportsResponseVo();
					reportVo.setDocCode("CLAIMFORM");
					reportVo.setFileName(filename);
					reportVo.setPolicyNo(policyNumber);
					reportVos.add(reportVo);
				}

				CiZipFileGeneration ciZipFileGen = new CiZipFileGeneration();
				ciZipFileGen.zipFilesGen(mpQQData, policyNumber, dspCommonTblCustomer, reportVos);

				DspEmailDispatchProcessor emailProcessor = new DspEmailDispatchProcessor(
						new ProductMailTemplateServiceImpl());
				String status = emailProcessor.emailDispatchProcess(mpQQData, paymentResponse, dspCommonTblCustomer,
						reportVos, company);
				logger.error("Email status: {}", status);

			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}

	}
}
