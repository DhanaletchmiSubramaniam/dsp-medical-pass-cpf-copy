package com.etiqa.medicalpass.controller.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.etiqa.medicalpass.pojo.VpmsRequest;
import com.etiqa.medicalpass.pojo.VpmsResponse;
import com.etiqa.medicalpass.pojo.VpmsResponseTakaful;
import com.etiqa.medicalpass.service.VpmsService;

@RestController
@RequestMapping(value = "/api/v1/vpms")
public class VpmsController {

	private static final Logger logger = LoggerFactory.getLogger(VpmsController.class);

	@Autowired
	private VpmsService vpmsService;

	@RequestMapping(value = "/takaful", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<VpmsResponseTakaful> getVpmsTakaful(@RequestBody VpmsRequest vpmsRequest) {

		VpmsResponseTakaful vpmsResponseTakaful = null;
		try {
			logger.info("vpmsRequest: {}", vpmsRequest);
			if (vpmsRequest != null) {
				vpmsResponseTakaful = vpmsService.getVpmsTakaful(vpmsRequest);
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}

		return new ResponseEntity<VpmsResponseTakaful>(vpmsResponseTakaful, HttpStatus.OK);
	}

	@RequestMapping(value = "/insurance", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<VpmsResponse> getVpmsInsurance(@RequestBody VpmsRequest vpmsRequest) {

		VpmsResponse vpmsResponse = null;
		try {
			logger.info("vpmsRequest: {}", vpmsRequest);
			if (vpmsRequest != null) {
				vpmsResponse = vpmsService.getVpmsInsurance(vpmsRequest);
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}

		return new ResponseEntity<VpmsResponse>(vpmsResponse, HttpStatus.OK);
	}
}
