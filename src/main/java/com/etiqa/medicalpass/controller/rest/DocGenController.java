
package com.etiqa.medicalpass.controller.rest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.etiqa.medicalpass.constant.DocumentConstant;
import com.etiqa.medicalpass.constant.QQConstant;
import com.etiqa.medicalpass.pojo.DocCustomPojo;
import com.etiqa.medicalpass.pojo.DspCommonTblCustomer;
import com.etiqa.medicalpass.pojo.DspMpTblQQ;
import com.etiqa.medicalpass.pojo.DspTlTblOcc;
import com.etiqa.medicalpass.pojo.PaymentResponse;
import com.etiqa.medicalpass.pojo.SalesIllustration;
import com.etiqa.medicalpass.pojo.VpmsRequest;
import com.etiqa.medicalpass.pojo.VpmsResponseData;
import com.etiqa.medicalpass.util.CommonUtils;
import com.etiqa.medicalpass.util.InputOutputStreamUtil;
import com.etiqa.medicalpass.util.PasswordUtils;
import com.lowagie.text.pdf.PdfWriter;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.ExporterInput;
import net.sf.jasperreports.export.OutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;

@RestController
@RequestMapping(value = "/pdf")
public class DocGenController {

	private static final Logger logger = LoggerFactory.getLogger(DocGenController.class);

	@RequestMapping(value = "/common", method = RequestMethod.GET, produces = "application/pdf")
	public @ResponseBody void getPdfByName(@RequestParam String name, HttpServletRequest request,
			HttpServletResponse response) {
		DspMpTblQQ mpQQData = null;
		FileInputStream fileInputStream = null;
		InputStream inputStreamPath = null;
		try {
			HttpSession session = request.getSession();
			mpQQData = (DspMpTblQQ) session.getAttribute(QQConstant.MP_QQ_DATA_SESSION_KEY);
			BigDecimal qqIdKey = (BigDecimal) session.getAttribute(QQConstant.DSP_QQ_ID_DATA_SESSION_KEY);
			PaymentResponse paymentResponse = (PaymentResponse) session
					.getAttribute(QQConstant.PAYMENT_RESPONSE_SESSION_KEY);
			String company = (String) session.getAttribute(QQConstant.COMPANY_SESSION_KEY);

			if (mpQQData != null && qqIdKey != null && paymentResponse != null
					&& QQConstant.PAYMENT_GATEWAY_SUCCESS_CODE.equals(paymentResponse.getPaymentStatus())
					&& company != null) {
				String filename = null;
				String pathFile = null;
				String policyNumber = paymentResponse.getPolicyNumber();
				switch (name) {
				case DocumentConstant.DOCUMENT_NAME_APPLICATION_FORM:
					filename = qqIdKey + "-EAppForm.pdf";
					break;
				case DocumentConstant.DOCUMENT_NAME_POLICY_INFROMATION_PAGE:
					if (company.equals(QQConstant.INSURANCE_COMPANY_KEY)) {
						filename = policyNumber + "-e-PolicyInformationPage.pdf";
					}else{
						filename = policyNumber + "-e-TakafulSchedule.pdf";
					}
					break;
				case DocumentConstant.DOCUMENT_NAME_CLAIMS_GUIDE:
					filename = policyNumber + "-Claims-Guide.pdf";
					break;
				case DocumentConstant.DOCUMENT_NAME_RECEIPT_FROM:
					filename = policyNumber + "-e-Receipt.pdf";
					break;
				case DocumentConstant.DOCUMENT_NAME_CONTRACT:
					if (company.equals(QQConstant.INSURANCE_COMPANY_KEY)) {
						filename = policyNumber + "-Contract.pdf";
					}else{
						filename = policyNumber + "-Certificate.pdf";
					}
					break;
				case DocumentConstant.DOCUMENT_NAME_NOMINATION:
					filename = policyNumber + "-Nomination.pdf";
					break;
				case DocumentConstant.DOCUMENT_NAME_PDS:
					filename = policyNumber + "-e-ProductDisclosureSheet.pdf";
					break;
				case DocumentConstant.DOCUMENT_NAME_SALES_ILLUSTRATION:
					if (company.equals(QQConstant.INSURANCE_COMPANY_KEY)) {
						filename = policyNumber + "-e-SalesIllustration.pdf";
					}else{
						filename = policyNumber + "-e-MarketingIllustration.pdf";
					}
					break;
				default:
					break;
				}
				inputStreamPath = this.getClass().getClassLoader().getResourceAsStream(QQConstant.PROPERTIES_FILE_NAME);
				Properties prop = new Properties();
				prop.load(inputStreamPath);
				pathFile = prop.getProperty(DocumentConstant.DOCUMENT_DESTINATION_PATH_KEY) + filename;
				File file = new File(pathFile);
				if (file.exists() == false) {
					logger.error(DocumentConstant.ERROR_MESSAGE_DOCUMENT_NOT_FOUND, filename);
				} else {
					fileInputStream = new FileInputStream(file);
					response.setContentType("application/pdf");
					response.setHeader("Content-Disposition", "inline; filename=" + filename);
					response.setHeader("Content-Length", String.valueOf(file.length()));
					FileCopyUtils.copy(fileInputStream, response.getOutputStream());
				}
			} else {
				throw new NullPointerException(QQConstant.ERROR_MESSAGE_SESSION_EXPIRED);
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		} finally {
			InputOutputStreamUtil.close(inputStreamPath);
			try {
				if (fileInputStream != null) {
					fileInputStream.close();
				}
			} catch (IOException ex) {
				logger.error(ex.getMessage(), ex);

			}
		}

	}

	@RequestMapping(value = "/zip", method = RequestMethod.GET, produces = "application/zip")
	public @ResponseBody void getZipFile(HttpServletRequest request, HttpServletResponse response) {
		DspMpTblQQ mpQQData = null;
		FileInputStream fileInputStream = null;
		InputStream inputStreamPath = null;
		try {
			HttpSession session = request.getSession();
			mpQQData = (DspMpTblQQ) session.getAttribute(QQConstant.MP_QQ_DATA_SESSION_KEY);
			BigDecimal qqIdKey = (BigDecimal) session.getAttribute(QQConstant.DSP_QQ_ID_DATA_SESSION_KEY);
			PaymentResponse paymentResponse = (PaymentResponse) session
					.getAttribute(QQConstant.PAYMENT_RESPONSE_SESSION_KEY);

			String pathFile = null;
			String filename = null;
			if (mpQQData != null && qqIdKey != null && paymentResponse != null
					&& QQConstant.PAYMENT_GATEWAY_SUCCESS_CODE.equals(paymentResponse.getPaymentStatus())) {
				filename = paymentResponse.getPolicyNumber() + ".zip";
				inputStreamPath = this.getClass().getClassLoader().getResourceAsStream(QQConstant.PROPERTIES_FILE_NAME);
				Properties prop = new Properties();
				prop.load(inputStreamPath);
				pathFile = prop.getProperty(DocumentConstant.DOCUMENT_DESTINATION_PATH_KEY) + filename;
				File file = new File(pathFile);
				if (file.exists() == false) {
					logger.error(DocumentConstant.ERROR_MESSAGE_DOCUMENT_NOT_FOUND, filename);
				} else {
					fileInputStream = new FileInputStream(file);
					response.setContentType("application/zip");
					response.setHeader("Content-Disposition", "inline; filename=" + filename);
					response.setHeader("Content-Length", String.valueOf(file.length()));
					FileCopyUtils.copy(fileInputStream, response.getOutputStream());
				}
			} else {
				throw new NullPointerException(QQConstant.ERROR_MESSAGE_SESSION_EXPIRED);
			}

		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		} finally {
			InputOutputStreamUtil.close(inputStreamPath);
			try {
				if (fileInputStream != null) {
					fileInputStream.close();
				}
			} catch (IOException ex) {
				logger.error(ex.getMessage(), ex);

			}
		}

	}

	@RequestMapping(value = "/eDraftApplicationPdfFile", method = RequestMethod.GET, produces = "application/pdf")
	public @ResponseBody void getEDraftApplication(HttpServletRequest request, HttpServletResponse response) {
		DspMpTblQQ mpQQData = null;
		FileInputStream fileInputStream = null;
		try {
			HttpSession session = request.getSession();
			mpQQData = (DspMpTblQQ) session.getAttribute(QQConstant.MP_QQ_DATA_SESSION_KEY);
			BigDecimal qqIdKey = (BigDecimal) session.getAttribute(QQConstant.DSP_QQ_ID_DATA_SESSION_KEY);
			DspCommonTblCustomer dspCommonTblCustomer = (DspCommonTblCustomer) session
					.getAttribute(QQConstant.COMMON_CUSTOMER_DATA_SESSION_KEY);
			String company = (String) session.getAttribute(QQConstant.COMPANY_SESSION_KEY);
			VpmsRequest vpmsRequest = (VpmsRequest) session.getAttribute(QQConstant.VPMS_REQUEST_SESSION_KEY);
			List<DspTlTblOcc> occupations = CommonUtils
					.castList(session.getAttribute(QQConstant.OCCUPATION_SESSION_KEY), DspTlTblOcc.class);

			if (mpQQData != null && qqIdKey != null) {
				String fileName = qqIdKey + "-EDraftAppForm.pdf";
				String pathFile = eDraftApplicationFormMethod(fileName, company, mpQQData, dspCommonTblCustomer,
						vpmsRequest, occupations);
				File file = new File(pathFile);
				if (file.exists() == false) {
					logger.error(DocumentConstant.ERROR_MESSAGE_DOCUMENT_NOT_FOUND, fileName);
				} else {
					fileInputStream = new FileInputStream(file);
					response.setContentType("application/pdf");
					response.setHeader("Content-Disposition", "inline; filename=EDraftAppForm.pdf");
					response.setHeader("Content-Length", String.valueOf(file.length()));
					FileCopyUtils.copy(fileInputStream, response.getOutputStream());
				}
			} else {
				throw new NullPointerException(QQConstant.ERROR_MESSAGE_SESSION_EXPIRED);
			}

		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		} finally {
			try {
				if (fileInputStream != null) {
					fileInputStream.close();
				}
			} catch (IOException ex) {
				logger.error(ex.getMessage(), ex);

			}
		}

	}

	@RequestMapping(value = "/paymentClaimGuideBeforePayment", method = RequestMethod.GET, produces = "application/pdf")
	public @ResponseBody void getPaymentClaimGuideBeforPayment(HttpServletRequest request,
			HttpServletResponse response) {
		FileInputStream fileInputStream = null;
		try {
			HttpSession session = request.getSession();
			BigDecimal qqIdKey = (BigDecimal) session.getAttribute(QQConstant.DSP_QQ_ID_DATA_SESSION_KEY);
			if (qqIdKey != null) {
				String filename = qqIdKey + "-ClaimsGuide.pdf";
				String pathFile = getPaymentClaimGuideBeforePayment(filename);
				File file = new File(pathFile);
				if (file.exists() == false) {
					logger.error(DocumentConstant.ERROR_MESSAGE_DOCUMENT_NOT_FOUND, filename);
				} else {
					fileInputStream = new FileInputStream(file);
					response.setContentType("application/pdf");
					response.setHeader("Content-Disposition", "inline; filename=ClaimsGuide.pdf");
					response.setHeader("Content-Length", String.valueOf(file.length()));
					FileCopyUtils.copy(fileInputStream, response.getOutputStream());
				}
			} else {
				throw new NullPointerException(QQConstant.ERROR_MESSAGE_SESSION_EXPIRED);
			}

		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		} finally {
			try {
				if (fileInputStream != null) {
					fileInputStream.close();
				}
			} catch (IOException ex) {
				logger.error(ex.getMessage(), ex);

			}
		}

	}

	public String getPaymentClaimGuideBeforePayment(String filename) {
		logger.info("DocGenController - getPaymentClaimGuideBeforEPayment()");
		FileOutputStream fileInputStream = null;
		try {
			InputStream inputStreamPath = null;
			inputStreamPath = this.getClass().getClassLoader().getResourceAsStream(QQConstant.PROPERTIES_FILE_NAME);
			Properties prop = new Properties();
			prop.load(inputStreamPath);
			String pdfFile = prop.getProperty("ClaimsGuideFile");
			logger.info("---------------------- Start generating Claim Guide PDF --------------------");
			InputStream input = this.getClass().getClassLoader().getResourceAsStream(pdfFile);
			fileInputStream = new FileOutputStream(
					prop.getProperty(DocumentConstant.DOCUMENT_DESTINATION_PATH_KEY) + filename, true);
			int b = 0;
			while ((b = input.read()) != -1) {
				fileInputStream.write(b);
			}
			fileInputStream.close();
			logger.info("---------------------- Claim guide PDF generation done! --------------------");
			String filePath = prop.getProperty(DocumentConstant.DOCUMENT_DESTINATION_PATH_KEY) + filename;
			File file = new File(filePath);
			filename = filePath;
			if (!file.exists()) {
				logger.error("The file does not existed...");
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			return "";
		} finally {
			try {
				if (fileInputStream != null) {
					fileInputStream.close();
				}
			} catch (IOException ex) {
				logger.error(ex.getMessage(), ex);
			}
		}
		return filename;
	}

	@RequestMapping(value = "/deductibleContent", method = RequestMethod.GET, produces = "application/pdf")
	public @ResponseBody void getDeductibleContent(HttpServletRequest request, HttpServletResponse response) {
		FileInputStream fileInputStream = null;
		try {
			String filename = "DeductibleContent.pdf";
			String pathFile = getDeductibleContent(filename);
			File file = new File(pathFile);
			if (file.exists() == false) {
				logger.error(DocumentConstant.ERROR_MESSAGE_DOCUMENT_NOT_FOUND, filename);
			} else {
				fileInputStream = new FileInputStream(file);
				response.setContentType("application/pdf");
				response.setHeader("Content-Disposition", "inline; filename=DeductibleContent.pdf");
				response.setHeader("Content-Length", String.valueOf(file.length()));
				FileCopyUtils.copy(fileInputStream, response.getOutputStream());
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		} finally {
			try {
				if (fileInputStream != null) {
					fileInputStream.close();
				}
			} catch (IOException ex) {
				logger.error(ex.getMessage(), ex);

			}
		}

	}

	public String getDeductibleContent(String filename) {
		logger.info("DocGenController - getDeductibleContent()");
		FileOutputStream fileInputStream = null;
		try {
			InputStream inputStreamPath = null;
			inputStreamPath = this.getClass().getClassLoader().getResourceAsStream(QQConstant.PROPERTIES_FILE_NAME);
			Properties prop = new Properties();
			prop.load(inputStreamPath);
			String pdfFile = prop.getProperty("DeductibleContent");
			logger.info("---------------------- Start generating DeductibleContent PDF --------------------");
			InputStream input = this.getClass().getClassLoader().getResourceAsStream(pdfFile);
			fileInputStream = new FileOutputStream(
					prop.getProperty(DocumentConstant.DOCUMENT_DESTINATION_PATH_KEY) + filename, true);
			int b = 0;
			while ((b = input.read()) != -1) {
				fileInputStream.write(b);
			}
			fileInputStream.close();
			logger.info("---------------------- DeductibleContent PDF generation done! --------------------");
			String filePath = prop.getProperty(DocumentConstant.DOCUMENT_DESTINATION_PATH_KEY) + filename;
			File file = new File(filePath);
			filename = filePath;
			if (!file.exists()) {
				logger.error("The file does not existed...");
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			return "";
		} finally {
			try {
				if (fileInputStream != null) {
					fileInputStream.close();
				}
			} catch (IOException ex) {
				logger.error(ex.getMessage(), ex);
			}
		}
		return filename;
	}

	public String getPaymentClaimGuide(HttpServletRequest request, HttpServletResponse response) {
		logger.info("DocGenController - getPaymentClaimGuide()");
		FileOutputStream fileInputStream = null;
		String filename = null;
		try {
			HttpSession session = request.getSession();
			PaymentResponse paymentResponse = (PaymentResponse) session
					.getAttribute(QQConstant.PAYMENT_RESPONSE_SESSION_KEY);

			if (paymentResponse != null
					&& QQConstant.PAYMENT_GATEWAY_SUCCESS_CODE.equals(paymentResponse.getPaymentStatus())) {
				String policyNumber = paymentResponse.getPolicyNumber();
				filename = policyNumber + "-Claims-Guide.pdf";
				InputStream inputStreamPath = null;
				inputStreamPath = this.getClass().getClassLoader().getResourceAsStream(QQConstant.PROPERTIES_FILE_NAME);
				Properties prop = new Properties();
				prop.load(inputStreamPath);
				String pdfFile = prop.getProperty("ClaimsGuideFile");
				logger.info("---------------------- Start generating Claim Guide PDF --------------------");
				InputStream input = this.getClass().getClassLoader().getResourceAsStream(pdfFile);
				fileInputStream = new FileOutputStream(
						prop.getProperty(DocumentConstant.DOCUMENT_DESTINATION_PATH_KEY) + filename, true);
				int b = 0;
				while ((b = input.read()) != -1) {
					fileInputStream.write(b);
				}
				fileInputStream.close();
				logger.info("---------------------- Claim guide PDF generation done! --------------------");
				String filePath = prop.getProperty(DocumentConstant.DOCUMENT_DESTINATION_PATH_KEY) + filename;
				File file = new File(filePath);
				if (!file.exists()) {
					logger.error("The file does not existed...");
				}
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			return "";
		} finally {
			try {
				if (fileInputStream != null) {
					fileInputStream.close();
				}
			} catch (IOException ex) {
				logger.error(ex.getMessage(), ex);
			}
		}
		return filename;
	}

	public String ePolicyInfoPageMethod(DspMpTblQQ mpQQData, DspCommonTblCustomer dspCommonTblCustomer,
			String policyNum, String company, PaymentResponse paymentResponse, String filename,
			VpmsResponseData vpmsResponseData) {
		logger.info("DocGenController - ePolicyInfoPageMethod()");
		Map<String, Object> parameters = new HashMap<String, Object>();

		Integer age = CommonUtils.getAge(mpQQData.getDob(), QQConstant.DOB_FORM_DATE_FORMAT);
		String todayDate = CommonUtils.formatSiDate();

		parameters.put("P_PLName",
				dspCommonTblCustomer.getCustomerName() == null ? "" : dspCommonTblCustomer.getCustomerName());
		parameters.put("P_PLICNo",
				dspCommonTblCustomer.getCustomerNricId() == null ? "" : dspCommonTblCustomer.getCustomerNricId());
		parameters.put("P_PLAge", String.valueOf(age));

		String gender = (dspCommonTblCustomer.getCustomerGender() == null ? ""
				: dspCommonTblCustomer.getCustomerGender());
		if (StringUtils.equals(QQConstant.FEMALE_CODE, gender)) {
			parameters.put("P_PLGender", QQConstant.FEMALE);
			parameters.put("P_LIGender", QQConstant.FEMALE);
		} else {
			parameters.put("P_PLGender", QQConstant.MALE);
			parameters.put("P_LIGender", QQConstant.MALE);
		}

		parameters.put("P_LIName",
				dspCommonTblCustomer.getCustomerName() == null ? "" : dspCommonTblCustomer.getCustomerName());
		parameters.put("P_LIICNo",
				dspCommonTblCustomer.getCustomerNricId() == null ? "" : dspCommonTblCustomer.getCustomerNricId());
		parameters.put("P_LIAge", String.valueOf(age));

		String dob = CommonUtils.formatDobToSiDate(dspCommonTblCustomer.getCustomerDob(),
				QQConstant.DOB_FORM_DATE_FORMAT);
		parameters.put("P_PLDob", dob);
		parameters.put("P_LIDob", dob);

		String formCodePostFix = CommonUtils.getFormCodePostFixFromProductCode(mpQQData.getProductCode());
		if (company.equals(QQConstant.INSURANCE_COMPANY_KEY)) {
			parameters.put("P_BasicPolicy", "e-Medical Pass");
			parameters.put("P_TypesOfCoverage", "e-Medical Pass");
			parameters.put("P_FormNo", QQConstant.FORM_NO_CODE_INSURANCE + formCodePostFix);
			parameters.put("P_LifeInsured1", "L/I");
			parameters.put("P_LifeInsured2", "L/I");
		} else {
			parameters.put("P_BasicPolicy", "e-Medical Pass Takaful");
			parameters.put("P_TypesOfCoverage", "e-Medical Pass Takaful");
			parameters.put("P_FormNo", QQConstant.FORM_NO_CODE_TAKAFUL + formCodePostFix);
			parameters.put("P_LifeInsured1", "P/C");
			parameters.put("P_LifeInsured2", "P/C");
		}

		parameters.put("P_PolicyNo", policyNum);
		parameters.put("P_IssueDate", todayDate);
		parameters.put("P_CommDate", todayDate);

		parameters.put("P_CoveredPeriod",
				mpQQData.getDurationOfBenifit() == null ? "0" : mpQQData.getDurationOfBenifit() + " " + " Years");

		parameters.put("P_PremiumMode", StringUtils.capitalize(mpQQData.getPremiumPaymentFrequencyType()));
		parameters.put("P_PremiumAmnt",
				mpQQData.getPremiumPaymentFrequency() == null ? "" : mpQQData.getPremiumPaymentFrequency());

		Double sumInsuredDoubleVal = null;
		if (mpQQData.getMpSumInsured() != null) {
			logger.info("Sum Insured (RM): {}", mpQQData.getMpSumInsured());
			sumInsuredDoubleVal = Double.parseDouble(mpQQData.getMpSumInsured());

			String formatted = CommonUtils.formatPremium(sumInsuredDoubleVal.toString());
			parameters.put("P_SumIsured", formatted);
		} else {
			parameters.put("P_SumIsured", "");
		}

		parameters.put("P_Participating", "Non-Participating");
		parameters.put("P_PremiumRM1",
				mpQQData.getPremiumPaymentFrequency() == null ? "" : mpQQData.getPremiumPaymentFrequency());
		parameters.put("P_PremiumRM2",
				mpQQData.getPremiumPaymentFrequency() == null ? "" : mpQQData.getPremiumPaymentFrequency());

		String expDate = null;
		DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MMM-yyyy");
		DateTime expDateJoda = formatter.parseDateTime(mpQQData.getQuotationCreationDatetime());
		expDateJoda = expDateJoda.plusYears(NumberUtils.toInt(mpQQData.getDurationOfBenifit()));
		expDateJoda = expDateJoda.minusDays(1);
		expDate = formatter.print(expDateJoda);

		DateTime lastPaymentDateJoda = null;
		if ((mpQQData.getPremiumPaymentFrequencyType()).equalsIgnoreCase(QQConstant.PAYMENT_TYPE_MONTHLY)) {
			lastPaymentDateJoda = expDateJoda.minusMonths(1);
		} else {
			lastPaymentDateJoda = expDateJoda.minusYears(1);
		}
		parameters.put("P_PaymentDate", formatter.print(lastPaymentDateJoda));


		parameters.put("P_ExpDate1", expDate);
		parameters.put("P_ExpDate2", expDate);
		parameters.put("P_NomineeName1", "");
		parameters.put("P_NomineeName2", "");
		parameters.put("P_NomineeName3", "");
		parameters.put("P_NomineeICNo1", "");
		parameters.put("P_NomineeICNo2", "");
		parameters.put("P_NomineeICNo3", "");
		parameters.put("P_NomineeRelation1", "");
		parameters.put("P_NomineeRelation2", "");
		parameters.put("P_NomineeRelation3", "");
		parameters.put("P_NomineeSharePer1", "");
		parameters.put("P_NomineeSharePer2", "");
		parameters.put("P_NomineeSharePer3", "");
		parameters.put("P_TrusteeName1", "");
		parameters.put("P_TrusteeName2", "");
		parameters.put("P_TrusteeICNo1", "");
		parameters.put("P_TrusteeICNo2", "");
		parameters.put("P_TrusteeRelation1", "");
		parameters.put("P_TrusteeRelation2", "");
		parameters.put("P_TrusteeSharePer1", "");
		parameters.put("P_TrusteeSharePer2", "");

		parameters.put("P_PlanType", CommonUtils.getPlanNameFromProductCode(mpQQData.getProductCode()));
		parameters.put("P_DeductibleOption", vpmsResponseData.getDeductibleOption());

		// generate PDF
		InputStream inputStreamPath = null;
		InputStream inputStreamFile = null;
		try {
			// generate password from ic and dob
			String ic = dspCommonTblCustomer.getCustomerNricId();
			String custDob = dspCommonTblCustomer.getCustomerDob();
			String pswd = PasswordUtils.generatePasswordForPdf(ic, custDob);
			logger.info("password: {}", pswd);

			inputStreamPath = this.getClass().getClassLoader().getResourceAsStream("application.properties");
			Properties prop = new Properties();
			prop.load(inputStreamPath);

			logger.info("----------------- Start generating E Policy Information Page PDF ------------------");
			String file = prop.getProperty(DocumentConstant.DOCUMENT_DESTINATION_PATH_KEY) + filename;
			String jrxmlFile = null;
			if (company.equals(QQConstant.INSURANCE_COMPANY_KEY)) {
				jrxmlFile = prop.getProperty("EPolicyInfoFormJrxmlFile");
			} else {
				jrxmlFile = prop.getProperty("EPolicyInfoFormJrxmlFileTakaful");
			}
			logger.info("jrxmlFile: {}", jrxmlFile);
			inputStreamFile = getClass().getClassLoader().getResourceAsStream(jrxmlFile);
			JasperReport jasperReport = JasperCompileManager.compileReport(inputStreamFile);

			JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
			JRPdfExporter exporter = new JRPdfExporter();
			ExporterInput exporterInput = new SimpleExporterInput(print);

			exporter.setExporterInput(exporterInput);

			OutputStreamExporterOutput exporterOutput = new SimpleOutputStreamExporterOutput(file);

			exporter.setExporterOutput(exporterOutput);
			SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();

			// set password for pdf
			configuration.setEncrypted(true);
			configuration.set128BitKey(true);
			configuration.setUserPassword(pswd);
			configuration.setOwnerPassword(pswd);
			configuration.setPermissions(PdfWriter.ALLOW_COPY | PdfWriter.ALLOW_PRINTING);
			// end of set password

			exporter.setConfiguration(configuration);
			exporter.exportReport();

			logger.info("---------------- E Policy Information Page Pdf Gen Done! ---------------------------");

			return filename;
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		} finally {
			InputOutputStreamUtil.close(inputStreamPath);
			InputOutputStreamUtil.close(inputStreamFile);
		}

		return "";
	}

	public String eReceiptMethod(DspMpTblQQ mpQqData, DspCommonTblCustomer dspCommonTblCustomer, String policyNum,
			String company) {
		logger.info("DocGenController - eReceiptMethod()");

		String fileName = policyNum + "-e-Receipt.pdf";

		Map<String, Object> parameters = new HashMap<String, Object>();

		Format formatter = new SimpleDateFormat("dd-MMM-yy");
		String sysDate = formatter.format(new Date());
		logger.info("Today Date: {}", sysDate);

		String addressVal = "", add1 = "", add2 = "", add3 = "";
		if (dspCommonTblCustomer.getCustomerAddress1() != null) {
			add1 = dspCommonTblCustomer.getCustomerAddress1() + "\n";
		}
		if (dspCommonTblCustomer.getCustomerAddress2() != null) {
			add2 = dspCommonTblCustomer.getCustomerAddress2() + "\n";
		}
		if (dspCommonTblCustomer.getCustomerAddress3() != null) {
			add3 = dspCommonTblCustomer.getCustomerAddress3() + "\n";
		}

		if (dspCommonTblCustomer.getCustomerPostcode() != null) {
			addressVal = dspCommonTblCustomer.getCustomerPostcode();
		}

		if (dspCommonTblCustomer.getCustomerState() != null) {
			addressVal += ", " + dspCommonTblCustomer.getCustomerState();
		}

		if (dspCommonTblCustomer.getCustomerCountry() != null) {
			String country = "";

			if ("mal".equals(dspCommonTblCustomer.getCustomerNationality())) {
				country = "Malaysia";
			}

			addressVal += ", " + country;
		}

		parameters.put("P_Date", mpQqData.getQuotationCreationDatetime());
		parameters.put("P_PaymentMethod", "Kredit Kad / Credit Card");
		parameters.put("P_Name",
				dspCommonTblCustomer.getCustomerName() == null ? "" : dspCommonTblCustomer.getCustomerName());
		parameters.put("P_Address", add1 + add2 + add3 + addressVal);
		if (company.equals(QQConstant.INSURANCE_COMPANY_KEY)) {
			parameters.put("P_ProductName", QQConstant.PRODUCT_NAME_INSURANCE);
		} else {
			parameters.put("P_ProductName", QQConstant.PRODUCT_NAME_TAKAFUL);
		}

		parameters.put("P_ERefNo", policyNum);
		parameters.put("P_BeingPayment", "Pembayaran Premium Pertama / Initial Premium");

		String premiumMode = mpQqData.getPremiumPaymentFrequencyType();

		if (premiumMode.equalsIgnoreCase("monthly")) {
			parameters.put("P_PremiumMode", "Bulanan / " + StringUtils.capitalize(premiumMode));
		} else {
			parameters.put("P_PremiumMode", "Tahunan / " + StringUtils.capitalize(premiumMode));
		}

		parameters.put("P_ReceiptNo", policyNum);
		parameters.put("P_Amount",
				mpQqData.getPremiumPaymentFrequency() == null ? "" : mpQqData.getPremiumPaymentFrequency());
		parameters.put("P_AgentCode", QQConstant.AGENT_CODE_INSURANCE);

		// generate PDF
		InputStream inputStreamPath = null;
		InputStream inputStreamFile = null;
		try {
			// generate password from ic and dob
			String ic = dspCommonTblCustomer.getCustomerNricId();
			String custDob = dspCommonTblCustomer.getCustomerDob();
			String pswd = PasswordUtils.generatePasswordForPdf(ic, custDob);
			logger.info("password: {}", pswd);

			inputStreamPath = this.getClass().getClassLoader().getResourceAsStream("application.properties");
			Properties prop = new Properties();
			prop.load(inputStreamPath);

			logger.info("----------------- Start generating E Receipt PDF ------------------");
			String file = prop.getProperty(DocumentConstant.DOCUMENT_DESTINATION_PATH_KEY) + fileName;
			String jrxmlFile = null;
			if (company.equals(QQConstant.INSURANCE_COMPANY_KEY)) {
				jrxmlFile = prop.getProperty("EReceiptFormJrxmlFile");
			} else {
				jrxmlFile = prop.getProperty("EReceiptFormJrxmlFileTakaful");
			}

			logger.info("jrxmlFile: {}", jrxmlFile);
			inputStreamFile = getClass().getClassLoader().getResourceAsStream(jrxmlFile);
			JasperReport jasperReport = JasperCompileManager.compileReport(inputStreamFile);

			JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
			JRPdfExporter exporter = new JRPdfExporter();
			ExporterInput exporterInput = new SimpleExporterInput(print);

			exporter.setExporterInput(exporterInput);

			OutputStreamExporterOutput exporterOutput = new SimpleOutputStreamExporterOutput(file);

			exporter.setExporterOutput(exporterOutput);
			SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();

			configuration.setEncrypted(true);
			configuration.set128BitKey(true);
			configuration.setUserPassword(pswd);
			configuration.setOwnerPassword(pswd);
			configuration.setPermissions(PdfWriter.ALLOW_COPY | PdfWriter.ALLOW_PRINTING);

			exporter.setConfiguration(configuration);
			exporter.exportReport();

			logger.info("---------------- E Receipt Pdf Gen Done! ---------------------------");
			logger.info("Path: {}", file);

			return fileName;
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		} finally {
			InputOutputStreamUtil.close(inputStreamPath);
			InputOutputStreamUtil.close(inputStreamFile);
		}

		return "";
	}

	public String getPaymentContractPdfFile(HttpServletRequest request, HttpServletResponse response, String company, String filename) {
		logger.info("DocGenController - getPaymentContractPdfFile()");
		FileOutputStream fileOutputStream = null;
		try {

			HttpSession session = request.getSession();
			PaymentResponse paymentResponse = (PaymentResponse) session
					.getAttribute(QQConstant.PAYMENT_RESPONSE_SESSION_KEY);

			if (paymentResponse != null
					&& QQConstant.PAYMENT_GATEWAY_SUCCESS_CODE.equals(paymentResponse.getPaymentStatus())) {
				String policyNumber = paymentResponse.getPolicyNumber();
				InputStream inputStreamPath = null;
				inputStreamPath = this.getClass().getClassLoader().getResourceAsStream("application.properties");
				Properties prop = new Properties();
				prop.load(inputStreamPath);
				String pdfFile = null;
				if (company.equals(QQConstant.INSURANCE_COMPANY_KEY)) {
					pdfFile = prop.getProperty("ContractFile");
				} else {
					pdfFile = prop.getProperty("ContractFileTakaful");
				}
				logger.info("----------------- Start generating Contract PDF ---------------------");
				InputStream input = this.getClass().getClassLoader().getResourceAsStream(pdfFile);
				fileOutputStream = new FileOutputStream(
						prop.getProperty(DocumentConstant.DOCUMENT_DESTINATION_PATH_KEY) + filename, true);
				int b = 0;
				while ((b = input.read()) != -1) {
					fileOutputStream.write(b);
				}
				fileOutputStream.close();
				logger.info("----------------- Contract PDF generation done! --------------------");
				String filePath = prop.getProperty(DocumentConstant.DOCUMENT_DESTINATION_PATH_KEY) + filename;
				File file = new File(filePath);
				if (!file.exists()) {
					logger.error("The file does not existed...");
				}
			}

		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		} finally {
			try {
				if (fileOutputStream != null) {
					fileOutputStream.close();
				}
			} catch (IOException ex) {
				logger.error(ex.getMessage(), ex);
			}
		}
		return filename;
	}

	public String getPaymentNominationPdfFile(HttpServletRequest request, HttpServletResponse response,
			String company) {
		logger.info("DocGenController - getPaymentNominationPdfFile()");
		String filename = null;
		FileOutputStream fileOutputStream = null;
		try {
			HttpSession session = request.getSession();
			PaymentResponse paymentResponse = (PaymentResponse) session
					.getAttribute(QQConstant.PAYMENT_RESPONSE_SESSION_KEY);

			if (paymentResponse != null
					&& QQConstant.PAYMENT_GATEWAY_SUCCESS_CODE.equals(paymentResponse.getPaymentStatus())) {
				String policyNumber = paymentResponse.getPolicyNumber();
				filename = policyNumber + "-Nomination.pdf";
				InputStream inputStreamPath = null;
				inputStreamPath = this.getClass().getClassLoader().getResourceAsStream("application.properties");
				Properties prop = new Properties();
				prop.load(inputStreamPath);
				String pdfFile = null;
				if (company.equals(QQConstant.INSURANCE_COMPANY_KEY)) {
					pdfFile = prop.getProperty("NominationFile");
				} else {
					pdfFile = prop.getProperty("NominationFileTakaful");
				}
				logger.info("----------------- Start generating Nomination PDF ---------------------");
				InputStream input = this.getClass().getClassLoader().getResourceAsStream(pdfFile);
				fileOutputStream = new FileOutputStream(
						prop.getProperty(DocumentConstant.DOCUMENT_DESTINATION_PATH_KEY) + filename, true);
				int b = 0;
				while ((b = input.read()) != -1) {
					fileOutputStream.write(b);
				}
				fileOutputStream.close();
				logger.info("----------------- Nomination PDF generation done! --------------------");
				String filePath = prop.getProperty(DocumentConstant.DOCUMENT_DESTINATION_PATH_KEY) + filename;
				File file = new File(filePath);
				if (!file.exists()) {
					logger.error("The file does not existed...");
				}
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		} finally {
			try {
				if (fileOutputStream != null) {
					fileOutputStream.close();
				}
			} catch (IOException ex) {
				logger.error(ex.getMessage(), ex);
			}
		}
		return filename;
	}

	public String eDraftApplicationFormMethod(String fileName, String company, DspMpTblQQ mpQQData,
			DspCommonTblCustomer dspCommonTblCustomer, VpmsRequest vpmsRequest, List<DspTlTblOcc> occupations) {
		InputStream inputStreamPath = null;
		InputStream inputStreamFile = null;
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();

			setParameters(parameters, company, mpQQData, dspCommonTblCustomer, vpmsRequest, occupations, null, false);

			inputStreamPath = this.getClass().getClassLoader().getResourceAsStream(QQConstant.PROPERTIES_FILE_NAME);
			Properties prop = new Properties();
			prop.load(inputStreamPath);

			logger.info("----------------- Start generating E Draft App Form PDF ------------------");
			String file = prop.getProperty(DocumentConstant.DOCUMENT_DESTINATION_PATH_KEY) + fileName;
			String jrxmlFile = null;
			if (company.equals(QQConstant.INSURANCE_COMPANY_KEY)) {
				jrxmlFile = prop.getProperty("EDraftAppFormJrxmlFile");
			} else {
				jrxmlFile = prop.getProperty("EDraftAppFormJrxmlFileTakaful");
			}
			inputStreamFile = getClass().getClassLoader().getResourceAsStream(jrxmlFile);
			JasperReport jasperReport = JasperCompileManager.compileReport(inputStreamFile);

			JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
			JRPdfExporter exporter = new JRPdfExporter();
			ExporterInput exporterInput = new SimpleExporterInput(print);

			exporter.setExporterInput(exporterInput);

			OutputStreamExporterOutput exporterOutput = new SimpleOutputStreamExporterOutput(file);

			exporter.setExporterOutput(exporterOutput);
			SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
			exporter.setConfiguration(configuration);
			exporter.exportReport();

			logger.info("---------------- E Draft App Form Pdf Gen Done! ---------------------------");
			logger.info("Path: {}", file);

			return file;
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		} finally {
			InputOutputStreamUtil.close(inputStreamPath);
			InputOutputStreamUtil.close(inputStreamFile);
		}

		return null;
	}

	public String eApplicationFormMethod(String fileName, String company, DspMpTblQQ mpQQData,
			DspCommonTblCustomer dspCommonTblCustomer, VpmsRequest vpmsRequest, List<DspTlTblOcc> occupations,
			PaymentResponse paymentResponse) {
		InputStream inputStreamPath = null;
		InputStream inputStreamFile = null;
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();

			setParameters(parameters, company, mpQQData, dspCommonTblCustomer, vpmsRequest, occupations,
					paymentResponse, true);

			inputStreamPath = this.getClass().getClassLoader().getResourceAsStream(QQConstant.PROPERTIES_FILE_NAME);
			Properties prop = new Properties();
			prop.load(inputStreamPath);

			logger.info("----------------- Start generating E Draft App Form PDF ------------------");
			String file = prop.getProperty(DocumentConstant.DOCUMENT_DESTINATION_PATH_KEY) + fileName;
			String jrxmlFile = null;
			if (company.equals(QQConstant.INSURANCE_COMPANY_KEY)) {
				jrxmlFile = prop.getProperty("EAppFormJrxmlFile");
			} else {
				jrxmlFile = prop.getProperty("EAppFormJrxmlFileTakaful");
			}
			inputStreamFile = getClass().getClassLoader().getResourceAsStream(jrxmlFile);
			JasperReport jasperReport = JasperCompileManager.compileReport(inputStreamFile);

			JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
			JRPdfExporter exporter = new JRPdfExporter();
			ExporterInput exporterInput = new SimpleExporterInput(print);

			exporter.setExporterInput(exporterInput);

			OutputStreamExporterOutput exporterOutput = new SimpleOutputStreamExporterOutput(file);
			String ic = dspCommonTblCustomer.getCustomerNricId();
			String dob = dspCommonTblCustomer.getCustomerDob();
			String pswd = PasswordUtils.generatePasswordForPdf(ic, dob);

			exporter.setExporterOutput(exporterOutput);
			SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
			configuration.setEncrypted(true);
			configuration.set128BitKey(true);
			configuration.setUserPassword(pswd);
			configuration.setOwnerPassword(pswd);
			configuration.setPermissions(PdfWriter.ALLOW_COPY | PdfWriter.ALLOW_PRINTING);

			exporter.setConfiguration(configuration);
			exporter.exportReport();

			logger.info("---------------- E App Pdf Gen Done! ---------------------------");
			logger.info("Path: {}", file);

			return file;
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		} finally {
			InputOutputStreamUtil.close(inputStreamPath);
			InputOutputStreamUtil.close(inputStreamFile);
		}

		return null;
	}

	public void setParameters(Map<String, Object> parameters, String company, DspMpTblQQ mpQQData,
			DspCommonTblCustomer dspCommonTblCustomer, VpmsRequest vpmsRequest, List<DspTlTblOcc> occupations,
			PaymentResponse paymentResponse, boolean isPostPayment) {

		parameters.put("P_FullName",
				dspCommonTblCustomer.getCustomerName() == null ? "" : dspCommonTblCustomer.getCustomerName());
		parameters.put("P_NRICNumber",
				dspCommonTblCustomer.getCustomerNricId() == null ? "" : dspCommonTblCustomer.getCustomerNricId());
		parameters.put("P_DOB",
				dspCommonTblCustomer.getCustomerDob() == null ? "" : dspCommonTblCustomer.getCustomerDob());

		String gender = dspCommonTblCustomer.getCustomerGender();
		if (StringUtils.equals(QQConstant.FEMALE_CODE, gender)) {
			parameters.put("P_Gender", QQConstant.FEMALE);
		} else {
			parameters.put("P_Gender", QQConstant.MALE);
		}

		parameters.put("P_MaritalStatus", dspCommonTblCustomer.getCustomerMaritalstatus() == null ? ""
				: dspCommonTblCustomer.getCustomerMaritalstatus());
		parameters.put("P_Race", dspCommonTblCustomer.getCustomerRace());
		parameters.put("P_Religion", dspCommonTblCustomer.getCustomerReligion());
		parameters.put("P_Nationality",
				CommonUtils.getNationalityFromCode(dspCommonTblCustomer.getCustomerNationality()));
		parameters.put("P_AddressOfUSA", "");
		parameters.put("P_EmailAddress",
				dspCommonTblCustomer.getCustomerEmail() == null ? "" : dspCommonTblCustomer.getCustomerEmail());
		parameters.put("P_MobileNo",
				dspCommonTblCustomer.getCustomerMobileNo() == null ? "" : dspCommonTblCustomer.getCustomerMobileNo());

		String addressVal = "";
		String add1 = "";
		String add2 = "";
		String add3 = "";
		if (dspCommonTblCustomer.getCustomerAddress1() != null) {
			add1 = dspCommonTblCustomer.getCustomerAddress1() + "\n";
		}
		if (dspCommonTblCustomer.getCustomerAddress2() != null) {
			add2 = dspCommonTblCustomer.getCustomerAddress2() + "\n";
		}
		if (dspCommonTblCustomer.getCustomerAddress3() != null) {
			add3 = dspCommonTblCustomer.getCustomerAddress3() + "\n";
		}

		if (dspCommonTblCustomer.getCustomerPostcode() != null) {
			addressVal = dspCommonTblCustomer.getCustomerPostcode() + ",";
		}

		String countryPincode = dspCommonTblCustomer.getCustomerState() + ","
				+ QQConstant.FORM_DEFAULT_COUNTRY;

		parameters.put("P_HomeAddress", add1 + add2 + add3 + addressVal + countryPincode);

		String mailAddressVal = "";
		String mailAdd1 = "";
		String mailAdd2 = "";
		String mailAdd3 = "";

		if (dspCommonTblCustomer.getCustomerMailAddress1() != null) {
			mailAdd1 = dspCommonTblCustomer.getCustomerMailAddress1() + "\n";
		}
		if (dspCommonTblCustomer.getCustomerMailAddress2() != null) {
			mailAdd2 = dspCommonTblCustomer.getCustomerMailAddress2() + "\n";
		}
		if (dspCommonTblCustomer.getCustomerMailAddress3() != null) {
			mailAdd3 = dspCommonTblCustomer.getCustomerMailAddress3() + "\n";
		}
		if (dspCommonTblCustomer.getCustomerMailPostcode() != null) {
			mailAddressVal = dspCommonTblCustomer.getCustomerMailPostcode() + ",";
		}

		String mailCountryPincode = dspCommonTblCustomer.getCustomerMailState() + ","
				+ QQConstant.FORM_DEFAULT_COUNTRY;

		if (dspCommonTblCustomer.getHomeMailCheck().equalsIgnoreCase("y")) {
			parameters.put("P_MailingAddress", add1 + add2 + add3 + addressVal + countryPincode);
		} else {
			parameters.put("P_MailingAddress", mailAdd1 + mailAdd2 + mailAdd3 + mailAddressVal + mailCountryPincode);
		}

		String occupation = null;
		String occupationCode = dspCommonTblCustomer.getCustomerOccupation();
		for (DspTlTblOcc dspTlTblOcc : occupations) {
			if (occupationCode.equalsIgnoreCase(dspTlTblOcc.getOccCode())) {
				occupation = dspTlTblOcc.getOccName();
				break;
			}
		}
		parameters.put("P_Occupation", occupation);

		parameters.put("P_EmployerName",
				dspCommonTblCustomer.getCustomerEmployer() == null ? "" : dspCommonTblCustomer.getCustomerEmployer());

		String salaryRange = (dspCommonTblCustomer.getCustomerSalaryRange() == null ? ""
				: dspCommonTblCustomer.getCustomerSalaryRange());

		String salary = null;
		if (StringUtils.equals("range1", salaryRange)) {
			salary = "RM1,000 and below";
		} else if (StringUtils.equals("range2", salaryRange)) {
			salary = "RM1,001 - RM3,000";
		} else if (StringUtils.equals("range3", salaryRange)) {
			salary = "RM3,001 - RM5,000";
		} else {
			salary = "RM5,001 and above";
		}

		parameters.put("P_MonthlyIncome", salary);

		parameters.put("P_PolicyNumber", "");
		parameters.put("P_ProductDesc", CommonUtils.getProductNameFromCompany(company));

		Double sumInsuredDoubleVal = null;
		if (mpQQData.getMpSumInsured() != null) {
			sumInsuredDoubleVal = Double.parseDouble(mpQQData.getMpSumInsured());
			String formatted = CommonUtils.formatPremium(sumInsuredDoubleVal.toString());
			parameters.put("P_SumInsured", formatted);
		} else {
			parameters.put("P_SumInsured", "");
		}

		parameters.put("P_PremiumAmount",
				mpQQData.getPremiumPaymentFrequency() == null ? "" : mpQQData.getPremiumPaymentFrequency());
		parameters.put("P_PolicyTeamCoverPeriod",
				mpQQData.getDurationOfBenifit() == null ? "0" : mpQQData.getDurationOfBenifit() + " years");
		parameters.put("P_CardholderName", "");
		parameters.put("P_CreditCardNumber", "");

		parameters.put("P_PlanCheckBox", "Y");
		parameters.put("P_heightCM", mpQQData.getHeight() == null ? "" : mpQQData.getHeight());
		parameters.put("P_weightKG", mpQQData.getWeight() == null ? "" : mpQQData.getWeight());

		parameters.put("P_DeclarationAgreeChk", "Y");
		parameters.put("P_DeclarationConfirmChk", "Y");

		parameters.put("P_PremiumType",
				mpQQData.getPremiumPaymentFrequencyType() == null ? "" : mpQQData.getPremiumPaymentFrequencyType());

		String formattedDate = CommonUtils.formatPdfDate();
		parameters.put("P_TimeStamp", formattedDate);

		parameters.put("P_PlanType", CommonUtils.getPlanNameFromProductCode(mpQQData.getProductCode()));
		parameters.put("P_DeductibleAmount", CommonUtils.formatCoverage(vpmsRequest.getDeductible()));

		if (isPostPayment == true && paymentResponse != null) {
			parameters.put("P_PolicyNumber", paymentResponse.getPolicyNumber());
		}

	}

	public String ePDSAfterDoc(DocCustomPojo doc, String company) throws IOException {
		logger.info("DocGenController - ePDSAfterDoc()");

		Map<String, Object> parameters = new HashMap<String, Object>();

		String sysDate = CommonUtils.formatPdfDateOnly();
		parameters.put("P_CreateDate", sysDate);
		parameters.put("P_issuedDate", sysDate);

		parameters.put("P_SumInsured", doc.getSumCovered());
		parameters.put("P_PremiumAmount", doc.getPayableAmt() == null ? "" : doc.getPayableAmt());

		String mode = null;
		if (doc.getPaymentFrequency().equalsIgnoreCase("Annually")) {
			mode = "annually";
		} else {
			mode = "monthly";
		}

		parameters.put("P_ModeOfPayment", mode);
		parameters.put("P_Duration", doc.getDurationOfBenefit());
		String fileName = doc.getPolicyNo() + "-e-ProductDisclosureSheet.pdf";

		String pswd = PasswordUtils.generatePasswordForPdf(doc.getIc(), doc.getDob());

		try (InputStream in = this.getClass().getClassLoader().getResourceAsStream("application.properties")) {
			Properties prop = new Properties();
			prop.load(in);

			String jrxmlFile = null;
			if (company.equals(QQConstant.INSURANCE_COMPANY_KEY)) {
				jrxmlFile = prop.getProperty("EPDSFormJrxmlFile");
			} else {
				jrxmlFile = prop.getProperty("EPDSFormJrxmlFileTakaful");
			}

			InputStream input = this.getClass().getClassLoader().getResourceAsStream(jrxmlFile);
			JasperReport jasperReport = JasperCompileManager.compileReport(input);
			JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());

			JRPdfExporter exporter = new JRPdfExporter();
			ExporterInput exporterInput = new SimpleExporterInput(print);
			exporter.setExporterInput(exporterInput);
			OutputStreamExporterOutput exporterOutput = new SimpleOutputStreamExporterOutput(
					prop.getProperty("DocumentDestionationPath") + fileName);
			exporter.setExporterOutput(exporterOutput);
			String file = fileName;
			SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
			// set password for pdf
			configuration.setEncrypted(true);
			configuration.set128BitKey(true);
			configuration.setUserPassword(pswd);
			configuration.setOwnerPassword(pswd);
			configuration.setPermissions(PdfWriter.ALLOW_COPY | PdfWriter.ALLOW_PRINTING);
			// end of set password

			exporter.setConfiguration(configuration);
			exporter.exportReport();
			logger.info("------------------- e-PDS after payment Pdf Gen Done! ------------------");
			logger.info("Destination Path: {}", prop.getProperty("DocumentDestionationPath") + fileName);
			logger.info("Source Path: {}", prop.getProperty("DocumentSourcePath") + fileName);

			File f = new File(prop.getProperty("DocumentSourcePath") + fileName);
			FileInputStream fr = new FileInputStream(f);

			return file;

		} catch (JRException ex) {
			logger.error(ex.getMessage(), ex);
			return "failtoStore";
		}

	}

	public String salesIllustrationFormMethod(DocCustomPojo formVo, VpmsRequest vpmsRequest,
			VpmsResponseData vpmsResponseData, String company, DspCommonTblCustomer dspCommonTblCustomer,
			List<DspTlTblOcc> occupations, String filename) throws IOException {

		Map<String, Object> parameters = new HashMap<String, Object>();

		String sysDate = CommonUtils.formatSiDate();

		String pswd = PasswordUtils.generatePasswordForPdf(formVo.getIc(), formVo.getDob());
		parameters.put("P_Date", sysDate);
		parameters.put("P_CustomerName", formVo.getName() == null ? "" : formVo.getName());

		String gender = dspCommonTblCustomer.getCustomerGender();
		if (StringUtils.equals(QQConstant.FEMALE_CODE, gender)) {
			parameters.put("P_Gender", QQConstant.FEMALE);
		} else {
			parameters.put("P_Gender", QQConstant.MALE);
		}
		parameters.put("P_Ic", formVo.getIc());
		String finalDate = formVo.getDob();
		parameters.put("P_Dob", CommonUtils.formatDobToSiDate(finalDate, QQConstant.DOB_FORM_DATE_FORMAT));
		parameters.put("P_NextBDay", CommonUtils.getAge(finalDate, QQConstant.DOB_FORM_DATE_FORMAT) + 1);

		parameters.put("P_TermOfCoverage", formVo.getDurationOfBenefit());
		parameters.put("P_PlanType", vpmsResponseData.getPlanType());
		parameters.put("P_DeductibleAmount", CommonUtils.getDeductionAmountFromString(vpmsRequest.getDeductible()));

		String finalAnnulizedValue = null;
		finalAnnulizedValue = CommonUtils.formatPremium(vpmsResponseData.getFirstYearPremium());
		parameters.put("P_AnnualizedAmnt", finalAnnulizedValue);

		String premiumMode = null;
		String premiumModeText = null;

		List<SalesIllustration> salesIllustrations = new ArrayList<>();
		if (company.equals(QQConstant.INSURANCE_COMPANY_KEY)) {
			if ((formVo.getPaymentFrequency()).equalsIgnoreCase("annually")) {
				premiumModeText = "First Year Premium (RM)";
				premiumMode = "Yearly";
			} else {
				premiumModeText = "First Year Monthly Premium (RM)";
				premiumMode = "Monthly";
			}
			int endCoveredAgeAndWakalahLength = vpmsResponseData.getEndCoveredAgeAndWakalahArray().length;
			int annualPremiumArrayLength = vpmsResponseData.getAnnualPremiumArray().length;
			int endCoveredAgeArrayLength = vpmsResponseData.getEndCoveredAgeArray().length;
			int totalPremiumLength = vpmsResponseData.getTotalPremiumToDate().length;
			boolean isSame = endCoveredAgeAndWakalahLength == annualPremiumArrayLength
					&& endCoveredAgeAndWakalahLength == endCoveredAgeArrayLength
					&& endCoveredAgeAndWakalahLength == totalPremiumLength;
			if (isSame == true) {
				int listSize = endCoveredAgeAndWakalahLength;
				SalesIllustration salesIllustration = null;
				String[] annualPremium = vpmsResponseData.getAnnualPremiumArray();
				String[] totalPremiumToDate = vpmsResponseData.getTotalPremiumToDate();
				String[] endCoveredAge = vpmsResponseData.getEndCoveredAgeArray();
				int endPolicyYear = 0;
				for (int i = 0; i < listSize; i++) {
					salesIllustration = new SalesIllustration();
					endPolicyYear = i + 1;
					if (endPolicyYear < 20 || (endPolicyYear >= 20 && endPolicyYear % 5 == 0)
							|| (i == (listSize - 1) && endPolicyYear % 5 != 0)) {
						salesIllustration.setEndPolicyYear(String.valueOf(endPolicyYear));
						salesIllustration.setAnnualPremium(CommonUtils.formatCoverage(annualPremium[i]));
						salesIllustration.setTotalPremiumToDate(CommonUtils.formatCoverage(totalPremiumToDate[i]));
						salesIllustration.setEndCoveredAge(endCoveredAge[i]);
						salesIllustrations.add(salesIllustration);
					}
				}
			}
		} else {
			if ((formVo.getPaymentFrequency()).equalsIgnoreCase("annually")) {
				premiumModeText = "First Year Contribution (RM)";
				premiumMode = "Yearly";
			} else {
				premiumModeText = "First Year Monthly Contribution (RM)";
				premiumMode = "Monthly";
			}
			int endCoveredAgeAndWakalahLength = vpmsResponseData.getEndCoveredAgeAndWakalahArray().length;
			int annualPremiumArrayLength = vpmsResponseData.getAnnualPremiumArray().length;
			int endCoveredAgeArrayLength = vpmsResponseData.getEndCoveredAgeArray().length;
			int totalPremiumLength = vpmsResponseData.getTotalPremiumToDate().length;

			boolean isSame = endCoveredAgeAndWakalahLength == annualPremiumArrayLength
					&& endCoveredAgeAndWakalahLength == endCoveredAgeArrayLength
					&& endCoveredAgeAndWakalahLength == totalPremiumLength;

			if (isSame == true) {
				int listSize = endCoveredAgeAndWakalahLength;
				SalesIllustration salesIllustration = null;
				String[] annualPremium = vpmsResponseData.getAnnualPremiumArray();
				String[] totalPremiumToDate = vpmsResponseData.getTotalPremiumToDate();
				String[] endCoveredAge = vpmsResponseData.getEndCoveredAgeArray();
				String[] wakalahFee = vpmsResponseData.getEndCoveredAgeAndWakalahArray();

				int endPolicyYear = 0;
				for (int i = 0; i < listSize; i++) {
					salesIllustration = new SalesIllustration();
					endPolicyYear = i + 1;
					if (endPolicyYear < 20 || (endPolicyYear >= 20 && endPolicyYear % 5 == 0)
							|| (i == (listSize - 1) && endPolicyYear % 5 != 0)) {
						salesIllustration.setEndPolicyYear(String.valueOf(endPolicyYear));
						salesIllustration.setAnnualPremium(CommonUtils.formatCoverage(annualPremium[i]));
						salesIllustration.setWakalahFee(CommonUtils.formatCoverage(wakalahFee[i]));
						salesIllustration.setTotalPremiumToDate(CommonUtils.formatCoverage(totalPremiumToDate[i]));
						salesIllustration.setEndCoveredAge(endCoveredAge[i]);
						salesIllustrations.add(salesIllustration);
					}
				}
			}
		}

		parameters.put("P_PremiumMode", premiumMode);
		parameters.put("P_PremiumModeText", premiumModeText);
		parameters.put("P_AnnualLimit", "RM" + CommonUtils.formatCoverage(
				StringUtils.replace(vpmsResponseData.getAnnualLimit(), QQConstant.VPMS_RINGGIT_MALAYSIA, "")));
		parameters.put("P_RoomBoard", vpmsResponseData.getRoomRatePerDay());
		parameters.put("P_VpmsDeduction", CommonUtils.getDeductionOptionString(vpmsRequest.getDeductible()));
		parameters.put("P_Occupation", vpmsResponseData.getOccupationClass());

		try (InputStream in = this.getClass().getClassLoader().getResourceAsStream("application.properties")) {

			Properties prop = new Properties();
			prop.load(in);
			String jrxmlFile = null;
			if (company.equals(QQConstant.INSURANCE_COMPANY_KEY)) {
				jrxmlFile = prop.getProperty("ESalesIllFormJrxmlFile");
			} else {
				jrxmlFile = prop.getProperty("ESalesIllFormJrxmlFileTakaful");
			}

			InputStream input = this.getClass().getClassLoader().getResourceAsStream(jrxmlFile);
			JasperReport jasperReport = JasperCompileManager.compileReport(input);
			JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters,
					new JRBeanCollectionDataSource(salesIllustrations));

			JRPdfExporter exporter = new JRPdfExporter();
			ExporterInput exporterInput = new SimpleExporterInput(print);
			exporter.setExporterInput(exporterInput);
			OutputStreamExporterOutput exporterOutput = new SimpleOutputStreamExporterOutput(
					prop.getProperty("DocumentDestionationPath") + filename);
			exporter.setExporterOutput(exporterOutput);
			SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
			// set password for pdf
			configuration.setEncrypted(true);
			configuration.set128BitKey(true);
			configuration.setUserPassword(pswd);
			configuration.setOwnerPassword(pswd);
			configuration.setPermissions(PdfWriter.ALLOW_COPY | PdfWriter.ALLOW_PRINTING);
			// end of set password
			exporter.setConfiguration(configuration);
			exporter.exportReport();
			logger.info("-------------------- PDS Pdf Gen Done! -------------------------");

			File f = new File(prop.getProperty("DocumentSourcePath") + filename);
			FileInputStream fr = new FileInputStream(f);

			return filename;

		} catch (JRException ex) {
			logger.error(ex.getMessage(), ex);
			return "failtoStore";
		}

	}

}
