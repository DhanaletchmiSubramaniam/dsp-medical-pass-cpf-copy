package com.etiqa.medicalpass.controller.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.etiqa.medicalpass.constant.QQConstant;
import com.etiqa.medicalpass.pojo.DspMiTblPostcodes;
import com.etiqa.medicalpass.pojo.DspTlTblOcc;
import com.etiqa.medicalpass.service.DspMiTblPostcodesService;
import com.etiqa.medicalpass.service.DspTlTblOccService;
import com.etiqa.medicalpass.util.CommonUtils;

@RestController
@RequestMapping(value = "/api/v1/utils")
public class UtilsController {

	private static final Logger logger = LoggerFactory.getLogger(UtilsController.class);

	@Autowired
	DspTlTblOccService dspTlTblOccService;

	@Autowired
	DspMiTblPostcodesService dspMiTblPostcodesService;

	@RequestMapping(value = "/ageNextBirthday", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> getAge(@RequestParam String dateOfBirth, @RequestParam String dateFormat,
			HttpServletResponse response) {

		Integer age = -1;
		Map<String, Object> map = new HashMap<>();
		try {
			dateOfBirth = CommonUtils.trimToNullParam(response, dateOfBirth);
			dateFormat = CommonUtils.trimToNullParam(response, dateFormat);

			age = CommonUtils.getAge(dateOfBirth, dateFormat);
			age = age + 1;
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			return new ResponseEntity<Map<String, Object>>(map, HttpStatus.BAD_GATEWAY);
		} finally {
			map.put("age", age);
		}

		return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
	}

	@RequestMapping(value = "/verifyIcWithBirthDate", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> verifyIcWithBirthDate(@RequestParam String dateOfBirth,
			@RequestParam String dateFormat, @RequestParam String strNric) {

		Integer isVerified = 0;
		Map<String, Object> map = new HashMap<>();
		try {
			isVerified = CommonUtils.verifyIcWithBirthDate(dateOfBirth, dateFormat, strNric);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			return new ResponseEntity<Map<String, Object>>(map, HttpStatus.BAD_GATEWAY);
		} finally {
			map.put("isVerified", isVerified);
		}

		return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
	}

	@RequestMapping(value = "/occupation", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> getOccupation(HttpServletRequest request,
			@RequestParam String occupation) {

		Map<String, Object> map = new HashMap<>();
		List<DspTlTblOcc> occupations = new ArrayList<>();
		try {
			HttpSession session = request.getSession();
			occupations = dspTlTblOccService.getOccupation(request, occupation);
			CommonUtils.clearSession(QQConstant.OCCUPATION_SESSION_KEY, session);
			session.setAttribute(QQConstant.OCCUPATION_SESSION_KEY, occupations);
			map.put("data", occupations);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			return new ResponseEntity<Map<String, Object>>(map, HttpStatus.BAD_GATEWAY);
		} finally {
			map.put("length", occupations.size());
		}

		return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
	}

	@RequestMapping(value = "/postcode", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> getPostcode(HttpServletRequest request, @RequestParam String postcode) {

		Map<String, Object> map = new HashMap<>();
		List<DspMiTblPostcodes> dspMiTblPostcodes = new ArrayList<>();
		DspMiTblPostcodes postcodeData = null;
		Integer isValidPostcode = 0;
		try {
			dspMiTblPostcodes = dspMiTblPostcodesService.getState(request, postcode);
			if (dspMiTblPostcodes.isEmpty() == false) {
				postcodeData = dspMiTblPostcodes.get(0);
				isValidPostcode = 1;
				map.put("code", postcodeData.getStateCode());
				map.put("desc", postcodeData.getStateDesc());
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			return new ResponseEntity<Map<String, Object>>(map, HttpStatus.BAD_GATEWAY);
		} finally {
			map.put("length", dspMiTblPostcodes.size());
			map.put("isValidPostcode", isValidPostcode);
		}

		return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
	}

}
