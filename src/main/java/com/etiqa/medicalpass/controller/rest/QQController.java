package com.etiqa.medicalpass.controller.rest;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.etiqa.medicalpass.constant.QQConstant;
import com.etiqa.medicalpass.pojo.DspCommonTblBankList;
import com.etiqa.medicalpass.pojo.DspCommonTblCustomer;
import com.etiqa.medicalpass.pojo.DspCommonTblCustomerExample;
import com.etiqa.medicalpass.pojo.DspCommonTblQQ;
import com.etiqa.medicalpass.pojo.DspCommonTblQQExample;
import com.etiqa.medicalpass.pojo.DspMpTblQQ;
import com.etiqa.medicalpass.pojo.DspMpTblQQExample;
import com.etiqa.medicalpass.pojo.DspMpTblUwRejection;
import com.etiqa.medicalpass.pojo.DspMpTblUwRejectionExample;
import com.etiqa.medicalpass.pojo.DspMpTblVpms;
import com.etiqa.medicalpass.pojo.DspTlTblOcc;
import com.etiqa.medicalpass.pojo.MDMPersonalData;
import com.etiqa.medicalpass.pojo.PaymentResponse;
import com.etiqa.medicalpass.pojo.VpmsRequest;
import com.etiqa.medicalpass.pojo.VpmsResponse;
import com.etiqa.medicalpass.pojo.VpmsResponseData;
import com.etiqa.medicalpass.pojo.VpmsResponseTakaful;
import com.etiqa.medicalpass.pojo.VpmsResponseTakafulData;
import com.etiqa.medicalpass.service.AsyncService;
import com.etiqa.medicalpass.service.DspCommonTblCustomerService;
import com.etiqa.medicalpass.service.DspCommonTblQQService;
import com.etiqa.medicalpass.service.DspMPTblQQService;
import com.etiqa.medicalpass.service.DspMpTblTerroristService;
import com.etiqa.medicalpass.service.DspMpTblUwRejectionService;
import com.etiqa.medicalpass.service.QQService;
import com.etiqa.medicalpass.service.VpmsService;
import com.etiqa.medicalpass.util.CommonUtils;
import com.etiqa.medicalpass.util.InputOutputStreamUtil;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

@Controller
@RequestMapping(value = QQConstant.REQUEST_MAPPING_QQ_ENDPOINT_PREFIX)
public class QQController {

	private static final Logger logger = LoggerFactory.getLogger(QQController.class);

	@Autowired
	private AsyncService asyncService;

	@Autowired
	private DspMpTblTerroristService dspMpTblTerroristService;

	@Autowired
	private DspCommonTblQQService dspCommonTblQQService;

	@Autowired
	private DspMPTblQQService dspMPTblQQService;

	@Autowired
	private DspMpTblUwRejectionService dspMpTblUwRejectionService;

	@Autowired
	private VpmsService vpmsService;

	@Autowired
	private QQService qqService;

	@Autowired
	private DspCommonTblCustomerService dspCommonTblCustomerService;

	@Value("#{servletContext.contextPath}")
	private String servletContextPath;

	private Properties prop = new Properties();

	@RequestMapping(value = QQConstant.REQUEST_MAPPING_QQ_ENDPOINT_ONE, method = RequestMethod.POST)
	public void insertFromQQ1(@RequestParam String nric, @RequestParam String dob,
			@RequestParam String paymentFrequency, @RequestParam String userLanguage,
			@RequestParam String selectedPackage, @RequestParam String gender, @RequestParam String deductible,
			@RequestParam String occupationCode, @RequestParam String company, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String page = servletContextPath + QQConstant.PAGE_URL_INSURACE_QQ2A_EN;
		String pageRejected = servletContextPath + QQConstant.PAGE_URL_INSURACE_QQ1_EN;

		DspMpTblQQ mpQQData = null;
		InputStream is = this.getClass().getClassLoader().getResourceAsStream(QQConstant.PROPERTIES_FILE_NAME);
		try {
			prop.load(is);
			String url = prop.getProperty(QQConstant.CPF_MDM_URL_KEY);
			HttpSession session = request.getSession();
			mpQQData = (DspMpTblQQ) session.getAttribute(QQConstant.MP_QQ_DATA_SESSION_KEY);

			if (mpQQData == null) {
				CommonUtils.clearAllSession(session);
				DspCommonTblQQ dspCommonTblQQ = new DspCommonTblQQ();
				mpQQData = new DspMpTblQQ();
				BigDecimal qqIdKey = new BigDecimal(0);
				nric = CommonUtils.trimToNullParam(response, nric);
				dob = CommonUtils.trimToNullParam(response, dob);
				paymentFrequency = CommonUtils.trimToNullParam(response, paymentFrequency);
				userLanguage = CommonUtils.trimToNullParam(response, userLanguage);
				selectedPackage = CommonUtils.trimToNullParam(response, selectedPackage);
				deductible = CommonUtils.trimToNullParam(response, deductible);
				occupationCode = CommonUtils.trimToNullParam(response, occupationCode);
				company = CommonUtils.trimToNullParam(response, company);

				String productCode = CommonUtils.getProductCode(selectedPackage, company);
				dspCommonTblQQ.setProductCode(productCode);
				dspCommonTblQQ.setCreatedDate(new Date());
				Gson gson = new Gson();
				DspMpTblQQExample mpTblQQExample = new DspMpTblQQExample();
				String key = null;
				session.setAttribute(QQConstant.COMPANY_SESSION_KEY, company);

				if (company.equals(QQConstant.INSURANCE_COMPANY_KEY)) {
					key = QQConstant.INSURANCE_PRODUCT_CODE_SEARCH_KEY;
				} else {
					key = QQConstant.TAKAFUL_PRODUCT_CODE_SEARCH_KEY;
					page = servletContextPath + QQConstant.PAGE_URL_TAKAFUL_QQ2A_EN;
					pageRejected = servletContextPath + QQConstant.PAGE_URL_TAKAFUL_QQ1_EN;
				}
				mpTblQQExample.createCriteria().andProductCodeLike(key)
						.andMpQqStatusEqualTo(QQConstant.QUOTATION_STATUS_COMPLETE).andNewIcCopyEqualTo(nric);

				Future<Integer> futureInsertDSPCommon = dspCommonTblQQService.insertDspCommonTblQQ(dspCommonTblQQ);
				Future<Long> futureRejection = dspMpTblUwRejectionService.getRejectionCount(nric,
						QQConstant.REJECT_CODE_PURCHASE_BEFORE);
				Future<Long> futurePolicyPurchasedCount = dspMPTblQQService.getPolicyPurchasedCount(mpTblQQExample);

				Long rejectionCount = 0L;
				Long policyPurchasedCount = 0L;

				if (futureInsertDSPCommon.get() == 1) {
					qqIdKey = dspCommonTblQQ.getDspQqId();
					session.setAttribute(QQConstant.DSP_QQ_ID_DATA_SESSION_KEY, qqIdKey);
				}
				rejectionCount = futureRejection.get();
				policyPurchasedCount = futurePolicyPurchasedCount.get();

				VpmsResponse vpmsResponse = null;
				VpmsResponseData vpmsResponseData = null;

				VpmsResponseTakaful vpmsResponseTakaful = null;

				VpmsRequest vpmsRequest = new VpmsRequest();
				vpmsRequest.setDateOfBirth(CommonUtils.formatDOBForVPMS(dob, QQConstant.DOB_FORM_DATE_FORMAT));
				vpmsRequest.setDeductible(deductible);
				vpmsRequest.setGender(gender);
				vpmsRequest.setOccupation(occupationCode);
				vpmsRequest.setPlanType(selectedPackage);
				vpmsRequest.setIsSmoker(QQConstant.VPMS_REQUEST_DEFAULT_IS_SMOKER);
				vpmsRequest.setLang(QQConstant.VPMS_REQUEST_DEFAULT_LANG);
				vpmsRequest.setName(QQConstant.VPMS_REQUEST_DEFAULT_NAME);
				vpmsRequest.setNric(QQConstant.VPMS_REQUEST_DEFAULT_NRIC);
				vpmsRequest.setQuotationDate(CommonUtils.formatVPMSDate());
				vpmsRequest.setPaymentMode(paymentFrequency);

				mpQQData = new DspMpTblQQ();
				if (company.equals(QQConstant.INSURANCE_COMPANY_KEY)) {
					vpmsResponse = vpmsService.getVpmsInsurance(vpmsRequest);
					mpQQData.setAgentCode(QQConstant.AGENT_CODE_INSURANCE);
				} else {
					vpmsResponseTakaful = vpmsService.getVpmsTakaful(vpmsRequest);
					mpQQData.setAgentCode(QQConstant.AGENT_CODE_TAKAFUL);
					vpmsResponse = assignToVpmsResponse(vpmsResponse, vpmsResponseTakaful);
				}

				if (vpmsResponse == null) {
					throw new NullPointerException(QQConstant.ERROR_MESSAGE_VPMS_RESPONSE_NULL);
				}
				vpmsResponseData = vpmsResponse.getData();

				DspMpTblVpms dspMpTblVpms = vpmsService.compileIntoDspMpTblVpms(qqIdKey, vpmsRequest, vpmsResponse,
						vpmsResponseData);
				vpmsService.insertDspMpTblVpmsAsyncVoid(dspMpTblVpms);

				mpQQData.setUserLanguage(userLanguage);
				mpQQData.setMpQqStatus(QQConstant.QUOTATION_STATUS_OPEN);
				mpQQData.setProductCode(productCode);
				mpQQData.setNewIcCopy(nric);
				mpQQData.setDob(dob);
				mpQQData.setDropInQq(new BigDecimal(QQConstant.PAGE_NUMBER_1));
				mpQQData.setDspQqId(qqIdKey);
				mpQQData.setQuotationCreationDatetime(CommonUtils.formatOracleDate());
				mpQQData.setUpdateDatetime(new Date());
				mpQQData.setGender(StringUtils.lowerCase(gender));
				mpQQData.setDurationOfBenifit(vpmsResponseData.getPaymentTerm());
				mpQQData.setIsSmoker(StringUtils.lowerCase(QQConstant.VPMS_REQUEST_DEFAULT_IS_SMOKER));
				mpQQData.setMpSumInsured(StringUtils.replace(vpmsResponseData.getRoomRatePerDay(),
						QQConstant.VPMS_RINGGIT_MALAYSIA, ""));

				if (paymentFrequency.equals(QQConstant.PAYMENT_TYPE_CODE_MONTHLY)) {
					mpQQData.setPremiumPaymentFrequency(vpmsResponseData.getMonthlyPremium());
					mpQQData.setPremiumPaymentFrequencyType(QQConstant.PAYMENT_TYPE_MONTHLY);
				} else {
					mpQQData.setPremiumPaymentFrequency(vpmsResponseData.getAnnualPremium());
					mpQQData.setPremiumPaymentFrequencyType(QQConstant.PAYMENT_TYPE_ANNUALLY);
				}

				Future<Map<String, Object>> futureMDM = asyncService.getMDMCustomerService(nric, url);
				Future<Integer> futureInsertDSPMpQQ = dspMPTblQQService.insertDspMpTblQQ(mpQQData);

				futureInsertDSPMpQQ.get();
				JsonElement jsonElement = gson.toJsonTree(futureMDM.get());
				MDMPersonalData personalData = gson.fromJson(jsonElement, MDMPersonalData.class);
				session.setAttribute(QQConstant.MDM_DATA_SESSION_KEY, personalData);
				session.setAttribute(QQConstant.MP_QQ_DATA_SESSION_KEY, mpQQData);

				if (policyPurchasedCount > 0) {
					updateRejection(session, qqIdKey, mpQQData, QQConstant.REJECT_CODE_PURCHASE_BEFORE);
					response.sendRedirect(pageRejected);
					return;
				}

				if (rejectionCount > 0) {
					updateRejection(session, qqIdKey, mpQQData, QQConstant.REJECT_CODE_REJECTED_BEFORE);
					response.sendRedirect(pageRejected);
					return;
				}

				DspCommonTblCustomer dspCommonTblCustomer = new DspCommonTblCustomer();
				dspCommonTblCustomer.setQqId(qqIdKey);
				dspCommonTblCustomer.setCreateDate(new Date());
				dspCommonTblCustomer.setUpdatedDate(new Date());
				dspCommonTblCustomer.setCustomerIdType(QQConstant.CUSTOMER_ID_TYPE_NRIC);
				dspCommonTblCustomer.setCustomerNricId(nric);
				dspCommonTblCustomer.setCustomerOccupation(occupationCode);
				dspCommonTblCustomer.setCustomerDob(dob);
				dspCommonTblCustomer.setCustomerName(personalData.getName());
				dspCommonTblCustomer.setCustomerAddress1(personalData.getAddress1());
				dspCommonTblCustomer.setCustomerAddress2(personalData.getAddress2());
				dspCommonTblCustomer.setCustomerAddress3(personalData.getAddress3());
				dspCommonTblCustomer.setCustomerEmail(personalData.getEmail());
				dspCommonTblCustomer.setCustomerPostcode(personalData.getPostcode());
				dspCommonTblCustomer.setCustomerReligion(personalData.getReligion());
				dspCommonTblCustomer.setCustomerRace(personalData.getRace());
				dspCommonTblCustomer.setCustomerCountry(QQConstant.FORM_DEFAULT_COUNTRY_KEY);
				dspCommonTblCustomer.setCustomerNationality(QQConstant.FORM_DEFAULT_NATIONALITY);
				dspCommonTblCustomer.setCustomerMobileNo(personalData.getMobileNumber());

				if (gender.equalsIgnoreCase("f")) {
					dspCommonTblCustomer.setCustomerGender(QQConstant.FEMALE_CODE);
				} else {
					dspCommonTblCustomer.setCustomerGender(QQConstant.MALE_CODE);
				}

				Future<Integer> futureClaim = asyncService.getMDMClaimHistoryService(nric, url);
				Future<Long> futureTerrorist = dspMpTblTerroristService.getTerroristCount(nric);
				Future<Integer> futureInsertCustomer = dspCommonTblCustomerService
						.insertDspCommonTblCustomer(dspCommonTblCustomer);
				Long terroristCount = 0L;
				Integer claimCount = 0;

				claimCount = futureClaim.get();
				terroristCount = futureTerrorist.get();

				if (claimCount > 0) {
					updateRejection(session, qqIdKey, mpQQData, QQConstant.REJECT_CODE_CLAIM_HISTORY);
					response.sendRedirect(pageRejected);
					return;
				}

				if (terroristCount > 0) {
					updateRejection(session, qqIdKey, mpQQData, QQConstant.REJECT_CODE_TERRORIST);
					response.sendRedirect(pageRejected);
					return;
				}

				futureInsertCustomer.get();
				session.setAttribute(QQConstant.MP_QQ_DATA_SESSION_KEY, mpQQData);
				session.setAttribute(QQConstant.COMMON_CUSTOMER_DATA_SESSION_KEY, dspCommonTblCustomer);
				session.setAttribute(QQConstant.VPMS_REQUEST_SESSION_KEY, vpmsRequest);
				session.setAttribute(QQConstant.VPMS_RESPONSE_SESSION_KEY, vpmsResponseData);

			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			response.sendRedirect(servletContextPath + QQConstant.PAGE_URL_ERROR_EN);
			Thread.currentThread().interrupt();
		} finally {
			InputOutputStreamUtil.close(is);
		}

		response.sendRedirect(page);
	}

	private VpmsResponse assignToVpmsResponse(VpmsResponse vpmsResponse, VpmsResponseTakaful vpmsResponseTakaful) {
		VpmsResponseTakafulData vpmsResponseTakafulData = null;
		VpmsResponseData vpmsResponseData = null;
		if(vpmsResponseTakaful != null){
			vpmsResponse = new VpmsResponse();
			vpmsResponseData = new VpmsResponseData();
			vpmsResponseTakafulData = vpmsResponseTakaful.getData();
			
			vpmsResponse.setCode(vpmsResponseTakaful.getCode()); 
			vpmsResponse.setMessage(vpmsResponseTakaful.getMessage());
			vpmsResponse.setStatus(vpmsResponseTakaful.getStatus());
			
			vpmsResponseData.setVersion(vpmsResponseTakafulData.getVersion());
			vpmsResponseData.setQuotationDate(vpmsResponseTakafulData.getQuotationDate());
			vpmsResponseData.setPremiumPaymentTerm(vpmsResponseTakafulData.getPremiumPaymentTerm());
			vpmsResponseData.setMonthlyPremium(vpmsResponseTakafulData.getMonthlyPremium());
			vpmsResponseData.setAnnualPremium(vpmsResponseTakafulData.getAnnualPremium());
			vpmsResponseData.setName(vpmsResponseTakafulData.getName());
			vpmsResponseData.setGender(vpmsResponseTakafulData.getGender());
			vpmsResponseData.setSmoker(vpmsResponseTakafulData.getSmoker());
			vpmsResponseData.setOccupationClass(vpmsResponseTakafulData.getOccupationClass());
			vpmsResponseData.setNric(vpmsResponseTakafulData.getNric());
			vpmsResponseData.setDob(vpmsResponseTakafulData.getDob());
			vpmsResponseData.setAgeNextBirthday(vpmsResponseTakafulData.getAgeNextBirthday());
			vpmsResponseData.setPlanName(vpmsResponseTakafulData.getPlanName());
			vpmsResponseData.setPolicyTerm(vpmsResponseTakafulData.getPolicyTerm());
			vpmsResponseData.setPaymentTerm(vpmsResponseTakafulData.getPaymentTerm());
			vpmsResponseData.setPlanType(vpmsResponseTakafulData.getPlanType());
			vpmsResponseData.setDeductible(vpmsResponseTakafulData.getDeductible());
			vpmsResponseData.setFirstYearPremium(vpmsResponseTakafulData.getFirstYearPremium());
			vpmsResponseData.setPremiumMode(vpmsResponseTakafulData.getPremiumMode());
			vpmsResponseData.setAnnualPremiumArray(vpmsResponseTakafulData.getAnnualPremiumString());
			vpmsResponseData.setTotalPremiumToDate(vpmsResponseTakafulData.getTotalPremiumToDateString());
			vpmsResponseData.setEndCoveredAgeArray(vpmsResponseTakafulData.getEndCoveredAgeArrayInString());
			vpmsResponseData.setPlanBenefit(vpmsResponseTakafulData.getPlanBenefit());
			vpmsResponseData.setAnnualLimit(vpmsResponseTakafulData.getAnnualLimit());
			vpmsResponseData.setLifetimeLimit(vpmsResponseTakafulData.getLifetimeLimit());
			vpmsResponseData.setRoomRatePerDay(vpmsResponseTakafulData.getRoomRatePerDay());
			vpmsResponseData.setRoomAnnualLimit(vpmsResponseTakafulData.getRoomAnnualLimit());
			vpmsResponseData.setDeductibleOption(vpmsResponseTakafulData.getDeductibleOption());
			vpmsResponseData.setEndCoveredAgeAndWakalah(vpmsResponseTakafulData.getEndCoveredAgeAndWakalah());
			vpmsResponse.setData(vpmsResponseData);
		}
		return vpmsResponse;
	}

	@RequestMapping(value = QQConstant.REQUEST_MAPPING_QQ_ENDPOINT_TWO_A, method = RequestMethod.POST)
	public void insertFromQQ2a(@RequestParam String uw1, @RequestParam String uw2, @RequestParam String uw3,
			@RequestParam String weight, @RequestParam String height, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		DspMpTblQQ mpQQData = null;
		String page = servletContextPath + QQConstant.PAGE_URL_INSURACE_QQ2B_EN;
		String pageRejected = servletContextPath + QQConstant.PAGE_URL_INSURACE_QQ2A_EN;
		Map<String, Object> map = new HashMap<>();
		try {
			HttpSession session = request.getSession();
			mpQQData = (DspMpTblQQ) session.getAttribute(QQConstant.MP_QQ_DATA_SESSION_KEY);
			BigDecimal qqIdKey = (BigDecimal) session.getAttribute(QQConstant.DSP_QQ_ID_DATA_SESSION_KEY);
			String company = (String) session.getAttribute(QQConstant.COMPANY_SESSION_KEY);

			if (company.equals(QQConstant.TAKAFUL_COMPANY_KEY)) {
				page = servletContextPath + QQConstant.PAGE_URL_TAKAFUL_QQ2B_EN;
				pageRejected = servletContextPath + QQConstant.PAGE_URL_TAKAFUL_QQ2A_EN;
			}

			if (mpQQData != null && qqIdKey != null) {
				map = qqService.calculateBMI(request, weight, height);
				if (map.containsKey(QQConstant.MAP_KEY_IS_PASSED)
						&& ((Integer) map.get(QQConstant.MAP_KEY_IS_PASSED)) == 1) {
					DspMpTblQQ record = new DspMpTblQQ();
					DspMpTblQQExample example = new DspMpTblQQExample();
					example.createCriteria().andDspQqIdEqualTo(qqIdKey).andMpQqIdEqualTo(mpQQData.getMpQqId());
					record.setUnderWritingQuestion1(uw1);
					record.setUnderWritingQuestion2(uw2);
					record.setUnderWritingQuestion3(uw3);
					mpQQData.setUnderWritingQuestion1(uw1);
					mpQQData.setUnderWritingQuestion2(uw2);
					mpQQData.setUnderWritingQuestion3(uw3);
					mpQQData.setWeight(weight);
					mpQQData.setHeight(height);
					session.setAttribute(QQConstant.MP_QQ_DATA_SESSION_KEY, mpQQData);
					dspMPTblQQService.updateDspMpTblQQVoid(record, example);
				} else {
					updateRejection(session, qqIdKey, mpQQData, QQConstant.REJECT_CODE_BMI);
					response.sendRedirect(pageRejected);
					return;
				}
			} else {
				throw new NullPointerException(QQConstant.ERROR_MESSAGE_SESSION_EXPIRED);
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			response.sendRedirect(servletContextPath + QQConstant.PAGE_URL_ERROR_EN);
			Thread.currentThread().interrupt();
		}

		response.sendRedirect(page);
	}

	@RequestMapping(value = QQConstant.REQUEST_MAPPING_QQ_ENDPOINT_TWO_B, method = RequestMethod.POST)
	public void insertFromQQ2b(@RequestParam String name, @RequestParam String race, @RequestParam String religion,
			@RequestParam String hpno, @RequestParam String email, @RequestParam String salaryRange,
			@RequestParam String employerName, @RequestParam String address1, @RequestParam String address2,
			@RequestParam String address3, @RequestParam String postcode, @RequestParam String state,
			@RequestParam String addressMail1, @RequestParam String addressMail2, @RequestParam String addressMail3,
			@RequestParam String postcodeMail, @RequestParam String stateMail, @RequestParam String isMailOn,
			@RequestParam String usTaxPayerId, @RequestParam String nationality, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		DspMpTblQQ mpQQData = null;
		String page = servletContextPath + QQConstant.PAGE_URL_INSURACE_QQ3_EN;
		String pageRejected = servletContextPath + QQConstant.PAGE_URL_INSURACE_QQ2A_EN;

		try {
			HttpSession session = request.getSession();
			mpQQData = (DspMpTblQQ) session.getAttribute(QQConstant.MP_QQ_DATA_SESSION_KEY);
			BigDecimal qqIdKey = (BigDecimal) session.getAttribute(QQConstant.DSP_QQ_ID_DATA_SESSION_KEY);
			DspCommonTblCustomer dspCommonTblCustomer = (DspCommonTblCustomer) session
					.getAttribute(QQConstant.COMMON_CUSTOMER_DATA_SESSION_KEY);
			String company = (String) session.getAttribute(QQConstant.COMPANY_SESSION_KEY);

			if (company.equals(QQConstant.TAKAFUL_COMPANY_KEY)) {
				page = servletContextPath + QQConstant.PAGE_URL_TAKAFUL_QQ3_EN;
				pageRejected = servletContextPath + QQConstant.PAGE_URL_TAKAFUL_QQ2A_EN;
			}

			if (mpQQData != null && qqIdKey != null && dspCommonTblCustomer != null) {
				BigDecimal mpQQid = mpQQData.getMpQqId();
				BigDecimal dspCommonCustomerId = dspCommonTblCustomer.getCustomerId();

				nationality = CommonUtils.trimToNullParam(response, nationality);
				name = CommonUtils.trimToNullParam(response, name);
				race = CommonUtils.trimToNullParam(response, race);
				religion = CommonUtils.trimToNullParam(response, religion);
				hpno = CommonUtils.trimToNullParam(response, hpno);
				email = CommonUtils.trimToNullParam(response, email);
				salaryRange = CommonUtils.trimToNullParam(response, salaryRange);
				address1 = CommonUtils.trimToNullParam(response, address1);
				address2 = CommonUtils.trimToNullParam(response, address2);
				postcode = CommonUtils.trimToNullParam(response, postcode);
				state = CommonUtils.trimToNullParam(response, state);
				isMailOn = CommonUtils.trimToNullParam(response, isMailOn);
				if (isMailOn.equals(QQConstant.FORM_DEFAULT_IS_MAIL_ON_NO)) {
					addressMail1 = CommonUtils.trimToNullParam(response, addressMail1);
					addressMail2 = CommonUtils.trimToNullParam(response, addressMail2);
					postcodeMail = CommonUtils.trimToNullParam(response, postcodeMail);
					stateMail = CommonUtils.trimToNullParam(response, stateMail);
				}

				DspCommonTblQQ commonQQData = new DspCommonTblQQ();

				commonQQData.setCustomerId(dspCommonTblCustomer.getCustomerId());

				mpQQData.setMpQqId(null);
				mpQQData.setFullName(name);
				mpQQData.setUsaTaxPayerId(usTaxPayerId);
				mpQQData.setDropInQq(new BigDecimal(QQConstant.PAGE_NUMBER_3));

				dspCommonTblCustomer.setCustomerId(null);
				dspCommonTblCustomer.setUpdatedDate(new Date());
				dspCommonTblCustomer.setCustomerName(name);
				dspCommonTblCustomer.setCustomerAddress1(address1);
				dspCommonTblCustomer.setCustomerAddress2(address2);
				dspCommonTblCustomer.setCustomerAddress3(address3);
				dspCommonTblCustomer.setCustomerEmail(email);
				dspCommonTblCustomer.setCustomerPostcode(postcode);
				dspCommonTblCustomer.setCustomerState(state);
				dspCommonTblCustomer.setCustomerNationality(nationality);
				dspCommonTblCustomer.setCustomerReligion(religion);
				dspCommonTblCustomer.setCustomerRace(race);
				dspCommonTblCustomer.setCustomerMobileNo(hpno);
				dspCommonTblCustomer.setCustomerSalaryRange(salaryRange);
				dspCommonTblCustomer.setCustomerEmployer(employerName);
				dspCommonTblCustomer.setHomeMailCheck(StringUtils.substring(isMailOn, 0, 1));
				if (isMailOn.equals(QQConstant.FORM_DEFAULT_IS_MAIL_ON_NO)) {
					dspCommonTblCustomer.setCustomerMailAddress1(addressMail1);
					dspCommonTblCustomer.setCustomerMailAddress2(addressMail2);
					dspCommonTblCustomer.setCustomerMailAddress3(addressMail3);
					dspCommonTblCustomer.setCustomerMailPostcode(postcodeMail);
					dspCommonTblCustomer.setCustomerMailState(stateMail);
					dspCommonTblCustomer.setCustomerMailCountry(QQConstant.FORM_DEFAULT_COUNTRY_KEY);
				} else {
					dspCommonTblCustomer.setCustomerMailAddress1(address1);
					dspCommonTblCustomer.setCustomerMailAddress2(address2);
					dspCommonTblCustomer.setCustomerMailAddress3(address3);
					dspCommonTblCustomer.setCustomerMailPostcode(postcode);
					dspCommonTblCustomer.setCustomerMailState(state);
					dspCommonTblCustomer.setCustomerMailCountry(QQConstant.FORM_DEFAULT_COUNTRY_KEY);
				}

				DspCommonTblCustomerExample commonCustomerExample = new DspCommonTblCustomerExample();
				DspMpTblQQExample mpQQExample = new DspMpTblQQExample();
				DspCommonTblQQExample commonQQExample = new DspCommonTblQQExample();

				commonCustomerExample.createCriteria().andQqIdEqualTo(qqIdKey);
				mpQQExample.createCriteria().andDspQqIdEqualTo(qqIdKey);
				commonQQExample.createCriteria().andDspQqIdEqualTo(qqIdKey);

				Future<Integer> futureUpdateMpQQ = dspMPTblQQService.updateDspMpTblQQAsync(mpQQData, mpQQExample);
				Future<Integer> futureUpdateCommonQQ = dspCommonTblQQService.updateDspCommonTblQQ(commonQQData,
						commonQQExample);
				Future<Integer> futureUpdateCommonCustomer = dspCommonTblCustomerService
						.updateDspCommonTblCustomer(dspCommonTblCustomer, commonCustomerExample);

				Integer updateMpQQ = futureUpdateMpQQ.get();
				Integer updateCommonQQ = futureUpdateCommonQQ.get();
				Integer updateCommonCustomer = futureUpdateCommonCustomer.get();

				if (updateMpQQ != 1 || updateCommonQQ != 1 || updateCommonCustomer != 1) {
					throw new SQLException(QQConstant.ERROR_MESSAGE_UPDATE_FAILED);
				} else {
					mpQQData.setMpQqId(mpQQid);
					dspCommonTblCustomer.setCustomerId(dspCommonCustomerId);
					session.setAttribute(QQConstant.MP_QQ_DATA_SESSION_KEY, mpQQData);
					session.setAttribute(QQConstant.COMMON_CUSTOMER_DATA_SESSION_KEY, dspCommonTblCustomer);
				}

				if (nationality.equals(QQConstant.FORM_NATIONALITY_NON_MALAYSIAN)) {
					updateRejection(session, qqIdKey, mpQQData, QQConstant.REJECT_CODE_NATIONALITY);
					response.sendRedirect(pageRejected);
					return;
				}
			} else {
				throw new NullPointerException(QQConstant.ERROR_MESSAGE_SESSION_EXPIRED);
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			response.sendRedirect(servletContextPath + QQConstant.PAGE_URL_ERROR_EN);
			Thread.currentThread().interrupt();
		}

		response.sendRedirect(page);
	}

	@RequestMapping(value = QQConstant.REQUEST_MAPPING_QQ_ENDPOINT_ONE
			+ QQConstant.REQUEST_MAPPING_QQ_ENDPOINT_REJECTION, method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> insertRejectionQq1(@RequestParam String name, @RequestParam String email,
			@RequestParam String hpno, HttpSession session) {
		Map<String, Object> map = new HashMap<>();
		Integer isUpdateRejectionSuccess = 0;
		Integer isUpdateMpQqSuccess = 0;
		try {
			DspMpTblQQ mpQQData = (DspMpTblQQ) session.getAttribute(QQConstant.MP_QQ_DATA_SESSION_KEY);
			BigDecimal qqIdKey = (BigDecimal) session.getAttribute(QQConstant.DSP_QQ_ID_DATA_SESSION_KEY);
			if (mpQQData != null) {
				DspMpTblQQ dspMpTblQQLocal = new DspMpTblQQ();
				DspMpTblUwRejection dspMpTblUwRejection = new DspMpTblUwRejection();
				dspMpTblQQLocal.setMobileNumber(hpno);
				dspMpTblUwRejection.setCustomerName(name);
				dspMpTblUwRejection.setCustomerEmail(email);
				DspMpTblUwRejectionExample exampleDspMpTblUwRejection = new DspMpTblUwRejectionExample();
				exampleDspMpTblUwRejection.createCriteria().andCustomerNricEqualTo(mpQQData.getNewIcCopy())
						.andDspQqIdEqualTo(String.valueOf(qqIdKey));
				DspMpTblQQExample exampleDspMpTblQQ = new DspMpTblQQExample();
				exampleDspMpTblQQ.createCriteria().andMpQqIdEqualTo(mpQQData.getMpQqId()).andDspQqIdEqualTo(qqIdKey);

				Future<Integer> futureUpdateRejection = dspMpTblUwRejectionService
						.updateSelectiveAsync(dspMpTblUwRejection, exampleDspMpTblUwRejection);
				Future<Integer> futureUpdateMpQQ = dspMPTblQQService.updateDspMpTblQQAsync(dspMpTblQQLocal,
						exampleDspMpTblQQ);

				isUpdateRejectionSuccess = futureUpdateRejection.get();
				isUpdateMpQqSuccess = futureUpdateMpQQ.get();
			}
			if (isUpdateRejectionSuccess == 1 && isUpdateMpQqSuccess == 1) {
				CommonUtils.clearAllSession(session);
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			return new ResponseEntity<Map<String, Object>>(map, HttpStatus.BAD_GATEWAY);
		} finally {
			map.put(QQConstant.RESTFUL_STATUS_ISUPDATEREJECTION_KEY, isUpdateRejectionSuccess);
			map.put(QQConstant.RESTFUL_STATUS_ISUPDATEMPQQ_KEY, isUpdateMpQqSuccess);
		}

		return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
	}

	@RequestMapping(value = QQConstant.REQUEST_MAPPING_QQ_ENDPOINT_ONE
			+ QQConstant.REQUEST_MAPPING_QQ_ENDPOINT_REJECTION
			+ QQConstant.REQUEST_MAPPING_QQ_ENDPOINT_OCCUPATION, method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> insertRejectionQq1Occupation(@RequestParam String name,
			@RequestParam String email, @RequestParam String hpno, @RequestParam String dob,
			@RequestParam String gender, @RequestParam String occupation, HttpSession session) {
		Map<String, Object> map = new HashMap<>();
		Integer isInsertRejectionSuccess = 0;
		Integer isInsertMpQqSuccess = 0;
		Integer isInsertDSPCommonSuccess = 0;
		Integer isInsertCustCommonSuccess = 0;

		try {
			DspMpTblQQ dspMpTblQQLocal = new DspMpTblQQ();
			DspCommonTblQQ dspCommonTblQQ = new DspCommonTblQQ();
			DspMpTblUwRejection dspMpTblUwRejection = new DspMpTblUwRejection();
			DspCommonTblCustomer dspCommonTblCustomer = new DspCommonTblCustomer();

			dspMpTblUwRejection.setCustomerName(name);
			dspMpTblUwRejection.setCustomerEmail(email);

			Future<Integer> futureInsertDSPCommon = dspCommonTblQQService.insertDspCommonTblQQ(dspCommonTblQQ);
			isInsertDSPCommonSuccess = futureInsertDSPCommon.get();

			dspMpTblUwRejection.setDspQqId(String.valueOf(dspCommonTblQQ.getDspQqId()));
			dspMpTblUwRejection.setCustomerName(name);
			dspMpTblUwRejection.setCustomerEmail(email);
			dspMpTblUwRejection.setUwQsCode(QQConstant.REJECT_CODE_OCCUPATION);

			dspMpTblQQLocal.setDspQqId(dspCommonTblQQ.getDspQqId());
			dspMpTblQQLocal.setMobileNumber(hpno);
			dspMpTblQQLocal.setRejectedReason(QQConstant.REJECT_CODE_OCCUPATION);
			dspMpTblQQLocal.setDob(dob);

			dspCommonTblCustomer.setQqId(dspCommonTblQQ.getDspQqId());
			dspCommonTblCustomer.setCreateDate(new Date());
			dspCommonTblCustomer.setUpdatedDate(new Date());
			dspCommonTblCustomer.setCustomerDob(dob);

			Future<Integer> futureInsertRejection = dspMpTblUwRejectionService.insertRejection(dspMpTblUwRejection);
			Future<Integer> futureInsertMpQQ = dspMPTblQQService.insertDspMpTblQQ(dspMpTblQQLocal);
			Future<Integer> futureInsertCommonCust = dspCommonTblCustomerService
					.insertDspCommonTblCustomer(dspCommonTblCustomer);

			isInsertRejectionSuccess = futureInsertRejection.get();
			isInsertMpQqSuccess = futureInsertMpQQ.get();
			isInsertCustCommonSuccess = futureInsertCommonCust.get();

			CommonUtils.clearAllSession(session);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			Thread.currentThread().interrupt();
			return new ResponseEntity<Map<String, Object>>(map, HttpStatus.BAD_GATEWAY);
		} finally {
			map.put(QQConstant.RESTFUL_STATUS_ISINSERTDSPCOMMON_KEY, isInsertDSPCommonSuccess);
			map.put(QQConstant.RESTFUL_STATUS_ISINSERTREJECTION_KEY, isInsertRejectionSuccess);
			map.put(QQConstant.RESTFUL_STATUS_ISINSERTMPQQ_KEY, isInsertMpQqSuccess);
			map.put(QQConstant.RESTFUL_STATUS_ISINSERTCUSTCOMMON_KEY, isInsertCustCommonSuccess);
		}

		return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
	}

	@RequestMapping(value = QQConstant.REQUEST_MAPPING_QQ_ENDPOINT_TWO_A
			+ QQConstant.REQUEST_MAPPING_QQ_ENDPOINT_REJECTION_UNDERWRITING, method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> insertRejectionQq2aUnderwriting(@RequestParam String uw1,
			@RequestParam String uw2, @RequestParam String uw3, HttpSession session) {
		Map<String, Object> map = new HashMap<>();
		Integer isInsertRejection = 0;
		Integer isUpdateMpQqSuccess = 0;
		try {
			DspMpTblQQ mpQQData = (DspMpTblQQ) session.getAttribute(QQConstant.MP_QQ_DATA_SESSION_KEY);
			BigDecimal qqIdKey = (BigDecimal) session.getAttribute(QQConstant.DSP_QQ_ID_DATA_SESSION_KEY);
			if (mpQQData != null && qqIdKey != null) {

				DspMpTblQQ dspMpTblQQLocal = new DspMpTblQQ();
				DspMpTblUwRejection dspMpTblUwRejection = new DspMpTblUwRejection();
				dspMpTblQQLocal.setUnderWritingQuestion1(uw1);
				dspMpTblQQLocal.setUnderWritingQuestion2(uw2);
				dspMpTblQQLocal.setUnderWritingQuestion3(uw3);
				dspMpTblUwRejection.setDspQqId(String.valueOf(qqIdKey));
				dspMpTblUwRejection.setCustomerNric(mpQQData.getNewIcCopy());

				if (uw1.equals(QQConstant.UNDERWRITING_ANSWER_YES)) {
					dspMpTblQQLocal.setRejectedReason(QQConstant.REJECT_CODE_HEALTH_QUESTION_1);
					dspMpTblUwRejection.setUwQsCode(QQConstant.REJECT_CODE_HEALTH_QUESTION_1);
				} else if (uw2.equals(QQConstant.UNDERWRITING_ANSWER_YES)) {
					dspMpTblQQLocal.setRejectedReason(QQConstant.REJECT_CODE_HEALTH_QUESTION_2);
					dspMpTblUwRejection.setUwQsCode(QQConstant.REJECT_CODE_HEALTH_QUESTION_2);
				} else {
					dspMpTblQQLocal.setRejectedReason(QQConstant.REJECT_CODE_HEALTH_QUESTION_3);
					dspMpTblUwRejection.setUwQsCode(QQConstant.REJECT_CODE_HEALTH_QUESTION_3);
				}

				DspMpTblQQExample exampleDspMpTblQQ = new DspMpTblQQExample();
				exampleDspMpTblQQ.createCriteria().andMpQqIdEqualTo(mpQQData.getMpQqId()).andDspQqIdEqualTo(qqIdKey);

				Future<Integer> futureInsertRejection = dspMpTblUwRejectionService.insertRejection(dspMpTblUwRejection);
				Future<Integer> futureUpdateMpQQ = dspMPTblQQService.updateDspMpTblQQAsync(dspMpTblQQLocal,
						exampleDspMpTblQQ);

				isInsertRejection = futureInsertRejection.get();
				isUpdateMpQqSuccess = futureUpdateMpQQ.get();
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			Thread.currentThread().interrupt();
			return new ResponseEntity<Map<String, Object>>(map, HttpStatus.BAD_GATEWAY);
		} finally {
			map.put(QQConstant.RESTFUL_STATUS_ISINSERTREJECTION_KEY, isInsertRejection);
			map.put(QQConstant.RESTFUL_STATUS_ISUPDATEMPQQ_KEY, isUpdateMpQqSuccess);
		}

		return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
	}

	@RequestMapping(value = QQConstant.REQUEST_MAPPING_QQ_ENDPOINT_TWO_A
			+ QQConstant.REQUEST_MAPPING_QQ_ENDPOINT_REJECTION, method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> insertRejectionQq2a(@RequestParam String name,
			@RequestParam String email, @RequestParam String hpno, @RequestParam String uw1, @RequestParam String uw2,
			@RequestParam String uw3, HttpSession session) {
		Map<String, Object> map = new HashMap<>();
		Integer isInsertRejection = 0;
		Integer isUpdateMpQqSuccess = 0;
		try {
			DspMpTblQQ mpQQData = (DspMpTblQQ) session.getAttribute(QQConstant.MP_QQ_DATA_SESSION_KEY);
			BigDecimal qqIdKey = (BigDecimal) session.getAttribute(QQConstant.DSP_QQ_ID_DATA_SESSION_KEY);
			if (mpQQData != null && qqIdKey != null) {
				DspMpTblQQ dspMpTblQQLocal = new DspMpTblQQ();
				DspMpTblUwRejection dspMpTblUwRejection = new DspMpTblUwRejection();
				dspMpTblQQLocal.setMobileNumber(hpno);
				dspMpTblQQLocal.setUnderWritingQuestion1(uw1);
				dspMpTblQQLocal.setUnderWritingQuestion2(uw2);
				dspMpTblQQLocal.setUnderWritingQuestion3(uw3);
				if (uw1.equals(QQConstant.UNDERWRITING_ANSWER_YES)) {
					dspMpTblQQLocal.setRejectedReason(QQConstant.REJECT_CODE_HEALTH_QUESTION_1);
					dspMpTblUwRejection.setUwQsCode(QQConstant.REJECT_CODE_HEALTH_QUESTION_1);
				} else if (uw2.equals(QQConstant.UNDERWRITING_ANSWER_YES)) {
					dspMpTblQQLocal.setRejectedReason(QQConstant.REJECT_CODE_HEALTH_QUESTION_2);
					dspMpTblUwRejection.setUwQsCode(QQConstant.REJECT_CODE_HEALTH_QUESTION_2);
				} else {
					dspMpTblQQLocal.setRejectedReason(QQConstant.REJECT_CODE_HEALTH_QUESTION_3);
					dspMpTblUwRejection.setUwQsCode(QQConstant.REJECT_CODE_HEALTH_QUESTION_3);
				}
				dspMpTblUwRejection.setDspQqId(String.valueOf(qqIdKey));
				dspMpTblUwRejection.setCustomerNric(mpQQData.getNewIcCopy());
				dspMpTblUwRejection.setCustomerName(name);
				dspMpTblUwRejection.setCustomerEmail(email);
				DspMpTblQQExample exampleDspMpTblQQ = new DspMpTblQQExample();
				exampleDspMpTblQQ.createCriteria().andMpQqIdEqualTo(mpQQData.getMpQqId()).andDspQqIdEqualTo(qqIdKey);

				Future<Integer> futureUpdateRejection = dspMpTblUwRejectionService.insertRejection(dspMpTblUwRejection);
				Future<Integer> futureUpdateMpQQ = dspMPTblQQService.updateDspMpTblQQAsync(dspMpTblQQLocal,
						exampleDspMpTblQQ);

				isInsertRejection = futureUpdateRejection.get();
				isUpdateMpQqSuccess = futureUpdateMpQQ.get();
			}
			if (isInsertRejection == 1 && isUpdateMpQqSuccess == 1) {
				CommonUtils.clearAllSession(session);
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			Thread.currentThread().interrupt();
			return new ResponseEntity<Map<String, Object>>(map, HttpStatus.BAD_GATEWAY);
		} finally {
			map.put(QQConstant.RESTFUL_STATUS_ISINSERTREJECTION_KEY, isInsertRejection);
			map.put(QQConstant.RESTFUL_STATUS_ISUPDATEMPQQ_KEY, isUpdateMpQqSuccess);
		}

		return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
	}

	@RequestMapping(value = QQConstant.REQUEST_MAPPING_QQ_ENDPOINT_FOUR
			+ QQConstant.REQUEST_MAPPING_QQ_ENDPOINT_BANK_ACCOUNT, method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> insertBankAccountNumber(@RequestParam String bank,
			@RequestParam String accountNumber, HttpSession session) {
		Map<String, Object> map = new HashMap<>();
		Integer isUpdateMpQqSuccess = 0;
		String message = null;
		String code = QQConstant.RESTFUL_STATUS_CODE_FAILED;
		try {
			DspMpTblQQ mpQQData = (DspMpTblQQ) session.getAttribute(QQConstant.MP_QQ_DATA_SESSION_KEY);
			BigDecimal qqIdKey = (BigDecimal) session.getAttribute(QQConstant.DSP_QQ_ID_DATA_SESSION_KEY);
			List<DspCommonTblBankList> bankList = CommonUtils
					.castList(session.getAttribute(QQConstant.BANK_LIST_SESSION_KEY), DspCommonTblBankList.class);
			bank = StringUtils.trimToNull(bank);
			accountNumber = StringUtils.trimToNull(accountNumber);

			if (bankList != null && bank != null && accountNumber != null) {
				DspCommonTblBankList bankDetails = findByBankKey(bankList, bank);
				if (mpQQData != null && qqIdKey != null) {
					String[] accountNumberLengths = bankDetails.getBankAccountNumberLengthArray();
					boolean isMatch = isAccountNumberLengthMatch(accountNumberLengths, accountNumber.length());
					if (isMatch == true) {
						DspMpTblQQ dspMpTblQQLocal = new DspMpTblQQ();
						dspMpTblQQLocal.setBankCode(bank);
						dspMpTblQQLocal.setAccountNumber(accountNumber);
						DspMpTblQQExample exampleDspMpTblQQ = new DspMpTblQQExample();
						exampleDspMpTblQQ.createCriteria().andMpQqIdEqualTo(mpQQData.getMpQqId())
								.andDspQqIdEqualTo(qqIdKey);
						Future<Integer> futureUpdateMpQQ = dspMPTblQQService.updateDspMpTblQQAsync(dspMpTblQQLocal,
								exampleDspMpTblQQ);
						isUpdateMpQqSuccess = futureUpdateMpQQ.get();
						code = QQConstant.RESTFUL_STATUS_CODE_SUCCESS;
						message = QQConstant.RESTFUL_STATUS_MESSAGE_SUCCESS_SAVED;
					} else {
						code = QQConstant.RESTFUL_STATUS_CODE_FAILED;
						message = QQConstant.RESTFUL_STATUS_MESSAGE_INCORRECT_ACCOUNT_NUMBER_LENGTH;
					}
				} else {
					code = QQConstant.RESTFUL_STATUS_CODE_FAILED;
					message = QQConstant.RESTFUL_STATUS_MESSAGE_SESSION_NULL;
				}
			} else {
				code = QQConstant.RESTFUL_STATUS_CODE_FAILED;
				message = QQConstant.RESTFUL_STATUS_MESSAGE_BANK_DETAILS_NULL;
			}
		} catch (Exception ex) {
			message = ex.getMessage();
			logger.error(ex.getMessage(), ex);
			Thread.currentThread().interrupt();
			return new ResponseEntity<Map<String, Object>>(map, HttpStatus.BAD_GATEWAY);
		} finally {
			map.put(QQConstant.RESTFUL_STATUS_ISUPDATEMPQQ_KEY, isUpdateMpQqSuccess);
			map.put(QQConstant.RESTFUL_STATUS_CODE_KEY, code);
			map.put(QQConstant.RESTFUL_STATUS_MESSAGE_KEY, message);

		}

		return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
	}

	private void updateRejection(HttpSession session, BigDecimal qqIdKey, DspMpTblQQ mpQQData, String rejectedReason) {
		try {
			session.setAttribute(QQConstant.IS_REJECTED_SESSION_KEY, true);
			DspMpTblQQ dspMpTblQQ = new DspMpTblQQ();
			DspMpTblUwRejection dspMpTblUwRejection = new DspMpTblUwRejection();
			DspMpTblQQExample exampleMpTblQQ = new DspMpTblQQExample();
			exampleMpTblQQ.createCriteria().andDspQqIdEqualTo(qqIdKey).andMpQqIdEqualTo(mpQQData.getMpQqId());
			dspMpTblQQ.setRejectedReason(rejectedReason);
			dspMpTblUwRejection.setCustomerNric(mpQQData.getNewIcCopy());
			dspMpTblUwRejection.setUwQsCode(rejectedReason);
			dspMpTblUwRejection.setDspQqId(String.valueOf(qqIdKey));
			dspMPTblQQService.updateDspMpTblQQVoid(dspMpTblQQ, exampleMpTblQQ);
			dspMpTblUwRejectionService.insertRejectionVoid(dspMpTblUwRejection);
		} catch (IOException ex) {
			logger.error(ex.getMessage(), ex);
			Thread.currentThread().interrupt();
		}
	}

	private DspCommonTblBankList findByBankKey(List<DspCommonTblBankList> bankList, String bankKey) {
		DspCommonTblBankList dspCommonTblBankList = new DspCommonTblBankList();
		for (DspCommonTblBankList record : bankList) {
			if ((record.getBankKey()).equals(bankKey) == true) {
				dspCommonTblBankList = record;
			}
		}
		return dspCommonTblBankList;
	}

	private boolean isAccountNumberLengthMatch(String[] accountNumberLengths, int accountNumberLength) {
		boolean isMatch = false;
		try {
			for (String length : accountNumberLengths) {
				if (NumberUtils.toInt(length) == accountNumberLength) {
					isMatch = true;
					break;
				}
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return isMatch;
	}

	@RequestMapping(value = "/session", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> getSession(HttpSession session) {
		Map<String, Object> map = new HashMap<>();
		DspMpTblQQ mpQQData = null;
		BigDecimal qqIdKey = null;
		String company = null;
		VpmsRequest vpmsRequest = null;
		VpmsResponseData vpmsResponseData = null;
		DspCommonTblCustomer dspCommonTblCustomer = null;
		PaymentResponse paymentResponse = null;
		List<DspTlTblOcc> occupations = null;

		try {
			mpQQData = (DspMpTblQQ) session.getAttribute(QQConstant.MP_QQ_DATA_SESSION_KEY);
			qqIdKey = (BigDecimal) session.getAttribute(QQConstant.DSP_QQ_ID_DATA_SESSION_KEY);
			company = (String) session.getAttribute(QQConstant.COMPANY_SESSION_KEY);
			vpmsRequest = (VpmsRequest) session.getAttribute(QQConstant.VPMS_REQUEST_SESSION_KEY);
			vpmsResponseData = (VpmsResponseData) session.getAttribute(QQConstant.VPMS_RESPONSE_SESSION_KEY);
			dspCommonTblCustomer = (DspCommonTblCustomer) session
					.getAttribute(QQConstant.COMMON_CUSTOMER_DATA_SESSION_KEY);
			paymentResponse = (PaymentResponse) session.getAttribute(QQConstant.PAYMENT_RESPONSE_SESSION_KEY);
			occupations = CommonUtils.castList(session.getAttribute(QQConstant.OCCUPATION_SESSION_KEY),
					DspTlTblOcc.class);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		} finally {
			map.put("DspMpTblQQ", mpQQData);
			map.put("QqIdKey", qqIdKey);
			map.put("Company", company);
			map.put("VpmsRequest", vpmsRequest);
			map.put("VpmsResponseData", vpmsResponseData);
			map.put("DspCommonTblCustomer", dspCommonTblCustomer);
			map.put("PaymentResponse", paymentResponse);
			map.put("DspTlTblOcc", occupations);
		}
		return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
	}

}
