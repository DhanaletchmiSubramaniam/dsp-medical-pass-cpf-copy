package com.etiqa.medicalpass.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.soap.SOAPMessage;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.joda.time.DateTime;
import org.joda.time.Years;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.etiqa.medicalpass.constant.QQConstant;
import com.google.gson.JsonObject;

public class CommonUtils {

	private static final Logger logger = LoggerFactory.getLogger(CommonUtils.class);

	public static Integer getAge(String dateOfBirth, String dateFormat) {
		Integer age = -1;
		try {
			if (StringUtils.trimToNull(dateOfBirth) != null && StringUtils.trimToNull(dateFormat) != null) {
				DateTimeFormatter formatter = DateTimeFormat.forPattern(dateFormat);
				DateTime dtDateOfBirth = formatter.parseDateTime(dateOfBirth);
				DateTime dtCurrent = new DateTime();
				Years yearsAge = Years.yearsBetween(dtDateOfBirth, dtCurrent);
				age = yearsAge.getYears();
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return age;
	}

	public static Integer verifyIcWithBirthDate(String dateOfBirth, String dateFormat, String strNric) {
		Integer isVerified = 0;
		try {
			if (StringUtils.trimToNull(dateOfBirth) != null && StringUtils.trimToNull(dateFormat) != null
					&& StringUtils.trimToNull(strNric) != null) {
				strNric = StringUtils.substring(strNric, 0, 6);
				DateTimeFormatter formatterDob = DateTimeFormat.forPattern(dateFormat);
				DateTimeFormatter formatterIc = DateTimeFormat.forPattern("yyMMdd");
				DateTime dtDateOfBirth = formatterDob.parseDateTime(dateOfBirth);
				DateTime dtNric = formatterIc.parseDateTime(strNric);
				if (dtDateOfBirth.getMillis() == dtNric.getMillis()) {
					isVerified = 1;
				}
			}

		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return isVerified;
	}

	public static String logHttpRequest(HttpServletRequest request) {
		JsonObject jsonObject = new JsonObject();
		try {
			jsonObject.addProperty("url", request.getRequestURI());
			jsonObject.addProperty("method", request.getMethod());
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return jsonObject.toString();
	}

	public static String formatPremium(String premium) {
		DecimalFormat formatter = null;
		String convertedPremium = null;
		try {
			formatter = new DecimalFormat(QQConstant.PREMIUM_NUMBER_FORMAT);
			convertedPremium = formatter.format(NumberUtils.toDouble(premium));
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return convertedPremium;
	}

	public static String formatPremium(double premium) {
		DecimalFormat formatter = null;
		String convertedPremium = null;
		try {
			formatter = new DecimalFormat(QQConstant.PREMIUM_NUMBER_FORMAT);
			convertedPremium = formatter.format(premium);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return convertedPremium;
	}

	public static String formatCoverage(String premium) {
		DecimalFormat formatter = null;
		String convertedPremium = null;
		try {
			formatter = new DecimalFormat(QQConstant.SUM_COVERAGE_NUMBER_FORMAT);
			convertedPremium = formatter.format(NumberUtils.toDouble(premium));
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return convertedPremium;
	}

	public static String formatBMI(String premium) {
		DecimalFormat formatter = null;
		String convertedPremium = null;
		try {
			formatter = new DecimalFormat(QQConstant.BMI_DOUBLE_FORMAT);
			convertedPremium = formatter.format(NumberUtils.toDouble(premium));
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return convertedPremium;
	}

	public static void clearSession(String sessionName, HttpSession session) {
		if (session.getAttribute(sessionName) != null) {
			session.removeAttribute(sessionName);
		}
	}

	public static void clearAllSession(HttpSession session) {
		String[] keysArray = QQConstant.CACHE_KEYS;
		for (int i = 0; i < keysArray.length; i++) {
			if (session.getAttribute(keysArray[i]) != null) {
				session.removeAttribute(keysArray[i]);
			}
		}
	}

	public static void clearAllSessionExceptRejected(HttpSession session) {
		String[] keysArray = QQConstant.CACHE_KEYS;
		for (int i = 0; i < keysArray.length; i++) {
			if (session.getAttribute(keysArray[i]) != null) {
				session.removeAttribute(keysArray[i]);
			}
		}
	}

	public static String uppercaseFirstCharacter(String value) {
		char[] array = value.toCharArray();
		array[0] = Character.toUpperCase(array[0]);
		return new String(array);
	}

	public static String determineGenderFromIC(String ic) {
		String gender = "";
		String lastStr = "";

		if (StringUtils.isNotBlank(ic)) {
			lastStr = ic.substring(ic.length() - 1);
			logger.info("IC: {} - Last letter: {}", ic, lastStr);

			int digit = Integer.valueOf(lastStr);
			if (digit % 2 == 0) {
				gender = QQConstant.GENDER_FEMALE;
			} else {
				gender = QQConstant.GENDER_MALE;
			}
		}

		return gender;
	}

	public static String formatPdfDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy  h:mm:ss a");
		return sdf.format(new Date());
	}

	public static String formatPdfDateOnly() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		return sdf.format(new Date());
	}

	public static String receiptDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy h:mm:ss");
		return sdf.format(new Date());
	}

	public static String formatVPMSDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(new Date());
	}

	public static String formatOracleDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		return sdf.format(new Date());
	}

	public static String formatSiDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		return sdf.format(new Date());
	}

	public static String formatDobToSiDate(String dateOfBirth, String dateFormat) {
		DateTimeFormatter formatterSiDate = DateTimeFormat.forPattern("dd-MMM-yyyy");
		DateTime dtDateOfBirth = new DateTime();
		try {
			if (StringUtils.trimToNull(dateOfBirth) != null && StringUtils.trimToNull(dateFormat) != null) {
				DateTimeFormatter formatter = DateTimeFormat.forPattern(dateFormat);
				dtDateOfBirth = formatter.parseDateTime(dateOfBirth);
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return formatterSiDate.print(dtDateOfBirth);
	}

	public static String formatDOBForVPMS(String dateOfBirth, String dateFormat) {
		String vpmsDob = null;
		try {
			if (StringUtils.trimToNull(dateOfBirth) != null && StringUtils.trimToNull(dateFormat) != null) {
				DateTimeFormatter formatterDob = DateTimeFormat.forPattern(dateFormat);
				DateTimeFormatter formatterVPMS = DateTimeFormat.forPattern("yyyy-MM-dd");
				DateTime dtDateOfBirth = formatterDob.parseDateTime(dateOfBirth);
				vpmsDob = formatterVPMS.print(dtDateOfBirth);
			}

		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return vpmsDob;
	}

	public static void writeToBaos(SOAPMessage request) {
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			request.writeTo(baos);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
	}

	public static String getProductCode(String selectedPackage, String company) {
		String productCode = null;
		if (company.equals(QQConstant.INSURANCE_COMPANY_KEY)) {
			if (selectedPackage.equals(QQConstant.PRODUCT_CODE_SILVER_KEY)) {
				productCode = QQConstant.INSURANCE_PRODUCT_CODE_SILVER;
			} else if (selectedPackage.equals(QQConstant.PRODUCT_CODE_GOLD_KEY)) {
				productCode = QQConstant.INSURANCE_PRODUCT_CODE_GOLD;
			} else {
				productCode = QQConstant.INSURANCE_PRODUCT_CODE_PLATINUM;
			}
		} else {
			if (selectedPackage.equals(QQConstant.PRODUCT_CODE_SILVER_KEY)) {
				productCode = QQConstant.TAKAFUL_PRODUCT_CODE_SILVER;
			} else if (selectedPackage.equals(QQConstant.PRODUCT_CODE_GOLD_KEY)) {
				productCode = QQConstant.TAKAFUL_PRODUCT_CODE_GOLD;
			} else {
				productCode = QQConstant.TAKAFUL_PRODUCT_CODE_PLATINUM;
			}
		}
		return productCode;
	}

	public static String getProductNameFromProductCode(String productCode) {
		String productName = null;
		if (productCode.equals(QQConstant.INSURANCE_PRODUCT_CODE_SILVER)
				|| productCode.equals(QQConstant.INSURANCE_PRODUCT_CODE_GOLD)
				|| productCode.equals(QQConstant.INSURANCE_PRODUCT_CODE_PLATINUM)) {
			productName = QQConstant.PRODUCT_NAME_INSURANCE;
		} else {
			productName = QQConstant.PRODUCT_NAME_TAKAFUL;
		}
		return productName;
	}

	public static String getProductNameFromCompany(String company) {
		String productName = null;
		if (company.equals(QQConstant.INSURANCE_COMPANY_KEY)) {
			productName = QQConstant.PRODUCT_NAME_INSURANCE;
		} else {
			productName = QQConstant.PRODUCT_NAME_TAKAFUL;
		}
		return productName;
	}

	public static boolean hasPurchasedBeforeFromAnotherCompany(String company, String productCode) {
		Boolean hasPurchasedBefore = false;
		if (company.equals(QQConstant.TAKAFUL_COMPANY_KEY)) {
			if (StringUtils.contains(productCode, QQConstant.INSURANCE_PRODUCT_CODE_SEARCH_KEY_STRING)) {
				hasPurchasedBefore = true;
			}
		} else {
			if (StringUtils.contains(productCode, QQConstant.TAKAFUL_PRODUCT_CODE_SEARCH_KEY_STRING)) {
				hasPurchasedBefore = true;
			}
		}
		return hasPurchasedBefore;
	}

	public static String getPlanNameFromProductCode(String productCode) {
		String planType = null;
		if (StringUtils.contains(productCode, QQConstant.PRODUCT_CODE_GOLD_SEARCH_KEY)) {
			planType = QQConstant.PLAN_TYPE_GOLD;
		} else if (StringUtils.contains(productCode, QQConstant.PRODUCT_CODE_SILVER_KEY)) {
			planType = QQConstant.PLAN_TYPE_SILVER;
		} else {
			planType = QQConstant.PLAN_TYPE_PLATINUM;
		}
		return planType;
	}

	public static String getFormCodePostFixFromProductCode(String productCode) {
		String planType = null;
		if (StringUtils.contains(productCode, QQConstant.PRODUCT_CODE_GOLD_SEARCH_KEY)) {
			planType = QQConstant.FORM_NO_CODE_GOLD;
		} else if (StringUtils.contains(productCode, QQConstant.PRODUCT_CODE_SILVER_KEY)) {
			planType = QQConstant.FORM_NO_CODE_SILVER;
		} else {
			planType = QQConstant.FORM_NO_CODE_PLATINUM;
		}
		return planType;
	}

	public static String trimToNullParam(HttpServletResponse response, String param) throws IOException {
		param = StringUtils.trimToNull(param);
		if (param == null) {
			response.sendRedirect(QQConstant.PAGE_URL_ERROR_EN);
		}
		return param;
	}

	public static <T> List<T> castList(Object obj, Class<T> clazz) {
		List<T> result = new ArrayList<T>();
		if (obj instanceof List<?>) {
			for (Object o : (List<?>) obj) {
				result.add(clazz.cast(o));
			}
			return result;
		}
		return null;
	}

	public static String getCompanyFromPolicyNo(String invoiceNo) {
		String company = null;
		if (StringUtils.contains(invoiceNo, QQConstant.INSURANCE_POLICY_PREFIX_SEARCH_KEY)) {
			company = QQConstant.INSURANCE_COMPANY_KEY;
		} else {
			company = QQConstant.TAKAFUL_COMPANY_KEY;
		}
		return company;
	}

	public static String getNationalityFromCode(String code) {
		String nationality = null;
		if (StringUtils.contains(code, QQConstant.NATIONALITY_SEARCH_KEY_MALAYSIAN)) {
			nationality = QQConstant.NATIONALITY_VALUE_MALAYSIAN;
		} else {
			nationality = QQConstant.NATIONALITY_VALUE_NON_MALAYSIAN;
		}
		return nationality;
	}

	public static String getDeductionOptionString(String deductible) {
		String deductionOption = null;
		DecimalFormat formatter = null;
		if (deductible.equalsIgnoreCase("0")) {
			deductionOption = "No - 100% of the eligible medical bill paid by Etiqa";
		} else {
			formatter = new DecimalFormat(QQConstant.SUM_COVERAGE_NUMBER_FORMAT);
			deductible = formatter.format(NumberUtils.toDouble(deductible));
			deductionOption = "Yes - Deductible limit: RM" + deductible + " per hospitalisation";
		}
		return deductionOption;
	}

	public static String getDeductionAmountFromString(String deductible) {
		String deductionOption = null;
		DecimalFormat formatter = null;
		if (deductible.equalsIgnoreCase("0")) {
			deductionOption = "Full Coverage";
		} else {
			formatter = new DecimalFormat(QQConstant.SUM_COVERAGE_NUMBER_FORMAT);
			deductible = formatter.format(NumberUtils.toDouble(deductible));
			deductionOption = "RM" + deductible + " Deductible";
		}
		return deductionOption;
	}

	public static void checkCompanyBySession(HttpServletResponse response, String companySession, String fromController)
			throws IOException {
		companySession = StringUtils.trimToNull(companySession);
		if (companySession == null) {
			if (fromController.equals(QQConstant.INSURANCE_COMPANY_KEY)) {
				response.sendRedirect(QQConstant.REQUEST_MAPPING_QQ_PREFIX + QQConstant.PAGE_URL_INSURACE_QQ1_EN);
			} else {
				response.sendRedirect(QQConstant.REQUEST_MAPPING_QQ_PREFIX + QQConstant.PAGE_URL_TAKAFUL_QQ1_EN);
			}
		}else{
			if (companySession.equals(fromController) == false) {
				if (fromController.equals(QQConstant.INSURANCE_COMPANY_KEY)) {
					response.sendRedirect(QQConstant.REQUEST_MAPPING_QQ_PREFIX + QQConstant.PAGE_URL_INSURACE_QQ1_EN);
				} else {
					response.sendRedirect(QQConstant.REQUEST_MAPPING_QQ_PREFIX + QQConstant.PAGE_URL_TAKAFUL_QQ1_EN);
				}
			}
		}
	}

}
