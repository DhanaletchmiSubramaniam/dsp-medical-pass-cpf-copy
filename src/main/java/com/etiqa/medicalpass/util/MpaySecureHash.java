package com.etiqa.medicalpass.util;



import java.security.MessageDigest;

public class MpaySecureHash {

	// String secureHash = reqSecureHashValue(hashKey, mid, invno,
	// amt).toUpperCase();

	public static String reqSecureHashValue(String hashKey, String mid, String invno, String amt) {

		String tmpAmt = leftPaddedString(amt.replace(".", ""), 12, '0');

		String hashValues = new StringBuffer(hashKey).append("Continue").append(mid).append(invno).append(tmpAmt)
				.toString();

		String secureHash = sha256(hashValues);

		return secureHash;
	}

	public static String sha256(String base) {
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] hash = digest.digest(base.getBytes("UTF-8"));
			StringBuffer hexString = new StringBuffer();

			for (int i = 0; i < hash.length; i++) {
				String hex = Integer.toHexString(0xff & hash[i]);
				if (hex.length() == 1) {
					hexString.append('0');
				}
				hexString.append(hex);
			}

			return hexString.toString();
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	public static String leftPaddedString(String str, Integer requiredLength, char appendVal) {
		String result = "";
		StringBuilder sb = new StringBuilder();

		for (int toPrepend = requiredLength - str.length(); toPrepend > 0; toPrepend--) {
			sb.append(appendVal);
		}

		sb.append(str);
		result = sb.toString();

		// System.out.println("result : "+result);

		return result;
	}

}
