/**
 * 
 */
package com.etiqa.medicalpass.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PasswordUtils {

	private static final Logger logger = LoggerFactory.getLogger(PasswordUtils.class);

	public static String generatePasswordForPdf(String ic, String dob) {
		logger.info("generatePasswordForPdf");

		if (StringUtils.isNoneBlank(ic) && StringUtils.isNoneBlank(dob)) {
			dob = dob.replace("/", "");
			if (dob.length() == 7) {
				dob = "0" + dob;
			}
			String lastDigitnric = ic.substring(ic.length() - 4);
			return (dob + lastDigitnric);
		} else {
			logger.error("IC or DOB is null");
		}

		return "";
	}
}
