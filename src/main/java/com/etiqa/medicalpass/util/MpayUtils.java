package com.etiqa.medicalpass.util;

import java.security.MessageDigest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.etiqa.medicalpass.constant.QQConstant;

public class MpayUtils {

	private static final Logger logger = LoggerFactory.getLogger(CommonUtils.class);

	public static String requestSecureHashValue(String mid, String invno, String amt, String company) {
		if (company.equals(QQConstant.INSURANCE_COMPANY_KEY)) {
			String hashKey = QQConstant.MPAY_HASH_KEY_INSURANCE;
			String tmpAmt = leftPaddedString(amt.replace(".", ""), 12, '0');
			String hashValues = new StringBuffer(hashKey).append(QQConstant.PAYMENT_GATEWAY_HASH_KEY_CONTINUE)
					.append(mid).append(invno).append(tmpAmt).toString();
			return sha256(hashValues);
		} else {
			String hashKey = QQConstant.MPAY_HASH_KEY_TAKAFUL;
			String tmpAmt = leftPaddedString(amt.replace(".", ""), 12, '0');
			String hashValues = new StringBuffer(hashKey).append(QQConstant.PAYMENT_GATEWAY_HASH_KEY_CONTINUE)
					.append(mid).append(invno).append(tmpAmt).toString();
			return sha256(hashValues);
		}
	}

	public static String responseSecureHashValue(String result, String company) {
		if (company.equals(QQConstant.INSURANCE_COMPANY_KEY)) {
			String hashKey = QQConstant.MPAY_HASH_KEY_INSURANCE;
			String hashValues = new StringBuffer(hashKey).append(QQConstant.PAYMENT_GATEWAY_HASH_KEY_CONTINUE)
					.append(result).toString();
			return sha256(hashValues);
		} else {
			String hashKey = QQConstant.MPAY_HASH_KEY_TAKAFUL;
			String hashValues = new StringBuffer(hashKey).append(QQConstant.PAYMENT_GATEWAY_HASH_KEY_CONTINUE)
					.append(result).toString();
			return sha256(hashValues);
		}
	}

	public static String sha256(String base) {
		StringBuilder hexString = new StringBuilder();
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] hash = digest.digest(base.getBytes("UTF-8"));
			for (int i = 0; i < hash.length; i++) {
				String hex = Integer.toHexString(0xff & hash[i]);
				if (hex.length() == 1) {
					hexString.append('0');
				}
				hexString.append(hex);
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return hexString.toString();
	}

	public static String leftPaddedString(String str, Integer requiredLength, char appendVal) {
		String result = "";
		StringBuilder sb = new StringBuilder();
		for (int toPrepend = requiredLength - str.length(); toPrepend > 0; toPrepend--) {
			sb.append(appendVal);
		}
		sb.append(str);
		result = sb.toString();
		return result;
	}

}
