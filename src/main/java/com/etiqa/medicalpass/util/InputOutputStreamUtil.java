package com.etiqa.medicalpass.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;
import java.sql.CallableStatement;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;

import net.sf.jasperreports.export.OutputStreamExporterOutput;

public class InputOutputStreamUtil {

	private static final Logger logger = LoggerFactory.getLogger(InputOutputStreamUtil.class);

	private InputOutputStreamUtil() {
		throw new IllegalStateException("Utility class");
	}

	public static void close(InputStream inputStream) {
		if (inputStream != null) {
			try {
				inputStream.close();
			} catch (IOException ex) {
				logger.error(ex.getMessage(), ex);
			}
		}
	}

	public static void close(OutputStream outputStream) {
		if (outputStream != null) {
			try {
				outputStream.close();
			} catch (IOException ex) {
				logger.error(ex.getMessage(), ex);
			}
		}
	}

	public static void close(Writer writer) {
		if (writer != null) {
			try {
				writer.close();
			} catch (IOException ex) {
				logger.error(ex.getMessage(), ex);
			}
		}
	}

	public static void close(CallableStatement callableStatement) {
		if (callableStatement != null) {
			try {
				callableStatement.close();
			} catch (SQLException ex) {
				logger.error(ex.getMessage(), ex);
			}
		}
	}

	public static void close(OutputStreamExporterOutput outputStreamExporterOutput) {
		if (outputStreamExporterOutput != null) {
			try {
				outputStreamExporterOutput.close();
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
			}
		}
	}

	public static void close(PdfReader pdfReader) {
		if (pdfReader != null) {
			try {
				pdfReader.close();
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
			}
		}
	}

	public static void close(PdfStamper pdfStamper) {
		if (pdfStamper != null) {
			try {
				pdfStamper.close();
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
			}
		}
	}

}
