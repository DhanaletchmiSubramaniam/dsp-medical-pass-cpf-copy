package com.etiqa.medicalpass.util;

import java.util.ArrayList;
import java.util.List;

public class ExceptionUtils {

	public static String[] toArrayString(Exception ex) {
		List<String> elementList = new ArrayList<>();
		if (ex != null) {
			elementList.add(ex.getClass().getCanonicalName());
			for (StackTraceElement element : ex.getStackTrace()) {
				elementList.add(element.toString());
			}
		}
		return elementList.toArray(new String[elementList.size()]);
	}

}
