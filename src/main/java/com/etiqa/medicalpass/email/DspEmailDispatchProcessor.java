package com.etiqa.medicalpass.email;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
//import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
//import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.etiqa.medicalpass.pojo.DspCommonTblCustomer;
import com.etiqa.medicalpass.pojo.DspMpTblQQ;
import com.etiqa.medicalpass.pojo.GeneratingReportsResponseVo;
import com.etiqa.medicalpass.pojo.PaymentResponse;
import com.etiqa.medicalpass.service.ProductMailTemplateService;

public class DspEmailDispatchProcessor {

	static Properties mailServerProperties;
	static Session getMailSession;
	static MimeMessage generateMailMessage;
	private static Properties activeDirMailProp = new Properties();
	ProductMailTemplateService emailService = null;

	private static final Logger logger = LoggerFactory.getLogger(DspEmailDispatchProcessor.class);

	public DspEmailDispatchProcessor(ProductMailTemplateService emailService) {
		this.emailService = emailService;
	}

	private String dispatchDspEmail(DspEmailTemplateBean template, List<GeneratingReportsResponseVo> fileNamelist,
			String policyNo) {

		String res = null;

		String host = activeDirMailProp.getProperty("emailhost");
		String fromemail = activeDirMailProp.getProperty("from");
		String port = activeDirMailProp.getProperty("emailport");

		logger.error("host: {}", host);
		logger.error("fromemail: {}", fromemail);
		logger.error("port: {}", port);


		Properties props = new Properties();
		props.put("mail.smtp.auth", "false");
		props.put("mail.smtp.starttls.enable", "false");
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", port);
		props.put("mail.smtp.ssl.trust", host);

		try {

			// Get the default Session object.
			Session session = Session.getDefaultInstance(props);

			// InternetAddress.parse(emailTo)
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(fromemail));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(template.getEmailTo()));
			message.setSubject(template.getTemplateSubject());
			message.setContent(template.getTemplateBody(), "text/html");

			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setText(template.getTemplateBody());
			messageBodyPart.setContent(template.getTemplateBody(), "text/html");

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			String filePath = activeDirMailProp.getProperty("filepath");

			addAttachment(multipart, filePath + policyNo + ".zip", policyNo + ".zip");

			message.setContent(multipart);
			Transport.send(message);
			res = "Done";
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			res = "Failed";
		}
		return res;
	}

	/**
	 * 
	 * @param multipart
	 * @param filename
	 * @param singleFileName
	 * @throws MessagingException
	 */
	private static void addAttachment(Multipart multipart, String filename, String singleFileName)
			throws MessagingException {
		DataSource source = new FileDataSource(filename);
		BodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setDataHandler(new DataHandler(source));
		messageBodyPart.setFileName(singleFileName);
		multipart.addBodyPart(messageBodyPart);
	}

	/**
	 * 
	 * @return
	 */
	public String processDspMailConfigurations() {

		return null;
	}

	// Load the email properties
	private void loadEmailProps() {
		InputStream infoad = this.getClass().getClassLoader()
				.getResourceAsStream("com/etiqa/medicalpass/email/emailconfig.properties");
		try {
			activeDirMailProp.load(infoad);
		} catch (IOException ex) {
			logger.error(ex.getMessage(), ex);
		}
	}

	/**
	 * 
	 * @param dspCiTblQQ
	 * @param dspCommonTblPolicy
	 * @param dtc
	 * @param list
	 * @return
	 * @throws FileNotFoundException
	 */
	public String emailDispatchProcess(DspMpTblQQ mpQqData, PaymentResponse paymentResponse,
			DspCommonTblCustomer dspCommonTblCustomer, List<GeneratingReportsResponseVo> list, String company) {

		DspEmailTemplateBean templateBean = null;

		// Load mail properties
		loadEmailProps();

		if (emailService != null) {
			templateBean = emailService.loadMailTemplate(mpQqData, paymentResponse, dspCommonTblCustomer, company);
		}

		return dispatchDspEmail(templateBean, list, paymentResponse.getPolicyNumber());
	}

}
